<?php

namespace App\Models\Tenant;
class Opening extends ModelTenant
{
    public $timestamps = false;
    protected $with = ['customer'];
    protected $table = "openings";  
    protected $fillable = [
    'libro',
    'oper',
    'fechae',
    'fechav',
    'tipo',
    'serie',
    'numero',
    'tipodoc',
    'customer_id',
    'nombre',
    'dir',
    'tota',
    'totac',
    'totalz',
    'periodo',
    'establishment_id'
    ];
    public function customer()
    {
        return $this->belongsTo(Person::class,'customer_id','id');
    }
}
