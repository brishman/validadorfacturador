<?php

namespace App\Models\Tenant;
class Plan extends ModelTenant
{
    protected $table = 'plan';
    public $timestamps = false;
    protected $fillable = [
        'order',
        'account',
        'name',
        'level',
        'cost',
        'charge1',
        'charge2',
        'pay',
    ];
}
