<?php

namespace App\Models\Tenant;

use App\Models\Tenant\Catalogs\IdentityDocumentType;

class Company extends ModelTenant
{
    //protected $with = ['identity_document_type'];
    protected $fillable = [
        'number',
        'name',
        'webservices',
        'number_contribuyente',
        'token',
        'logo_store',
        'soap_type_id'
    ];

 
}
