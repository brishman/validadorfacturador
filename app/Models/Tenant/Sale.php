<?php

namespace App\Models\Tenant;
use App\Models\Tenant\Tables;
use Hyn\Tenancy\Traits\UsesTenantConnection;
use Illuminate\Database\Eloquent\Model;
class Sale extends ModelTenant
{
   
    protected $table = "sales";  
    protected $with = ['customer'];
    protected $fillable = [
        'period', 
        'oper',
        'tipoc',
        'customer_id',
        'serie',
        'numero',
        'serienota',
        'numeronota',
        'tipodocm',
       'fechae',
       'fechav',
       'fecham',
        'moneda',
        'fechacam',
        'tipocam',
        'asiento',
        'detalle',
        'tota',
        'igv',
         'n_igv',
        'base',
        'based',
        'baseivap',
        'ivap',
        'inafecta',
        'icbper',
        'exo',
        'otros',
        'isc',
        'reten',
        'export',
        'detrac',
       'fechadet',
        'numerodet',
        'totadetrac',
        'cobrar',
        'exportc',
        'basec',
        'exoc',
        'inafc',
        'iscoc',
        'igvc',
        'otrosc',
         'totac',
        'code_status',
        'xml_sales',
        'cdr_sales',
        'pdf_sales',
       'establishment_id'
        ];
    public function customer()
    {
        return $this->belongsTo(Person::class);
    }
}
