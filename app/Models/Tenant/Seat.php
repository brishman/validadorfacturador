<?php

namespace App\Models\Tenant;
use Illuminate\Database\Eloquent\Builder;
use Hyn\Tenancy\Traits\UsesTenantConnection;

class Seat extends ModelTenant
{
    use UsesTenantConnection;
    protected $table = 'seating';
    protected $fillable = [
        'cod',
        'description',
        'seating1',
        'seating2',
        'seating3',
        'seating4',
        'seating5',
        'seating6',
        'seating7',
        'seating8',
        'type',
        'status',
        'state',
    ];

     
}