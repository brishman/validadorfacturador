<?php

namespace App\Models\Tenant;
use Illuminate\Database\Eloquent\Builder;
use Hyn\Tenancy\Traits\UsesTenantConnection;
use Illuminate\Database\Eloquent\Model;

class Tables extends Model
{
    use UsesTenantConnection; 
    protected $table = "tables";  
  protected $fillable = [
        'description1',
        'description2',
        'code_rent',
        'description3',
        'type',
        'state'   
    ];

}
