<?php

namespace App\Imports;

use App\Models\Tenant\Company;
use App\Models\Tenant\Establishment;
use App\Models\Tenant\Person;
use App\Models\Tenant\Purchasee;
use App\Models\Tenant\Seat;
use Carbon\Carbon;
use Illuminate\Support\Collection;
use Maatwebsite\Excel\Concerns\Importable;
use Maatwebsite\Excel\Concerns\ToCollection;
use PhpOffice\PhpSpreadsheet\Shared\Date;

class PurchaseeImport implements ToCollection
{
    use Importable;

    protected $data;

    public function collection(Collection $rows)
    {
           
            $total = count($rows);
            $registered = 0;
            unset($rows[0]);
            $contribuyente=Establishment::where('id',request()->input('establishment_id'))->first();
           // $contribuyente=Establishment::where('number',$rows[1][20])->first();
            foreach ($rows as $row)
            {
              
              
                $rows=Person::where('number',trim($row[6]))->first();
                $fecha=substr($row[0],6,4)."-".substr($row[0],3,2)."-".substr($row[0],0,2);
                if($rows!=null){
                    $customer_id=$rows->id;
                }else{
                    if(strlen(trim($row[6]))=="8"){
                        $identity_document_type_id="1";
                    }else{
                        $identity_document_type_id="6";
                    }
                   $customer= Person::create([
                        'type'=>'suppliers',
                        'identity_document_type_id'=>$identity_document_type_id,
                        'number'=>$row[6],
                        'name'=>$row[7],
                        'country_id'=>'PE',
                        'department_id'=>'15',
                        'province_id'=>'1501',
                        'district_id'=>'150101',
                        'token_firebase'=>'',
                        'address'=>'',
                        'condition'=>'',
                        'state'=>'',
                        'email'=>'',
                        'telephone'=>'',
                        'perception_agent'=>'0',
                        'person_type_id'=>'1',
                        'enabled'=>true,
                    ]);
                    $customer_id=$customer->id;
                }
                if($row[12]=="0"){
                    $detalle="105";
                }else{
                    $detalle="100";
                }
                $periodo=substr($row[0],3,2)."-".substr($row[0],6,4);
                if($row[19]=="0.00"){
                    $detrac="No";
                }else{
                   $detrac="Si";
                }
                if($row[14]==null){
                    $otros="0.00";
                }else{
                    $otros=$row[14];
                }
                $seatting=Seat::where('cod','1401')->first();
               
                $purchasee=Purchasee::where('numero',trim($row[4]))->where('tipoc',$row[2])->where('establishment_id',$contribuyente->id)->first();
                $cant=Purchasee::where('numero',trim($row[4]))->where('tipoc',$row[2])->where('establishment_id',$contribuyente->id)->count();
             
                if($cant=="0"){
                   $totadetrac= number_format($row[18]-$row[19],2);
                 $sale= Purchasee::create([
                        'period' => session('periodo'),
                        'oper' => '01',
                        'tipoc' => $row[2],
                        'serie' => $row[3],
                        'numero' =>$row[4],
                        'customer_id' => $customer_id,
                        'fechae' =>$fecha,
                        'fechav' =>$fecha,
                        'moneda' => $row[8],
                        'fechacam' =>$fecha,
                        'tipocam' => $row[9],
                        'tota' => $row[18],
                        'igv' => $row[12],
                        'n_igv' => "18",
                        'base' => $row[11],
                        'exo' => $row[15],
                        'otros' => $otros,
                        'isc' => 0.00,
                        'reten' => $row[17],
                        'export' => $row[10],
                        'detrac' => $detrac,
                        'totadetrac' => $row[19],
                        'cobrar' => number_format($row[18]-$row[19],2),
                        'exportc2' => '',
                        'asiento' => "1401",
                        'detalle' => '105',
                        'exportc' => $seatting->seating1,
                        'basec' => $seatting->seating2,
                        'exoc' => $seatting->seating3,
                        'inafc' => $seatting->seating4,
                        'iscoc' => $seatting->seating5,
                        'igvc' => $seatting->seating6,
                        'otrosc' => $seatting->seating7,
                        'totac' => $seatting->seating8,
                        'establishment_id' => '1',
                        'fechadet'=>$row[20],
                        'numerodet'=>$row[21],
                        'fecham' =>$row[22],
                        'tipodocm' =>$row[23],
                        'serienota' =>$row[24],
                        'numeronota' =>$row[25],
                        ]);       
                }else{
                    $purchasee->update([
                        'period' => session('periodo'),
                        'oper' => '01',
                        'tipoc' => $row[2],
                        'serie' => $row[3],
                        'numero' =>$row[4],
                        'customer_id' => $customer_id,
                        'fechae' =>$fecha,
                        'fechav' =>$fecha,
                        'moneda' => $row[8],
                        'fechacam' =>$fecha,
                        'tipocam' => $row[9],
                        'tota' => $row[18],
                        'igv' => $row[12],
                        'n_igv' => "18",
                        'base' => $row[11],
                        'exo' => $row[15],
                        'otros' => $row[14],
                        'isc' => 0.00,
                        'reten' => $row[17],
                        'export' => $row[10],
                        'detrac' => $detrac,
                        'totadetrac' => $row[19],
                        'cobrar' => number_format($row[18]-$row[19],2),
                        'exportc2' => '',
                        'asiento' => "1401",
                        'detalle' => '105',
                        'exportc' => $seatting->seating1,
                        'basec' => $seatting->seating2,
                        'exoc' => $seatting->seating3,
                        'inafc' => $seatting->seating4,
                        'iscoc' => $seatting->seating5,
                        'igvc' => $seatting->seating6,
                        'otrosc' => $seatting->seating7,
                        'totac' => $seatting->seating8,
                        'establishment_id' => '1',
                        'fechadet'=>$row[20],
                        'numerodet'=>$row[21],
                        'fecham' =>$row[22],
                        'tipodocm' =>$row[23],
                        'serienota' =>$row[24],
                        'numeronota' =>$row[25],
                        ]);  
                }
            
                /*$company = Company::first();
                $validate_cpe = new ValidateCpe2();
                reValidate:
                $response = $validate_cpe->search_web($company->number,
                $row[2],
                $row[3],
                $row[4],
                substr($row[0],6,4)."-".substr($row[0],3,2)."-".substr($row[0],0,2),
                $row[18]);
                $sql_purchasee=Purchasee::where('numero',$row[4])->where('tipoc',$row[2])->first();
                 $purchasee_save=Purchasee::findOrFail($sql_purchasee->id);
                if ($response['success']===true) {
                    if($response['data']['comprobante_estado_codigo']=="0"){
                             //Variable inicio
                        reValidates:
                        $responses = $validate_cpe->search($company->number,
                        $row[2],
                        $row[3],
                        $row[4],
                        substr($row[0],6,4)."-".substr($row[0],3,2)."-".substr($row[0],0,2),
                        $row[18],2);
                        if ($responses['success']===true) {
                            $purchasee_save->code_status=$response['data']['comprobante_estado_codigo'];
                            $purchasee_save->save();
                        }else{
                             //Volver a Variable inicio
                             goto reValidates;
                        }        
                    }else{
                      
                    }
                    
                      $purchasee_save->code_status=$response['data']['comprobante_estado_codigo'];
                        $purchasee_save->save();
                } else {
                    goto reValidate;
                }*/
                $registered += 1;
            }
           
            $this->data = compact('total', 'registered');
          
           
    }

    public function getData()
    {
        return $this->data;
    }
}
