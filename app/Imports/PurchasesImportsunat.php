<?php

namespace App\Imports;

use Carbon\Carbon;
use App\Models\Tenant\Seat;
 use App\Models\Tenant\Person;
use App\Models\Tenant\Company;
use App\Models\Tenant\Purchasee;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\File; 
use PhpOffice\PhpSpreadsheet\Shared\Date;
use Maatwebsite\Excel\Concerns\Importable;
use Maatwebsite\Excel\Concerns\ToCollection;
class PurchasesImportsunat implements ToCollection
{
    use Importable;
    protected $data;
    public function collection(Collection $rows)
    {
        try {
            $total = count($rows);
            $registered = 0;
            unset($rows[0]);
            $contenido = "";
            $company = Company::first();
             $correlativo=0;
            $conteo=-1;
           $array_txt=[];
           //Insertar Data
           foreach ($rows as $row)
           {
             
               $rows_pers=Person::where('number',trim($row[6]))->first();
                
               if($rows_pers!=null){
                   $customer_id=$rows_pers->id;
               }else{
                   if(strlen(trim($row[6]))=="8"){
                       $identity_document_type_id="1";
                   }else{
                       $identity_document_type_id="6";
                   }
                  $customer= Person::create([
                       'type'=>'customers',
                       'identity_document_type_id'=>$identity_document_type_id,
                       'number'=>$row[6],
                       'name'=>$row[7],
                       'country_id'=>'PE',
                       'department_id'=>'15',
                       'province_id'=>'1501',
                       'district_id'=>'150101',
                       'token_firebase'=>'',
                       'address'=>'',
                       'condition'=>'',
                       'state'=>'',
                       'email'=>'',
                       'telephone'=>'',
                       'perception_agent'=>'0',
                       'person_type_id'=>'1',
                       'enabled'=>true,
                   ]);
                   $customer_id=$customer->id;
               }
               if($row[12]=="0"){
                   $detalle="105";
               }else{
                   $detalle="100";
               }
               $periodo=substr($row[0],3,2)."-".substr($row[0],6,4);
               if($row[19]=="0.00"){
                   $detrac="No";
               }else{
                  $detrac="Si";
               }
               if($row[14]==null){
                   $otros="0.00";
               }else{
                   $otros=$row[14];
               }
               $seatting=Seat::where('cod','1401')->first();
              
               $purchasee=Purchasee::where('numero',trim($row[4]))->where('tipoc',$row[2])->first();
               $cant=Purchasee::where('numero',trim($row[4]))->where('tipoc',$row[2])->count();
            
               if($cant=="0"){
                  $totadetrac= number_format($row[18]-$row[19],2);
                $sale= Purchasee::create([
                       'period' => $periodo,
                       'oper' => '01',
                       'tipoc' => $row[2],
                       'serie' => $row[3],
                       'numero' =>$row[4],
                       'customer_id' => $customer_id,
                       'fechae' =>$row[0],
                       'fechav' =>$row[0],
                       'moneda' => $row[8],
                       'fechacam' =>$row[0],
                       'tipocam' => $row[9],
                       'tota' => $row[20],
                       'igv' => $row[12],
                       'n_igv' => "18",
                       'base' => $row[11],
                       'exo' => $row[15],
                       'otros' => $otros,
                       'isc' => 0.00,
                       'reten' => $row[17],
                       'export' => $row[10],
                       'detrac' => $detrac,
                       'totadetrac' => $row[19],
                       'cobrar' => number_format($row[18]-$row[19],2),
                       'exportc2' => '',
                       'asiento' => "1401",
                       'detalle' => '105',
                       'exportc' => $seatting->seating1,
                       'basec' => $seatting->seating2,
                       'exoc' => $seatting->seating3,
                       'inafc' => $seatting->seating4,
                       'iscoc' => $seatting->seating5,
                       'igvc' => $seatting->seating6,
                       'otrosc' => $seatting->seating7,
                       'totac' => $seatting->seating8,
                       'establishment_id' => '1',
                       ]);       
               }else{
                   $purchasee->update([
                       'period' => $periodo,
                       'oper' => '01',
                       'tipoc' => $row[2],
                       'serie' => $row[3],
                       'numero' =>$row[4],
                       'customer_id' => $customer_id,
                       'fechae' =>$row[0],
                       'fechav' =>$row[0],
                       'moneda' => $row[8],
                       'fechacam' =>$row[0],
                       'tipocam' => $row[9],
                       'tota' => $row[20],
                       'igv' => $row[12],
                       'n_igv' => "18",
                       'base' => $row[11],
                       'exo' => $row[15],
                       'otros' => $row[14],
                       'isc' => 0.00,
                       'reten' => $row[17],
                       'export' => $row[10],
                       'detrac' => $detrac,
                       'totadetrac' => $row[19],
                       'cobrar' => number_format($row[18]-$row[19],2),
                       'exportc2' => '',
                       'asiento' => "1401",
                       'detalle' => '105',
                       'exportc' => $seatting->seating1,
                       'basec' => $seatting->seating2,
                       'exoc' => $seatting->seating3,
                       'inafc' => $seatting->seating4,
                       'iscoc' => $seatting->seating5,
                       'igvc' => $seatting->seating6,
                       'otrosc' => $seatting->seating7,
                       'totac' => $seatting->seating8,
                       'establishment_id' => '1',
                       ]);  
               }
           
      
                $registered += 1;
              
           }

           //
          
         foreach ($rows as $row){
             
             $conteo++;
             $contenido .= $row[6]."|";
             $contenido .= $row[2]."|";
             $contenido .= $row[3]."|";
             $contenido .= $row[4]."|";
             $contenido .= substr($row[0],8,2)."/".substr($row[0],5,2)."/".substr($row[0],0,4)."|";
             $contenido .= floatval($row[20])."\n";  
             if($conteo==250){ //CANTIDAD_TXT=250    
                 Storage::disk('public')->put("txt/".$correlativo."_".$company->number."_validarcpe.txt", $contenido);
                 $correlativo++;
                 $conteo=-1;
                 $contenido="";
              } 
              Storage::disk('public')->put("txt/".$correlativo."_".$company->number."_validarcpe.txt", $contenido);
              $success=true;
             
         }
         for ($ii=0; $ii<=$correlativo; $ii++) { 
                 $list = fopen(public_path("storage/txt/". $ii."_".$company->number."_validarcpe.txt"),"r");
                 $salida = fopen(public_path("storage/txt/".$ii."_".$company->number."_newvalidarcpe.txt"),'w');
                 while(!feof($list))
                 {
                     $lines[] = trim(fgets($list));  
                 }
                 $cant = count($lines);
                 $i=0;
                 for ($i=0;$i<=$cant-2;$i++)
                 {
                     if($i  === $cant-2) {
                         fwrite($salida, $lines[$i]);
                     } else {
                         fwrite($salida, $lines[$i]."\r\n");
                     }
                 }
                 fwrite($salida, $lines[$cant-1]);
                 fclose($salida);
               
              
          }
          for ($i=0; $i <=$correlativo ; $i++) { 
             unlink(public_path("storage/txt/".$i."_20573198108_validarcpe.txt"));
             array_push($array_txt,$i."_".$company->number."_newvalidarcpe.txt");
         }
         
         $this->data = compact('success','array_txt');
        } catch (Exception $e) { 
            return $e->getMessage();
        }
    }
    public function getData()
    {
        return $this->data;
    }
}
