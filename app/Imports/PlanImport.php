<?php

namespace App\Imports;
use App\Models\Tenant\Plan;
use Illuminate\Support\Collection;
use Maatwebsite\Excel\Concerns\Importable;
use Maatwebsite\Excel\Concerns\ToCollection;
use Carbon\Carbon;
use PhpOffice\PhpSpreadsheet\Shared\Date;

class PlanImport implements ToCollection
{
    use Importable;

    protected $data;

    public function collection(Collection $rows)
    {
            $total = count($rows);
            $registered = 0;
            unset($rows[0]);
            foreach ($rows as $row)
            {
                $plan=Plan::where('account',trim($row[1]))->first();
                if(!$plan){               
                    $plan= Plan::create([
                        'order'  => $row[0],
                        'account' =>$row[1],
                        'name'   => $row[2],
                        'level'  => $row[3],
                        'cost'  => $row[4],
                        'charge1' => $row[5],
                        'charge2' => $row[6],
                        'pay'  => $row[7],
                        ]);   
                }else{                    
                    $plan->update([
                        'order'  => $row[0],
                        'account' =>$row[1],
                        'name'   => $row[2],
                        'level'  => $row[3],
                        'cost'  => $row[4],
                        'charge1' => $row[5],
                        'charge2' => $row[6],
                        'pay'  => $row[7],
                        ]);   
                }
                $registered += 1;
            }
            $this->data = compact('total', 'registered');

    }

    public function getData()
    {
        return $this->data;
    }
}
