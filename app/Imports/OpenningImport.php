<?php

namespace App\Imports;

use App\Models\Tenant\Company;
use App\Models\Tenant\Establishment;
use App\Models\Tenant\Opening;
use App\Models\Tenant\Person;
use App\Models\Tenant\Sale;
use App\Models\Tenant\Seat;
use Carbon\Carbon;
use Illuminate\Support\Collection;
use Maatwebsite\Excel\Concerns\Importable;
use Maatwebsite\Excel\Concerns\ToCollection;
use PhpOffice\PhpSpreadsheet\Shared\Date;

class OpenningImport implements ToCollection
{
    use Importable;

    protected $data;

    public function collection(Collection $rows)
    {
            $total = count($rows);
            $registered = 0;
            unset($rows[0]);
             
            $contribuyente=Establishment::where('id',request()->input('establishment_id'))->first();
            Opening::where('establishment_id',$contribuyente->id)->delete();
            foreach ($rows as $row)
            {
                //$periodo=substr($row[0],3,2)."-".substr($row[0],6,4);
                if($row[8]!="0"){
                    $rows_clientes=Person::where('number',trim($row[9]))->first();
                    $fecha=substr($row[0],6,4)."-".substr($row[0],3,2)."-".substr($row[0],0,2);
                    
                    if($rows_clientes!=null){
                        $customer_id=$rows_clientes->id;
                    }else{
                        if(strlen(trim($row[9]))=="8"){
                            $identity_document_type_id="1";
                        }else{
                            $identity_document_type_id="6";
                        }
                       $customer= Person::create([
                            'type'=>'customers',
                            'identity_document_type_id'=>$identity_document_type_id,
                            'number'=>$row[9],
                            'name'=>$row[10],
                            'country_id'=>'PE',
                            'department_id'=>'15',
                            'province_id'=>'1501',
                            'district_id'=>'150101',
                            'token_firebase'=>'',
                            'address'=>$row[11],
                            'condition'=>'',
                            'state'=>'',
                            'email'=>'',
                            'telephone'=>'',
                            'perception_agent'=>'0',
                            'person_type_id'=>'1',
                            'enabled'=>true,
                        ]);
                        $customer_id=$customer->id;
                    }
    
                }else{
                    $customer_id=0;
                    $identity_document_type_id="1";
                }
                                
                //$opening=Opening::where('establishment_id',$contribuyente->id)->first();
                //  $cant=Opening::where('establishment_id',$contribuyente->id)->count();
                //
                $opening= Opening::create([
                    'libro' => $row[1],
                    'oper' => $row[2],
                    'fechae' => "01-01-".date("Y"),
                    'fechav' =>"01-01-".date("Y"),
                    'tipo' =>$row[5],
                    'serie' =>$row[6],
                    'numero' =>$row[7],
                    'tipodoc' =>$row[8],
                    'nombre' =>$row[10],
                    'dir' =>$row[11],
                    'customer_id' => $customer_id,
                    'tota' => $row[12],
                    'totac' => $row[13],
                    'totalz' => $row[14],
                    'periodo' => $row[15],
                    'establishment_id' => $contribuyente->id,
                    ]); 
                /*if($cant=="0"){
                    //dd($cant);
                    $opening= Opening::create([
                        'libro' => $row[1],
                        'oper' => $row[2],
                        'fechae' => $row[3],
                        'fechav' => $row[4],
                        'tipo' =>$row[5],
                        'serie' =>$row[6],
                        'numero' =>$row[7],
                        'tipodoc' =>$row[8],
                        'customer_id' => $customer_id,
                        'tota' => $row[12],
                        'totac' => $row[13],
                        'totalz' => $row[14],
                        'periodo' => $row[15],
                        'establishment_id' => $contribuyente->id,
                        ]);  
                }else{
                    $opening->update([
                        'libro' => $row[1],
                        'oper' => $row[2],
                        'fechae' => $row[3],
                        'fechav' => $row[4],
                        'tipo' =>$row[5],
                        'serie' =>$row[6],
                        'numero' =>$row[7],
                        'tipodoc' =>$row[8],
                        'customer_id' => $customer_id,
                        'tota' => $row[12],
                        'totac' => $row[13],
                        'totalz' => $row[14],
                        'periodo' => $row[15],
                        'establishment_id' => $contribuyente->id,
                        ]);  
                }*/
            
                $registered += 1;
            }
           
            $this->data = compact('total', 'registered');
          
           
    }

    public function getData()
    {
        return $this->data;
    }
}
