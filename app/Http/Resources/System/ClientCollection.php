<?php

namespace App\Http\Resources\System;

use Hyn\Tenancy\Environment;
use Illuminate\Support\Facades\DB;
use Illuminate\Http\Resources\Json\ResourceCollection;

class ClientCollection extends ResourceCollection
{
    /**
     * Transform the resource collection into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return mixed
     */
    public function toArray($request)
    {
        return $this->collection->transform(function($row, $key) {
            $tenancy = app(Environment::class);
            $tenancy->tenant($row->hostname->website);
            // $row->count_doc = DB::connection('tenant')->table('documents')->count();
            $row->count_doc        = DB::connection('tenant')->table('configurations')->first()->quantity_documents;
            $row->count_user       = DB::connection('tenant')->table('users')->count();
            $row->accepted         = DB::connection('tenant')->table('documents')->where('state_type_id','05')->count();
            //$row->registered       = DB::connection('tenant')->table('documents')->where('state_type_id','01')->count();
           // $row->canceled         = DB::connection('tenant')->table('documents')->where('state_type_id','11')->count();
            $row->boleta_accepted  = DB::connection('tenant')->table('documents')->where('state_type_id','05')->where('document_type_id','03')->count();
            $row->factura_accepted = DB::connection('tenant')->table('documents')->where('state_type_id','05')->where('document_type_id','01')->count();
            $row->credito_accepted = DB::connection('tenant')->table('documents')->where('state_type_id','05')->where('document_type_id','07')->count();
            $row->debito_accepted  = DB::connection('tenant')->table('documents')->where('state_type_id','05')->where('document_type_id','08')->count();

            $row->boleta_registered  = DB::connection('tenant')->table('documents')->where('state_type_id','01')->where('document_type_id','03')->count();
            $row->factura_registered = DB::connection('tenant')->table('documents')->where('state_type_id','01')->where('document_type_id','01')->count();
            $row->credito_registered = DB::connection('tenant')->table('documents')->where('state_type_id','01')->where('document_type_id','07')->count();
            $row->debito_registered  = DB::connection('tenant')->table('documents')->where('state_type_id','01')->where('document_type_id','08')->count();

            $row->boleta_canceled  = DB::connection('tenant')->table('documents')->where('state_type_id','11')->where('document_type_id','03')->count();
            $row->factura_canceled = DB::connection('tenant')->table('documents')->where('state_type_id','11')->where('document_type_id','01')->count();
            $row->credito_canceled = DB::connection('tenant')->table('documents')->where('state_type_id','11')->where('document_type_id','07')->count();
            $row->debito_canceled  = DB::connection('tenant')->table('documents')->where('state_type_id','11')->where('document_type_id','08')->count();
            
            $total_aceptado=[[
                "factura"=> $row->factura_accepted,
                "boleta"=> $row->boleta_accepted,
                "credito"=> $row->credito_accepted,
                "debito"=> $row->debito_accepted,
            ]];
            $total_registered=[[
                "factura"=> $row->factura_registered,
                "boleta"=> $row->boleta_registered,
                "credito"=> $row->credito_registered,
                "debito"=> $row->debito_registered,
            ]];
            $total_canceled=[[
                "factura"=> $row->factura_canceled,
                "boleta"=> $row->boleta_canceled,
                "credito"=> $row->credito_canceled,
                "debito"=> $row->debito_canceled,
            ]];
            return [
                'id' => $row->id,
                'hostname' => $row->hostname->fqdn,
                'name' => $row->name,
                'email' => $row->email,
                'token' => $row->token,
                'number' => $row->number,
                'plan' => $row->plan->name,
                'locked' => (bool) $row->locked,
                'locked_emission' => (bool) $row->locked_emission,
                'locked_users' => (bool) $row->locked_users,
                'locked_tenant' => (bool) $row->locked_tenant,
                'count_doc' => $row->count_doc,
                'max_documents' => (int) $row->plan->limit_documents,
                'count_user' => $row->count_user,
                'max_users' => (int) $row->plan->limit_users,
                'created_at' => $row->created_at->format('Y-m-d H:i:s'),
                'updated_at' => $row->updated_at->format('Y-m-d H:i:s'),
                'start_billing_cycle' => ( $row->start_billing_cycle ) ? $row->start_billing_cycle->format('Y-m-d') : '',
                'count_doc_month' => $row->count_doc_month,
                'accepted' => $row->accepted,
                'total_accepted' =>  $total_aceptado,              
                'total_registered' =>$total_registered,
                'total_canceled' =>$total_canceled,

                'registered' => $row->registered,
                'canceled' => $row->canceled,
                'boleta'   => $row->boleta,
                'factura' => $row->factura,
                'credito' => $row->credito,
                'debito' => $row->debito,


                'select_date_billing' => '',
            ];
        });
    }
}
