<?php

namespace App\Http\Resources\Tenant;

use Illuminate\Http\Resources\Json\JsonResource;
use App\Models\Tenant\Configuration;


class CompanyResource extends JsonResource
{
  
    public function toArray($request)
    {
         return [
            'id' => $this->id,
            'number' => $this->number,
            'name' => $this->name,
            'webservices' => $this->webservices,
            'token' => $this->token,
            'logo' => $this->logo,
            'logo_store' => $this->logo_store,
     

        ];
    }
}
