<?php

namespace App\Http\Resources\Tenant;

use Illuminate\Http\Resources\Json\ResourceCollection;
class OpeningCollection extends ResourceCollection
{

    public function toArray($request)
    {
        return $this->collection->transform(function($row, $key) {
 
            return [
             'id'=> $row->id,  
            'libro' =>  $row->libro,  
            'oper' =>  $row->oper,  
            'fechae' =>  $row->fechae,  
            'fechav' =>  $row->fechav,  
            'tipo' => $row->tipo,  
            'serie' => $row->serie,  
            'numero' => $row->numero,  
            'tipodoc' => $row->tipodoc,  
            'documento' =>  $row->documento,  
            'nombre' => $row->nombre,  
            'direccion' => $row->dir,  
            'tota' => $row->tota,  
            'totac' => $row->totac,  
            'totalz' =>  $row->totalz,  
            'periodo' =>  $row->periodo,  
            'establishment_id' =>  $row->establishment_id,  
            ];
        });
    }
}
