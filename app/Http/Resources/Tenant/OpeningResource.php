<?php

namespace App\Http\Resources\Tenant;

use App\Models\Tenant\Person;
use Illuminate\Http\Resources\Json\JsonResource;

class OpeningResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request
     * @return array
     */
    public function toArray($request)
    {
        //$cliente=Person::where('id',$this->customer_id)->first();
        return [
            'id'=> $this->id,  
            'libro' =>  $this->libro,  
            'oper' =>  $this->oper,  
            'fechae' =>  $this->fechae,  
            'fechav' =>  $this->fechav,  
            'tipo' => $this->tipo,  
            'serie' => $this->serie,  
            'numero' => $this->numero,  
            'tipodoc' => $this->tipodoc,  
          //  'documento' =>  $this->documento,  
          //  'nombre' => $this->customer->name,  
          //  'direccion' => $this->customer->address,  
            'customer_id' => $this->customer_id,
            'customer' => $this->customer,  
            'tota' => $this->tota,  
            'totac' => $this->totac,  
            'totalz' =>  $this->totalz,  
            'periodo' =>  $this->periodo,  
            'establishment_id' =>  $this->establishment_id,  
        ];
    }
}
