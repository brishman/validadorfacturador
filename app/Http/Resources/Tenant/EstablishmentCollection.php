<?php

namespace App\Http\Resources\Tenant;

use Illuminate\Http\Resources\Json\ResourceCollection;

class EstablishmentCollection extends ResourceCollection
{
    /**
     * Transform the resource collection into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return mixed
     */
    public function toArray($request)
    {
        return $this->collection->transform(function($row, $key) {
            return [
                'id' => $row->id,
                'number' => $row->number,
                'identity_document_type_id' => $row->identity_document_type_id,
                'name' => $row->description,
                'description' => $row->description,
                'country_id' => $row->country_id,
                'department_id' => $row->department_id,
                'province_id' => $row->province_id,
                'district_id' => $row->district_id,
                'address' => $row->address,
                'telephone' => $row->telephone,
                'email' => $row->email,
                'code' => $row->code,
                'user_sol'=>$row->user_sol,
                'clave_sol'=>$row->clave_sol
            ];
        });
    }
}