<?php
namespace App\Http\Resources\Tenant;
use Illuminate\Http\Resources\Json\JsonResource;

class PurchaseeResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id'=> $this->id, 
            'period'=> $this->period, 
            'establishment_id'=> $this->establishment_id, 
            'oper'=> $this->oper, 
            'tipoc'=> $this->tipoc, 
            'customer_id'=> $this->customer_id, 
            'customer'=> $this->customer, 
            'serie'=> $this->serie, 
            'numero'=> $this->serie."-".$this->numero, 
            'fechae'=> $this->fechae, 
            'fechav'=> $this->fechav, 
            'moneda'=> $this->moneda, 
            'fechacam'=> $this->fechacam, 
            'tipocam'=> $this->tipocam, 
            'asiento'=> $this->asiento, 
            'detalle'=> $this->detalle, 
            'tota'=> $this->tota, 
            'n_igv'=> $this->n_igv, 
            'igv'=> $this->igv, 
            'base'=> $this->base, 
            'exo'=> $this->exo, 
            'otros'=> $this->otros, 
            'isc'=> $this->isc, 
            'reten'=> $this->reten, 
            'export'=> $this->export, 
            'detrac'=> $this->detrac, 
            'totadetrac'=> $this->totadetrac, 
            'cobrar'=> $this->cobrar, 
            'exportc'=> $this->exportc, 
            'inafc'=> $this->inafc, 
            'basec'=> $this->basec, 
            'exoc'=> $this->exoc, 
            'iscoc'=> $this->iscoc, 
            'igvc'=> $this->igvc, 
            'otrosc'=> $this->otrosc, 
            'totac'=> $this->totac, 
            'xml_purchase'=> $this->xml_purchase, 
            'cdr_purchase'=> $this->cdr_purchase, 
            'pdf_purchase'=> $this->pdf_purchase, 
            'created_at' => $this->created_at->format('Y-m-d H:i:s'),
            'updated_at' => $this->updated_at->format('Y-m-d H:i:s'),
        ];
    }
}
