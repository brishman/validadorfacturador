<?php

namespace App\Http\Resources\Tenant;

use Illuminate\Http\Resources\Json\ResourceCollection;

class PlanCollection extends ResourceCollection
{
    /**
     * Transform the resource collection into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return mixed
     */
    public function toArray($request)
    {
        return $this->collection->transform(function($row, $key) {
            return [
            'id'=> $row->id,  
            'order'=> $row->order, 
            'account'=> $row->account, 
            'name'=> $row->name, 
            'level'=> $row->level, 
            'cost'=> $row->cost, 
            'charge1'=> $row->charge1, 
            'charge2'=> $row->charge2, 
            'pay'=> $row->pay, 
             
            ];
        });
    }
}
