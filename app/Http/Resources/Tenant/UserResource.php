<?php

namespace App\Http\Resources\Tenant;

use App\Models\Tenant\Module;
use Illuminate\Http\Resources\Json\JsonResource;

class UserResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request
     * @return array
     */
    public function toArray($request)
    {
       
         $levels_in_user = $this->levels->pluck('id')->toArray();
        
        $levels = [];

 
        return [
            'id' => $this->id,
            'email' => $this->email,
            'name' => $this->name,
            'api_token' => $this->api_token,
            'establishment_id' => $this->establishment_id,
            'type' => $this->type,
        
            'locked' => (bool) $this->locked,

        ];
    }
}