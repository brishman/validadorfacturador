<?php
namespace App\Http\Resources\Tenant;
use Illuminate\Http\Resources\Json\ResourceCollection;
class PurchaseeCollection extends ResourceCollection
{

    public function toArray($request)
    {
        return $this->collection->transform(function($row, $key) {
            if($row->code_status=="0"){
                $state_type_sunat_description="NO EXISTE";
            }else if($row->code_status=="1"){
                $state_type_sunat_description="ACEPTADO";
            }else if($row->code_status=="2"){
                $state_type_sunat_description="ANULADO";
            }else if($row->code_status=="3"){
                $state_type_sunat_description="AUTORIZADO";
            }else{
                $state_type_sunat_description="NO AUTORIZADO";
            }

             return [
            'id'=> $row->id,  
            'period'=> $row->period, 
            'establishment_id'=> $row->establishment_id, 
            'oper'=> $row->oper, 
            'tipoc'=> $row->tipoc, 
            'customer'=> $row->customer, 
            'customer_id'=> $row->customer_id, 
            'serie'=> $row->serie, 
            'numero'=> $row->numero, 
            'fechae'=> $row->fechae, 
            'fechav'=> $row->fechav, 
            'moneda'=> $row->moneda, 
            'fechacam'=> $row->moneda, 
            'tipocam'=>$row->tipocam, 
            'asiento'=> $row->asiento, 
            'detalle'=> $row->detalle, 
            'tota'=> $row->tota, 
            'igv'=> $row->igv, 
            'base'=> $row->base, 
            'exo'=> $row->exo, 
            'otros'=> $row->otros, 
            'isc'=> $row->isc, 
            'reten'=> $row->reten, 
            'export'=> $row->export, 
            'detrac'=> $row->detrac, 
            'totadetrac'=> $row->totadetrac, 
            'cobrar'=> $row->cobrar, 
            'exportc'=> $row->exportc, 
            'inafc'=> $row->inafc, 
            'basec'=> $row->inafc, 
            'exoc'=> $row->exoc, 
            'inafc'=> $row->inafc, 
            'iscoc'=> $row->iscoc, 
            'igvc'=> $row->igvc, 
            'otrosc'=> $row->otrosc, 
            'totac'=> $row->totac, 
            'code_status'=> $row->code_status, 

            'xml_purchase'=> $row->xml_purchase, 
            'cdr_purchase'=> $row->cdr_purchase, 
            'pdf_purchase'=> $row->pdf_purchase, 

            'state_type_sunat_description'=> $state_type_sunat_description,
            'created_at' => $row->created_at->format('Y-m-d H:i:s'),
            'updated_at' => $row->updated_at->format('Y-m-d H:i:s'),
            ];
        });
    }
}
