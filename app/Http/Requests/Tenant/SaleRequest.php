<?php

namespace App\Http\Requests\Tenant;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

class SaleRequest extends FormRequest
{
    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        $id = $this->input('id');
        return [
        'period'=>['required'], 
        'establishment_id'=>['required'], 
        'oper'=>['required'], 
        'tipoc'=>['required'], 
        'customer_id'=>['required'], 
        'serie'=>['required'], 
        'numero'=>['required'], 
        'fechae'=>['required'], 
        'fechav'=>['required'], 
        'moneda'=>['required'], 
        'fechacam'=>['required'], 
        'tipocam'=>['required'], 
        'asiento'=>['required'], 
        'detalle'=>['required'], 
     //   'tota'=>'numeric|between:0.001,99.99', 
        'igv'=>['required'], 
    //    'base'=>'numeric|between:0.001,99.99',
        'exo'=>['required'], 
        'otros'=>['required'], 
        'isc'=>['required'], 
        'reten'=>['required'], 
        'export'=>['required'], 
        'detrac'=>['required'], 
        'totadetrac'=>['required'], 
    //    'cobrar'=>'numeric|between:0.001,99.99',
    //    'exportc'=>['required'], 
    //    'exoc'=>['required'], 
  //      'inafc'=>['required'], 
    //    'basec'=>['required'], 
    //    'inafc'=>['required'], 
    //    'iscoc'=>['required'], 
    //    'igvc'=>['required'], 
    //    'otrosc'=>['required'], 
   //     'totac'=>['required'] 
        ];
    }
    public function messages()
    {
    return [
        'period.required'=>'Es Obligatorio', 
        'establishment_id.required'=>'Es Obligatorio',
        'oper.required'=>'Es Obligatorio',
        'tipoc.required'=>'Es Obligatorio',
        'customer_id.required'=>'Es Obligatorio',
        'serie.required'=>'Es Obligatorio',
        'numero.required'=>'Es Obligatorio',
        'fechae.required'=>'Es Obligatorio',
        'fechav.required'=>'Es Obligatorio',
        'moneda.required'=>'Es Obligatorio',
        'fechacam.required'=>'Es Obligatorio',
        'tipocam.required'=>'Es Obligatorio',
        'asiento.required'=>'Es Obligatorio',
        'detalle.required'=>'Es Obligatorio',
        'tota.required'=>'Es Obligatorio',
        
        'igv.required'=>'Es Obligatorio',
        'base.required'=>'Es Obligatorio',
        'exo.required'=>'Es Obligatorio',
        'otros.required'=>'Es Obligatorio',
        'isc.required'=>'Es Obligatorio',
        'reten.required'=>'Es Obligatorio',
        'export.required'=>'Es Obligatorio',
        'detrac.required'=>'Es Obligatorio',
        'totadetrac.required'=>'Es Obligatorio',
        'cobrar.required'=>'Es Obligatorio',
        'exportc.required'=>'Es Obligatorio',
        'exoc.required'=>'Es Obligatorio',
        'inafc.required'=>'Es Obligatorio',
        'basec.required'=>'Es Obligatorio',
        'exoc.required'=>'Es Obligatorio',
        'inafc.required'=>'Es Obligatorio',
        'iscoc.required'=>'Es Obligatorio',
        'igvc.required'=>'Es Obligatorio',
        'otrosc.required'=>'Es Obligatorio',
        'totac.required'=>'Es Obligatorio'     
    ];
    }
}