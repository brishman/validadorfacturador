<?php

namespace App\Http\Requests\Tenant;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

class OpeningRequest extends FormRequest
{
    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        $id = $this->input('id');
        return [
            'libro' => 'required',
            'oper' => 'required',
            'fechae' => 'required',
            'fechav' => 'required',
            'tipo' =>'required',
            'serie' =>'required',
            'numero' =>'required',
            'tipodoc' =>'required',
            //'documento' => 'required',
            //'nombre' =>'required',
            //'dir' =>'required',
            'tota' =>'required',
            'totac' => 'required',
            'totalz' => 'required',
            'periodo' => 'required',
            'establishment_id' => 'required',
   
        ];
    }
    public function messages()
    {
    return [
            'libro.required' => 'Es Obligatorio',
            'oper.required' => 'Es Obligatorio',
            'fechae.required' => 'Es Obligatorio',
            'fechav.required' => 'Es Obligatorio',
            'tipo.required' =>'Es Obligatorio',
            'serie.required' =>'Es Obligatorio',
            'numero.required' =>'Es Obligatorio',
            'tipodoc.required' =>'Es Obligatorio',
            'documento.required' => 'Es Obligatorio',
            'customer_id.required' =>'Es Obligatorio',
            'tota.required' =>'Es Obligatorio',
            'totac.required' => 'Es Obligatorio',
            'totalz.required' => 'Es Obligatorio',
            'periodo.required' => 'Es Obligatorio',
            'establishment_id.required' => 'Es Obligatorio'       
    ];
    }
}