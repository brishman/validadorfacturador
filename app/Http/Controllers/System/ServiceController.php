<?php
namespace App\Http\Controllers\System;

use GuzzleHttp\Client;
use App\Http\Controllers\Controller;
//use Modules\Services\Data\ServiceData;
use App\CoreFacturalo\Services\Ruc\Sunat;
use App\Models\System\Configuration;
class ServiceController extends Controller
{
    public function ruc($number)
    {
        $configuration = Configuration::first();
        $url = $configuration->webservices;
        $token = $configuration->token;
        $client = new Client(['base_uri' => $url, 'verify' => false]);
        $parameters = [
            'http_errors' => false,
            'connect_timeout' => 5,
            'headers' => [
                'Authorization' => 'Bearer '.$token,
                'Accept' => 'application/json',
            ],
        ];
        $res = $client->request('GET', '/api/ruc/'.$number, $parameters);
        $response = json_decode($res->getBody()->getContents());
      
        if ($response->success==true) {
            return [
                'success' => true,
                'data' => [
                    'name' => $response->data->nombre_o_razon_social,
                    'trade_name' => $response->data->nombre_o_razon_social,
                ]
            ];
        } else {
            return [
                'success' => false,
                'message' =>"Ocurrio un Error"
            ];
        }
   
}
}