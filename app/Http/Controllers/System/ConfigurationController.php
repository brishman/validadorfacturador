<?php
namespace App\Http\Controllers\System;

use App\Http\Controllers\Controller;
use Exception;
use Illuminate\Http\Request;
use App\Models\System\Configuration;



class ConfigurationController extends Controller
{

    public function index()
    {
        return view('system.configuration.index');
    }

    public function record()
    {

        $configuration = Configuration::first();

        return [
            'webservices' => $configuration->webservices,
            'token' => $configuration->token,
        ];
    }


    public function store(Request $request)
    {
        $configuration = Configuration::first();

        if($request->webservices)
        {
            $configuration->webservices = $request->webservices;
        }

        if($request->token)
        {
            $configuration->token = $request->token;
        }

        $configuration->save();

        return [
            'success' => true,
            'message' => 'Datos guardados con exito'
        ];


    }
}
