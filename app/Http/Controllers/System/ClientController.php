<?php

namespace App\Http\Controllers\System;

use Exception;
use App\Models\System\Plan;
use Illuminate\Support\Str;
use Hyn\Tenancy\Environment;
use Illuminate\Http\Request;
use App\Models\System\Client;
use Hyn\Tenancy\Models\Website;
use Hyn\Tenancy\Models\Hostname;
use Illuminate\Support\Facades\DB;
use App\Http\Controllers\Controller;
use App\Models\System\Configuration;
use Illuminate\Support\Facades\Storage;
use App\Http\Requests\System\ClientRequest;
use App\Http\Resources\System\ClientCollection;
use Hyn\Tenancy\Contracts\Repositories\WebsiteRepository;
use App\Http\Controllers\System\Services\Data\ServiceData;
use Hyn\Tenancy\Contracts\Repositories\HostnameRepository;
class ClientController extends Controller
{
    protected $document_state = [
        '-' => '-',
        '0' => 'No Existe',
        '1' => 'Aceptado',
        '2' => 'Anulado',
        '3' => 'AUTORIZADO',
        '4' => 'NO AUTORIZADO'
    ];

    public function index()
    {
        return view('system.clients.index');
    }

    public function create()
    {
        return view('system.clients.form');
    }

    public function tables()
    {

        $url_base = '.'.config('tenant.app_url_base');
        $plans = Plan::all();
        $types = [['type' => 'admin', 'description'=>'Administrador'], ['type' => 'integrator', 'description'=>'Listar Documentos']];
        $config = Configuration::first();
        $certificate_admin = $config->certificate;
        $soap_username =  $config->soap_username;
        $soap_password =  $config->soap_password;
        $webservices=$config->webservices;
        $token=$config->token;

        return compact('url_base','plans','types', 'certificate_admin', 'soap_username', 'soap_password','webservices','token');
    }
    public function columns(){
        return [
            'number' => 'Número Ruc',
            'name' => 'Nombre',
         ];
    }
    public function records(Request $request)
    {
        $records = Client::where($request->column, 'like', "%{$request->value}%")
         ->orderBy($request->column);
         return new ClientCollection($records->paginate(config('tenant.items_per_page')));
    }

    public function search($id,$state_type_id)
    {
        $client = Client::findOrFail($id);
        $tenancy = app(Environment::class);
        $tenancy->tenant($client->hostname->website);
        $records =  DB::connection('tenant')->table('documents')->where('state_type_id',$state_type_id);
        $correlativo=0;
        $conteo=0;  
        $contenido="";
        $num_cpe=0;
        $cantidad_rows=$records->count();
        $company=DB::connection('tenant')->table('companies')->first();
        foreach ($records->get() as $row){
            $num_cpe++;
            if($num_cpe==$cantidad_rows){
                break;
            }else{
                $conteo= $conteo+1; 
                $contenido .= $company->number."|";
                $contenido .= $row->document_type_id."|";
                $contenido .= $row->series."|";
                $contenido .= intval($row->number)."|";
                $contenido .= substr($row->date_of_issue,8,2)."/".substr($row->date_of_issue,5,2)."/".substr($row->date_of_issue,0,4)."|";
                $contenido .= $row->total."\n";  
                if($conteo==250){ //CANTIDAD_TXT=250    
                    Storage::disk('public')->put("txt/".$correlativo."_".$company->number."_validarcpe.txt", $contenido);
                    $correlativo++;
                    $conteo=-1;
                    $contenido="";
                 }
                 Storage::disk('public')->put("txt/".$correlativo."_".$company->number."_validarcpe.txt", $contenido);
                 $success=true;
    
            }
        }
            return [
                "success"=>true,
                "archivo_txt"=>$correlativo+1,
                "cantidad_cpe"=>$records->count(),
                "message"=>"se genero los archivos txt correctamente"
            ];
        
    }

    public function validateDocuments(Request $request){
        $client = Client::findOrFail($request->id);
        $tenancy = app(Environment::class);
        $tenancy->tenant($client->hostname->website);
        $company=DB::connection('tenant')->table('companies')->first();
        $service = new ServiceData(); 
        $filename=$request->numero."_".$company->number."_validarcpe.txt";
        $file=public_path("storage/txt/{$filename}") ;
        $res = $service->validar_cpe($company->number,substr($company->soap_username,11,8),$company->soap_password,$filename);    
        $data_response=json_decode($res);
        if (array_key_exists('data',json_decode($res,true))) {
            $res =json_decode($res);
            $data=$res->data;
            foreach ($data as $key => $value) {
                $sale_find= DB::connection('tenant')->table('documents')
                            ->where('series',$value->Serie)
                            ->where('number',$value->Numero)
                            ->where('document_type_id',$value->TipoComprobante)
                            ->first();
                $state_type=DB::connection('tenant')->table('state_types')->where('description', 'like', "%{$this->document_state[$value->EstadoCodComprobante]}%")->first();
                if($value->EstadoComprobante=="NO EXISTE"){
                $state_type_id="01";
                }else{
                    $state_type_id=$state_type->id;
                }
                DB::connection('tenant')->table('documents')->where('id',  $sale_find->id)
                                        ->update(['state_type_id' => $state_type_id]);
            }
            return response()->json($res);
        }else{
        
          return response()->json(["success"=>false,"message"=>$data_response]);
          
        }
       
         
    }


    public function update(Request $request)
    {
        try
        {
        
            $temp_path = $request->input('temp_path');

            $client = Client::findOrFail($request->id);
            $client->plan_id = $request->plan_id;
            $client->save();

            $plan = Plan::find($request->plan_id);

            $tenancy = app(Environment::class);
            $tenancy->tenant($client->hostname->website);
            DB::connection('tenant')->table('configurations')->where('id',  $request->id)
                ->update([
                            'plan' => json_encode($plan),
                            'config_system_env' => $request->config_system_env,
                            'limit_documents' =>  $plan->limit_documents
                        ]);

            DB::connection('tenant')->table('companies')->where('id', 1)->update([
                'number' => $request->number,
                'name'=> $request->name,
                'webservices'=> $request->webservices,
                'number_contribuyente'=>$request->number_contribuyente,
                'token'=>$request->token,
            ]);
 
            //modules

            return [
                'success' => true,
                'message' => 'Cliente Actualizado satisfactoriamente'
            ];

        }catch(Exception $e)
        {
            return [
                'success' => false,
                'message' => $e->getMessage()
            ];

        }

    }

    public function store(ClientRequest $request)
    {
        $temp_path = $request->input('temp_path');
        $configuration = Configuration::first();

        $name_certificate = $configuration->certificate;

        $subDom = strtolower($request->input('subdomain'));
        $uuid = config('tenant.prefix_database').'_'.$subDom;
        $fqdn = $subDom.'.'.config('tenant.app_url_base');

        $website = new Website();
        $hostname = new Hostname();
        $this->validateWebsite($uuid, $website);
        DB::connection('system')->beginTransaction();
        try {
            $website->uuid = $uuid;
            app(WebsiteRepository::class)->create($website);
            $hostname->fqdn = $fqdn;
            app(HostnameRepository::class)->attach($hostname, $website);
            $tenancy = app(Environment::class);
            $tenancy->tenant($website);
            $token = Str::random(50);
            $client = new Client();
            $client->hostname_id = $hostname->id;
            $client->token = $token;
            $client->email = strtolower($request->input('email'));
            $client->name = $request->input('name');
            $client->number = $request->input('number');
            $client->plan_id = $request->input('plan_id');
            $client->locked_emission = $request->input('locked_emission');
            $client->save();
            DB::connection('system')->commit();
        }
        catch (Exception $e) {
            DB::connection('system')->rollBack();
            app(HostnameRepository::class)->delete($hostname, true);
            app(WebsiteRepository::class)->delete($website, true);

            return [
                'success' => false,
                'message' => $e->getMessage()
            ];
        }
        DB::connection('tenant')->table('companies')->insert([
            'number' => $request->input('number'),
            'name' => $request->input('name'),
            'webservices' =>$request->input('webservices'),
            'token' =>$request->input('token'),
            'number_contribuyente'=>$request->input('number_contribuyente'),
        ]);

        $plan = Plan::findOrFail($request->input('plan_id'));

        DB::connection('tenant')->table('configurations')->insert([
            'send_auto' => true,
            'locked_emission' =>  $request->input('locked_emission'),
            'locked_tenant' =>  false,
            'locked_users' =>  false,
            'limit_documents' =>  $plan->limit_documents,
            'limit_users' =>  $plan->limit_users,
            'plan' => json_encode($plan),
            'date_time_start' =>  date('Y-m-d H:i:s'),
            'quantity_documents' =>  0,
            'config_system_env' => $request->config_system_env
        ]);

       /* DB::connection('tenant')->table('series')->insert([
            ['establishment_id' => 1, 'document_type_id' => '01', 'number' => 'F001'],
            ['establishment_id' => 1, 'document_type_id' => '03', 'number' => 'B001'],
            ['establishment_id' => 1, 'document_type_id' => '07', 'number' => 'FC01'],
            ['establishment_id' => 1, 'document_type_id' => '07', 'number' => 'BC01'],
            ['establishment_id' => 1, 'document_type_id' => '08', 'number' => 'FD01'],
            ['establishment_id' => 1, 'document_type_id' => '08', 'number' => 'BD01'],
            ['establishment_id' => 1, 'document_type_id' => '20', 'number' => 'R001'],
            ['establishment_id' => 1, 'document_type_id' => '09', 'number' => 'T001'],
            ['establishment_id' => 1, 'document_type_id' => '40', 'number' => 'P001'],
        ]);*/

////////////////////////////////////////////////////////////////////////////////////////////////////////////
// tenacy_demo

// tabla1
DB::connection('tenant')->table('tables')->insert([
    'description1' => '001',
    'description2' => 'DEPÓSITO EN CUENTA',
    'code_rent' => '',
    'type' => '1',
    'state'=>true
]);

DB::connection('tenant')->table('tables')->insert([
    'description1' => '002',
    'description2' => 'GIRO',
    'code_rent' => '',
   'type' => '1',
    'state'=>true
]);   

DB::connection('tenant')->table('tables')->insert([
    'description1' => '003',
    'description2' => 'TRANSFERENCIA DE FONDOS',
    'code_rent' => '',
     'type' => '1',
    'state'=>true
]);

DB::connection('tenant')->table('tables')->insert([
    'description1' => '004',
    'description2' => 'ORDEN DE PAGO',
    'code_rent' => '',
     'type' => '1',
    'state'=>true
]);
DB::connection('tenant')->table('tables')->insert([
    'description1' => '005',
    'description2' => 'TARJETA DE DÉBITO',
    'code_rent' => '',
     'type' => '1',
    'state'=>true
]);
DB::connection('tenant')->table('tables')->insert([
    'description1' => '006',
'description2' => 'TARJETA DE CRÉDITO EMITIDA EN EL PAÍS POR UNA EMPRESA DEL SISTEMA FINANCIERO',
    'code_rent' => '',
     'type' => '1',
    'state'=>true
]);
DB::connection('tenant')->table('tables')->insert([
    'description1' => '007',
'description2' => 'CHEQUES CON LA CLÁUSULA DE "NO NEGOCIABLE"', 
    'code_rent' => '',
     'type' => '1',
    'state'=>true
]);
DB::connection('tenant')->table('tables')->insert([
    'description1' => '008',
'description2' => 'EFECTIVO, POR OPERACIONES EN LAS QUE NO EXISTE OBLIGACIÓN DE UTILIZAR MEDIO DE PAGO',
    'code_rent' => '',
     'type' => '1',
    'state'=>true
]);
DB::connection('tenant')->table('tables')->insert([
    'description1' => '009',
    'description2' => 'EFECTIVO, EN LOS DEMÁS CASOS',
    'code_rent' => '',
     'type' => '1',
    'state'=>true
]);
DB::connection('tenant')->table('tables')->insert([
    'description1' => '010',
    'description2' => 'MEDIOS DE PAGO USADOS EN COMERCIO EXTERIOR ',
    'code_rent' => '',
     'type' => '1',
    'state'=>true
]);
DB::connection('tenant')->table('tables')->insert([
    'description1' => '011',
'description2' => 'DOCUMENTOS EMITIDOS POR LAS EDPYMES',
    'code_rent' => '',
     'type' => '1',
    'state'=>true
]);
DB::connection('tenant')->table('tables')->insert([
    'description1' => '012',
'description2' => 'TARJETA DE CRÉDITO ',
    'code_rent' => '',
     'type' => '1',
    'state'=>true
]);
DB::connection('tenant')->table('tables')->insert([
    'description1' => '013',
'description2' => 'TARJETAS DE CRÉDITO',
    'code_rent' => '',
     'type' => '1',
    'state'=>true
]);
DB::connection('tenant')->table('tables')->insert([
    'description1' => '101',
    'description2' => 'TRANSFERENCIAS-COMERCIO EXTERIOR',
    'code_rent' => '',
     'type' => '1',
    'state'=>true
]);
DB::connection('tenant')->table('tables')->insert([
    'description1' => '102',
    'description2' => 'CHEQUES BANCARIOS-COMERCIO EXTERIOR',
    'code_rent' => '',
     'type' => '1',
    'state'=>true
]);
DB::connection('tenant')->table('tables')->insert([
    'description1' => '103',
    'description2' => 'ORDEN DE PAGO SIMPLE-COMERCIO EXTERIOR',
    'code_rent' => '',
     'type' => '1',
    'state'=>true
]);
DB::connection('tenant')->table('tables')->insert([
    'description1' => '104',
    'description2' => 'ORDEN DE PAGO DOCUMENTARIO-COMERCIO EXTERIOR',
    'code_rent' => '',
     'type' => '1',
    'state'=>true
]);
DB::connection('tenant')->table('tables')->insert([
    'description1' => '105',
    'description2' => 'REMESA SIMPLE-COMERCIO EXTERIOR',
    'code_rent' => '',
     'type' => '1',
    'state'=>true
]);
DB::connection('tenant')->table('tables')->insert([
    'description1' => '106',
    'description2' => 'REMESA DOCUMENTARIA-COMERCIO EXTERIOR',
    'code_rent' => '',
     'type' => '1',
    'state'=>true
]);
DB::connection('tenant')->table('tables')->insert([
    'description1' => '107',
    'description2' => 'CARTA DE CRÉDITO SIMPLE-COMERCIO EXTERIOR',
    'code_rent' => '',
     'type' => '1',
    'state'=>true
]);
DB::connection('tenant')->table('tables')->insert([
    'description1' => '108',
    'description2' => 'CARTA DE CRÉDITO DOCUMENTARIO-COMERCIO EXTERIOR',
    'code_rent' => '',
     'type' => '1',
    'state'=>true
]);
DB::connection('tenant')->table('tables')->insert([
    'description1' => '999',
    'description2' => 'OTROS MEDIOS DE PAGO',
    'code_rent' => '',
     'type' => '1',
    'state'=>true
]);
// tabla2
DB::connection('tenant')->table('tables')->insert([
'description1' => '0',
'description2' => 'OTROS TIPOS DE DOCUMENTOS',
'code_rent' => '',
'type' => '2',
'state'=>true
]);
DB::connection('tenant')->table('tables')->insert([
'description1' => '1',
'description2' => 'DOCUMENTO NACIONAL DE IDENTIDAD (DNI)',
'code_rent' => '',
'type' => '2',
'state'=>true
]);
DB::connection('tenant')->table('tables')->insert([
'description1' => '4',
'description2' => 'CARNET DE EXTRANJERIA ',
'code_rent' => '',
'type' => '2',
'state'=>true
]);
DB::connection('tenant')->table('tables')->insert([
'description1' => '6',
'description2' => 'REGISTRO ÚNICO DE CONTRIBUYENTES',
'code_rent' => '',
'type' => '2',
'state'=>true
]);
DB::connection('tenant')->table('tables')->insert([
'description1' => '7',
'description2' => 'PASAPORTE',
'code_rent' => '',
'type' => '2',
'state'=>true
]);
DB::connection('tenant')->table('tables')->insert([
'description1' => 'A',
'description2' => 'CÉDULA DIPLOMÁTICA DE IDENTIDAD',
'code_rent' => '',
'type' => '2',
'state'=>true
]);
//tabla3
DB::connection('tenant')->table('tables')->insert([
'description1' => '01',
'description2' => 'CENTRAL RESERVA DEL PERU',
'code_rent' => '',
'type' => '3',
'state'=>true
]);
DB::connection('tenant')->table('tables')->insert([
'description1' => '02',
'description2' => 'DE CREDITO DEL PERU',
'code_rent' => '',
'type' => '3',
'state'=>true
]);
DB::connection('tenant')->table('tables')->insert([
'description1' => '03',
'description2' => 'INTERNACIONAL DEL PERU',
'code_rent' => '',
'type' => '3',
'state'=>true
]);
DB::connection('tenant')->table('tables')->insert([
'description1' => '05',
'description2' => 'LATINO',
'code_rent' => '',
'type' => '3',
'state'=>true
]);
DB::connection('tenant')->table('tables')->insert([
'description1' => '07',
'description2' => 'CITIBANK DEL PERU S.A.',
'code_rent' => '',
'type' => '3',
'state'=>true
]);
DB::connection('tenant')->table('tables')->insert([
'description1' => '08',
'description2' => 'STANDARD CHARTERED',
'code_rent' => '',
'type' => '3',
'state'=>true
]);
DB::connection('tenant')->table('tables')->insert([
'description1' => '09',
'description2' => 'SCOTIABANK PERU',
'code_rent' => '',
'type' => '3',
'state'=>true
]);
DB::connection('tenant')->table('tables')->insert([
'description1' => '11',
'description2' => 'CONTINENTAL',
'code_rent' => '',
'type' => '3',
'state'=>true
]);
DB::connection('tenant')->table('tables')->insert([
'description1' => '12',
'description2' => 'DE LIMA',
'code_rent' => '',
'type' => '3',
'state'=>true
]);
DB::connection('tenant')->table('tables')->insert([
'description1' => '16',
'description2' => 'MERCANTIL',
'code_rent' => '',
'type' => '3',
'state'=>true
]);
DB::connection('tenant')->table('tables')->insert([
'description1' => '18',
'description2' => 'NACION ',
'code_rent' => '',
'type' => '3',
'state'=>true
]);
DB::connection('tenant')->table('tables')->insert([
'description1' => '22',
'description2' => 'SANTANDER CENTRAL HISPANO',
'code_rent' => '',
'type' => '3',
'state'=>true
]);
DB::connection('tenant')->table('tables')->insert([
'description1' => '23',
'description2' => 'DE COMERCIO',
'code_rent' => '',
'type' => '3',
'state'=>true
]);
DB::connection('tenant')->table('tables')->insert([
'description1' => '25',
'description2' => 'REPUBLICA',
'code_rent' => '',
'type' => '3',
'state'=>true
]);
DB::connection('tenant')->table('tables')->insert([
'description1' => '26',
'description2' => 'NBK BANK',
'code_rent' => '',
'type' => '3',
'state'=>true
]);
DB::connection('tenant')->table('tables')->insert([
'description1' => '29',
'description2' => 'BANCOSUR',
'code_rent' => '',
'type' => '3',
'state'=>true
]);
DB::connection('tenant')->table('tables')->insert([
'description1' => '35',
'description2' => 'FINANCIERO DEL PERU',
'code_rent' => '',
'type' => '3',
'state'=>true
]);
DB::connection('tenant')->table('tables')->insert([
'description1' => '37',
'description2' => 'DEL PROGRESO',
'code_rent' => '',
'type' => '3',
'state'=>true
]);
DB::connection('tenant')->table('tables')->insert([
'description1' => '38',
'description2' => 'INTERAMERICANO FINANZAS',
'code_rent' => '',
'type' => '3',
'state'=>true
]);
DB::connection('tenant')->table('tables')->insert([
'description1' => '39',
'description2' => 'BANEX',
'code_rent' => '',
'type' => '3',
'state'=>true
]);
DB::connection('tenant')->table('tables')->insert([
'description1' => '40',
'description2' => 'NUEVO MUNDO',
'code_rent' => '',
'type' => '3',
'state'=>true
]);
DB::connection('tenant')->table('tables')->insert([
'description1' => '41',
'description2' => 'SUDAMERICANO',
'code_rent' => '',
'type' => '3',
'state'=>true
]);
DB::connection('tenant')->table('tables')->insert([
'description1' => '42',
'description2' => 'DEL LIBERTADOR',
'code_rent' => '',
'type' => '3',
'state'=>true
]);
DB::connection('tenant')->table('tables')->insert([
'description1' => '43',
'description2' => 'DEL TRABAJO',
'code_rent' => '',
'type' => '3',
'state'=>true
]);
DB::connection('tenant')->table('tables')->insert([
'description1' => '44',
'description2' => 'SOLVENTA',
'code_rent' => '',
'type' => '3',
'state'=>true
]);
DB::connection('tenant')->table('tables')->insert([
'description1' => '45',
'description2' => 'SERBANCO SA.',
'code_rent' => '',
'type' => '3',
'state'=>true
]);
DB::connection('tenant')->table('tables')->insert([
'description1' => '46',
'description2' => 'BANK OF BOSTON ',
'code_rent' => '',
'type' => '3',
'state'=>true
]);
DB::connection('tenant')->table('tables')->insert([
'description1' => '47',
'description2' => 'ORION',
'code_rent' => '',
'type' => '3',
'state'=>true
]);
DB::connection('tenant')->table('tables')->insert([
'description1' => '48',
'description2' => 'DEL PAIS',
'code_rent' => '',
'type' => '3',
'state'=>true
]);
DB::connection('tenant')->table('tables')->insert([
'description1' => '49',
'description2' => 'MI BANCO',
'code_rent' => '',
'type' => '3',
'state'=>true
]);
DB::connection('tenant')->table('tables')->insert([
'description1' => '50',
'description2' => 'BNP PARIBAS',
'code_rent' => '',
'type' => '3',
'state'=>true
]);
DB::connection('tenant')->table('tables')->insert([
'description1' => '51',
'description2' => 'AGROBANCO',
'code_rent' => '',
'type' => '3',
'state'=>true
]);
DB::connection('tenant')->table('tables')->insert([
'description1' => '53',
'description2' => 'HSBC BANK PERU S.A.',
'code_rent' => '',
'type' => '3',
'state'=>true
]);
DB::connection('tenant')->table('tables')->insert([
'description1' => '54',
'description2' => 'BANCO FALABELLA S.A.',
'code_rent' => '',
'type' => '3',
'state'=>true
]);
DB::connection('tenant')->table('tables')->insert([
'description1' => '55',
'description2' => 'BANCO RIPLEY',
'code_rent' => '',
'type' => '3',
'state'=>true
]);
DB::connection('tenant')->table('tables')->insert([
'description1' => '56',
'description2' => 'BANCO SANTANDER PERU S.A.',
'code_rent' => '',
'type' => '3',
'state'=>true
]);
DB::connection('tenant')->table('tables')->insert([
'description1' => '58',
'description2' => 'BANCO AZTECA DEL PERU',
'code_rent' => '',
'type' => '3',
'state'=>true
]);
DB::connection('tenant')->table('tables')->insert([
'description1' => '99',
'description2' => 'OTROS',
'code_rent' => '',
'type' => '3',
'state'=>true
]);
// tabla 4
 
DB::connection('tenant')->table('tables')->insert([
'description1' => 'PEN',
'description2' => 'Nuevo Sol o Sol',
'code_rent' => 'Perú',
'type' => '4',
'state'=>true
]);
 
DB::connection('tenant')->table('tables')->insert([
'description1' => 'USD',
'description2' => 'US Dollar',
'code_rent' => 'Estados Unidos (EEUU)',
'type' => '4',
'state'=>true
]);
 
// tabla 5
DB::connection('tenant')->table('tables')->insert([
'description1' => '01',
'description2' => 'MERCADERÍAS',
'code_rent' => '',
'type' => '5',
'state'=>true
]);
DB::connection('tenant')->table('tables')->insert([
'description1' => '02',
'description2' => 'PRODUCTOS TERMINADOS',
'code_rent' => '',
'type' => '5',
'state'=>true
]);
DB::connection('tenant')->table('tables')->insert([
'description1' => '03',
'description2' => 'MATERIAS PRIMAS',
'code_rent' => '',
'type' => '5',
'state'=>true
]);
DB::connection('tenant')->table('tables')->insert([
'description1' => '04',
'description2' => 'ENVASES',
'code_rent' => '',
'type' => '5',
'state'=>true
]);
DB::connection('tenant')->table('tables')->insert([
'description1' => '05',
'description2' => 'MATERIALES AUXILIARES',
'code_rent' => '',
'type' => '5',
'state'=>true
]);
DB::connection('tenant')->table('tables')->insert([
'description1' => '06',
'description2' => 'SUMINISTROS ',
'code_rent' => '',
'type' => '5',
'state'=>true
]);
DB::connection('tenant')->table('tables')->insert([
'description1' => '07',
'description2' => 'REPUESTOS',
'code_rent' => '',
'type' => '5',
'state'=>true
]);
DB::connection('tenant')->table('tables')->insert([
'description1' => '08',
'description2' => 'EMBALAJES',
'code_rent' => '',
'type' => '5',
'state'=>true
]);
DB::connection('tenant')->table('tables')->insert([
'description1' => '09',
'description2' => 'SUBPRODUCTOS',
'code_rent' => '',
'type' => '5',
'state'=>true
]);
DB::connection('tenant')->table('tables')->insert([
'description1' => '10',
'description2' => 'DESECHOS Y DESPERDICIOS',
'code_rent' => '',
'type' => '5',
'state'=>true
]);
DB::connection('tenant')->table('tables')->insert([
'description1' => '91',
'description2' => 'OTROS 1',
'code_rent' => '',
'type' => '5',
'state'=>true
]);
DB::connection('tenant')->table('tables')->insert([
'description1' => '92',
'description2' => 'OTROS 2',
'code_rent' => '',
'type' => '5',
'state'=>true
]);
DB::connection('tenant')->table('tables')->insert([
'description1' => '93',
'description2' => 'OTROS 3',
'code_rent' => '',
'type' => '5',
'state'=>true
]);
DB::connection('tenant')->table('tables')->insert([
'description1' => '94',
'description2' => 'OTROS 4',
'code_rent' => '',
'type' => '5',
'state'=>true
]);
DB::connection('tenant')->table('tables')->insert([
'description1' => '95',
'description2' => 'OTROS 5',
'code_rent' => '',
'type' => '5',
'state'=>true
]);
DB::connection('tenant')->table('tables')->insert([
'description1' => '96',
'description2' => 'OTROS 6',
'code_rent' => '',
'type' => '5',
'state'=>true
]);
DB::connection('tenant')->table('tables')->insert([
'description1' => '97',
'description2' => 'OTROS 7',
'code_rent' => '',
'type' => '5',
'state'=>true
]);
DB::connection('tenant')->table('tables')->insert([
'description1' => '98',
'description2' => 'OTROS 8',
'code_rent' => '',
'type' => '5',
'state'=>true
]);
DB::connection('tenant')->table('tables')->insert([
'description1' => '99',
'description2' => 'OTROS',
'code_rent' => '',
'type' => '5',
'state'=>true
]);
//tabla6
DB::connection('tenant')->table('tables')->insert([
'description1' => '4A',
'description2' => 'BOBINAS',
'code_rent' => '',
'type' => '6',
'state'=>true
]);
DB::connection('tenant')->table('tables')->insert([
'description1' => 'BJ',
'description2' => 'BALDE',
'code_rent' => '',
'type' => '6',
'state'=>true
]);
DB::connection('tenant')->table('tables')->insert([
'description1' => 'BLL',
'description2' => 'BARRILES',
'code_rent' => '',
'type' => '6',
'state'=>true
]);
DB::connection('tenant')->table('tables')->insert([
'description1' => 'BG',
'description2' => 'BOLSA',
'code_rent' => '',
'type' => '6',
'state'=>true
]);
DB::connection('tenant')->table('tables')->insert([
'description1' => 'BO',
'description2' => 'BOTELLAS',
'code_rent' => '',
'type' => '6',
'state'=>true
]);
DB::connection('tenant')->table('tables')->insert([
'description1' => 'BX',
'description2' => 'CAJA',
'code_rent' => '',
'type' => '6',
'state'=>true
]);
DB::connection('tenant')->table('tables')->insert([
'description1' => 'CT',
'description2' => 'CARTONES',
'code_rent' => '',
'type' => '6',
'state'=>true
]);
DB::connection('tenant')->table('tables')->insert([
'description1' => 'CMK',
'description2' => 'CENTIMETRO CUADRADO',
'code_rent' => '',
'type' => '6',
'state'=>true
]);
DB::connection('tenant')->table('tables')->insert([
'description1' => 'CMQ',
'description2' => 'CENTIMETRO CUBICO',
'code_rent' => '',
'type' => '6',
'state'=>true
]);
DB::connection('tenant')->table('tables')->insert([
'description1' => 'CMT',
'description2' => 'CENTIMETRO LINEAL',
'code_rent' => '',
'type' => '6',
'state'=>true
]);
DB::connection('tenant')->table('tables')->insert([
'description1' => 'CEN',
'description2' => 'CIENTO DE UNIDADES',
'code_rent' => '',
'type' => '6',
'state'=>true
]);
DB::connection('tenant')->table('tables')->insert([
'description1' => 'CY',
'description2' => 'CILINDRO',
'code_rent' => '',
'type' => '6',
'state'=>true
]);
DB::connection('tenant')->table('tables')->insert([
'description1' => 'CJ',
'description2' => 'CONOS',
'code_rent' => '',
'type' => '6',
'state'=>true
]);
DB::connection('tenant')->table('tables')->insert([
'description1' => 'DZN',
'description2' => 'DOCENA',
'code_rent' => '',
'type' => '6',
'state'=>true
]);
DB::connection('tenant')->table('tables')->insert([
'description1' => 'DZP',
'description2' => 'DOCENA POR 10**6',
'code_rent' => '',
'type' => '6',
'state'=>true
]);
DB::connection('tenant')->table('tables')->insert([
'description1' => 'BE',
'description2' => 'FARDO',
'code_rent' => '',
'type' => '6',
'state'=>true
]);
DB::connection('tenant')->table('tables')->insert([
'description1' => 'GLI',
'description2' => 'GALON INGLES (4,545956L)',
'code_rent' => '',
'type' => '6',
'state'=>true
]);
DB::connection('tenant')->table('tables')->insert([
'description1' => 'GRM',
'description2' => 'GRAMO',
'code_rent' => '',
'type' => '6',
'state'=>true
]);
DB::connection('tenant')->table('tables')->insert([
'description1' => 'GRO',
'description2' => 'GRUESA',
'code_rent' => '',
'type' => '6',
'state'=>true
]);
DB::connection('tenant')->table('tables')->insert([
'description1' => 'HLT',
'description2' => 'HECTOLITRO',
'code_rent' => '',
'type' => '6',
'state'=>true
]);
DB::connection('tenant')->table('tables')->insert([
'description1' => 'LEF',
'description2' => 'HOJA',
'code_rent' => '',
'type' => '6',
'state'=>true
]);
DB::connection('tenant')->table('tables')->insert([
'description1' => 'SET',
'description2' => 'JUEGO',
'code_rent' => '',
'type' => '6',
'state'=>true
]);
DB::connection('tenant')->table('tables')->insert([
'description1' => 'KGM',
'description2' => 'KILOGRAMO',
'code_rent' => '',
'type' => '6',
'state'=>true
]);
DB::connection('tenant')->table('tables')->insert([
'description1' => 'KTM',
'description2' => 'KILOMETRO',
'code_rent' => '',
'type' => '6',
'state'=>true
]);
DB::connection('tenant')->table('tables')->insert([
'description1' => 'KWH',
'description2' => 'KILOVATIO HORA',
'code_rent' => '',
'type' => '6',
'state'=>true
]);
DB::connection('tenant')->table('tables')->insert([
'description1' => 'KT',
'description2' => 'KIT',
'code_rent' => '',
'type' => '6',
'state'=>true
]);
DB::connection('tenant')->table('tables')->insert([
'description1' => 'CA',
'description2' => 'LATAS',
'code_rent' => '',
'type' => '6',
'state'=>true
]);
DB::connection('tenant')->table('tables')->insert([
'description1' => 'LBR',
'description2' => 'LIBRAS',
'code_rent' => '',
'type' => '6',
'state'=>true
]);
DB::connection('tenant')->table('tables')->insert([
'description1' => 'LTR',
'description2' => 'LITRO',
'code_rent' => '',
'type' => '6',
'state'=>true
]);
DB::connection('tenant')->table('tables')->insert([
'description1' => 'MWH',
'description2' => 'MEGAWATT HORA',
'code_rent' => '',
'type' => '6',
'state'=>true
]);
DB::connection('tenant')->table('tables')->insert([
'description1' => 'MTR',
'description2' => 'METRO',
'code_rent' => '',
'type' => '6',
'state'=>true
]);
DB::connection('tenant')->table('tables')->insert([
'description1' => 'MTK',
'description2' => 'METRO CUADRADO',
'code_rent' => '',
'type' => '6',
'state'=>true
]);
DB::connection('tenant')->table('tables')->insert([
'description1' => 'MTQ',
'description2' => 'METRO CUBICO',
'code_rent' => '',
'type' => '6',
'state'=>true
]);
DB::connection('tenant')->table('tables')->insert([
'description1' => 'MGM',
'description2' => 'MILIGRAMOS',
'code_rent' => '',
'type' => '6',
'state'=>true
]);
DB::connection('tenant')->table('tables')->insert([
'description1' => 'MLT',
'description2' => 'MILILITRO',
'code_rent' => '',
'type' => '6',
'state'=>true
]);
DB::connection('tenant')->table('tables')->insert([
'description1' => 'MMT',
'description2' => 'MILIMETRO',
'code_rent' => '',
'type' => '6',
'state'=>true
]);
DB::connection('tenant')->table('tables')->insert([
'description1' => 'MMK',
'description2' => 'MILIMETRO CUADRADO',
'code_rent' => '',
'type' => '6',
'state'=>true
]);
DB::connection('tenant')->table('tables')->insert([
'description1' => 'MMQ',
'description2' => 'MILIMETRO CUBICO',
'code_rent' => '',
'type' => '6',
'state'=>true
]);
DB::connection('tenant')->table('tables')->insert([
'description1' => 'MLL',
'description2' => 'MILLARES',
'code_rent' => '',
'type' => '6',
'state'=>true
]);
DB::connection('tenant')->table('tables')->insert([
'description1' => 'UM',
'description2' => 'MILLON DE UNIDADES',
'code_rent' => '',
'type' => '6',
'state'=>true
]);
DB::connection('tenant')->table('tables')->insert([
'description1' => 'ONZ',
'description2' => 'ONZAS',
'code_rent' => '',
'type' => '6',
'state'=>true
]);
DB::connection('tenant')->table('tables')->insert([
'description1' => 'PF',
'description2' => 'PALETAS',
'code_rent' => '',
'type' => '6',
'state'=>true
]);
DB::connection('tenant')->table('tables')->insert([
'description1' => 'PK',
'description2' => 'PAQUETE',
'code_rent' => '',
'type' => '6',
'state'=>true
]);
DB::connection('tenant')->table('tables')->insert([
'description1' => 'PR',
'description2' => 'PAR',
'code_rent' => '',
'type' => '6',
'state'=>true
]);
DB::connection('tenant')->table('tables')->insert([
'description1' => 'FOT',
'description2' => 'PIES',
'code_rent' => '',
'type' => '6',
'state'=>true
]);
DB::connection('tenant')->table('tables')->insert([
'description1' => 'FTK',
'description2' => 'PIES CUADRADOS',
'code_rent' => '',
'type' => '6',
'state'=>true
]);
DB::connection('tenant')->table('tables')->insert([
'description1' => 'FTQ',
'description2' => 'PIES CUBICOS',
'code_rent' => '',
'type' => '6',
'state'=>true
]);
DB::connection('tenant')->table('tables')->insert([
'description1' => 'C62',
'description2' => 'PIEZAS',
'code_rent' => '',
'type' => '6',
'state'=>true
]);
DB::connection('tenant')->table('tables')->insert([
'description1' => 'PG',
'description2' => 'PLACAS',
'code_rent' => '',
'type' => '6',
'state'=>true
]);
DB::connection('tenant')->table('tables')->insert([
'description1' => 'ST',
'description2' => 'PLIEGO',
'code_rent' => '',
'type' => '6',
'state'=>true
]);
DB::connection('tenant')->table('tables')->insert([
'description1' => 'INH',
'description2' => 'PULGADAS',
'code_rent' => '',
'type' => '6',
'state'=>true
]);
DB::connection('tenant')->table('tables')->insert([
'description1' => 'RM',
'description2' => 'RESMA',
'code_rent' => '',
'type' => '6',
'state'=>true
]);
DB::connection('tenant')->table('tables')->insert([
'description1' => 'DR',
'description2' => 'TAMBOR',
'code_rent' => '',
'type' => '6',
'state'=>true
]);
DB::connection('tenant')->table('tables')->insert([
'description1' => 'STN',
'description2' => 'TONELADA CORTA',
'code_rent' => '',
'type' => '6',
'state'=>true
]);
DB::connection('tenant')->table('tables')->insert([
'description1' => 'LTN',
'description2' => 'TONELADA LARGA',
'code_rent' => '',
'type' => '6',
'state'=>true
]);
DB::connection('tenant')->table('tables')->insert([
'description1' => 'TNE',
'description2' => 'TONELADAS',
'code_rent' => '',
'type' => '6',
'state'=>true
]);
DB::connection('tenant')->table('tables')->insert([
'description1' => 'TU',
'description2' => 'TUBOS',
'code_rent' => '',
'type' => '6',
'state'=>true
]);
DB::connection('tenant')->table('tables')->insert([
'description1' => 'NIU',
'description2' => 'UNIDAD (BIENES)',
'code_rent' => '',
'type' => '6',
'state'=>true
]);
DB::connection('tenant')->table('tables')->insert([
'description1' => 'ZZ',
'description2' => 'UNIDAD (SERVICIOS)',
'code_rent' => '',
'type' => '6',
'state'=>true
]);
DB::connection('tenant')->table('tables')->insert([
'description1' => 'GLL',
'description2' => 'US GALON (3,7843 L)',
'code_rent' => '',
'type' => '6',
'state'=>true
]);
DB::connection('tenant')->table('tables')->insert([
'description1' => 'YRD',
'description2' => 'YARDA',
'code_rent' => '',
'type' => '6',
'state'=>true
]);
DB::connection('tenant')->table('tables')->insert([
'description1' => 'YDK',
'description2' => 'YARDA CUADRADA',
'code_rent' => '',
'type' => '6',
'state'=>true
]);
//Tabla 8
DB::connection('tenant')->table('tables')->insert([
    'code_rent' => '1',
    'description1' => 'LIBRO CAJA Y BANCOS',    
    'type' => '8',
    'state'=>true
]);
DB::connection('tenant')->table('tables')->insert([
    'code_rent' => '2',
    'description1' => 'LIBRO DE INGRESOS Y GASTOS',    
    'type' => '8',
    'state'=>true
]);
DB::connection('tenant')->table('tables')->insert([
    'code_rent' => '3',
    'description1' => 'LIBRO DE INVENTARIOS Y BALANCES',    
    'type' => '8',
    'state'=>true
]);
DB::connection('tenant')->table('tables')->insert([
    'code_rent' => '4',
    'description1' => 'LIBRO DE RETENCIONES',    
    'type' => '8',
    'state'=>true
]);
DB::connection('tenant')->table('tables')->insert([
    'code_rent' => '5',
    'description1' => 'LIBRO DIARIO',    
    'type' => '8',
    'state'=>true
]);
DB::connection('tenant')->table('tables')->insert([
    'code_rent' => '6',
    'description1' => 'LIBRO MAYOR',    
    'type' => '8',
    'state'=>true
]);
DB::connection('tenant')->table('tables')->insert([
    'code_rent' => '7',
    'description1' => 'REGISTRO DE ACTIVOS FIJOS',    
    'type' => '8',
    'state'=>true
]);
DB::connection('tenant')->table('tables')->insert([
    'code_rent' => '8',
    'description1' => 'REGISTRO DE COMPRAS',    
    'type' => '8',
    'state'=>true
]);
DB::connection('tenant')->table('tables')->insert([
    'code_rent' => '9',
    'description1' => 'REGISTRO DE CONSIGNACIONES',    
    'type' => '8',
    'state'=>true
]);
DB::connection('tenant')->table('tables')->insert([
    'code_rent' => '10',
    'description1' => 'REGISTRO DE COSTOS',    
    'type' => '8',
    'state'=>true
]);
DB::connection('tenant')->table('tables')->insert([
    'code_rent' => '11',
    'description1' => 'REGISTRO DE HUÉSPEDES',    
    'type' => '8',
    'state'=>true
]);
DB::connection('tenant')->table('tables')->insert([
    'code_rent' => '12',
    'description1' => 'REGISTRO DE INVENTARIO',    
    'type' => '8',
    'state'=>true
]);
DB::connection('tenant')->table('tables')->insert([
    'code_rent' => '13',
    'description1' => 'REGISTRO DE INVENTARIO PERMANENTE VALORIZADO',    
    'type' => '8',
    'state'=>true
]);
DB::connection('tenant')->table('tables')->insert([
    'code_rent' => '14',
    'description1' => 'REGISTRO DE VENTAS E INGRESOS',    
    'type' => '8',
    'state'=>true
]);
DB::connection('tenant')->table('tables')->insert([
    'code_rent' => '15',
    'description1' => 'REGISTRO DE VENTAS E INGRESOS',    
    'type' => '8',
    'state'=>true
]);
DB::connection('tenant')->table('tables')->insert([
    'code_rent' => '16',
    'description1' => 'REGISTRO DEL RÉGIMEN DE PERCEPCIONES',    
    'type' => '8',
    'state'=>true
]);
DB::connection('tenant')->table('tables')->insert([
    'code_rent' => '17',
    'description1' => 'REGISTRO DEL RÉGIMEN DE RETENCIONES',    
    'type' => '8',
    'state'=>true
]);
DB::connection('tenant')->table('tables')->insert([
    'code_rent' => '18',
    'description1' => 'REGISTRO IVAP',    
    'type' => '8',
    'state'=>true
]);
DB::connection('tenant')->table('tables')->insert([
    'code_rent' => '19',
    'description1' => 'REGISTRO(S) AUXILIAR(ES) DE ADQUISICIONES - ARTÍCULO 8° RESOLUCIÓN DE SUPERINTENDENCIA N° 022-98/SUNAT',    
    'type' => '8',
    'state'=>true
]);
DB::connection('tenant')->table('tables')->insert([
    'code_rent' => '20',
    'description1' => 'REGISTRO(S) AUXILIAR(ES) DE ADQUISICIONES - INCISO A) PRIMER PÁRRAFO ARTÍCULO 5° RESOLUCIÓN DE SUPERINTENDENCIA N° 021-99/SUNAT',    
    'type' => '8',
    'state'=>true
]);
DB::connection('tenant')->table('tables')->insert([
    'code_rent' => '21',
    'description1' => 'REGISTRO(S) AUXILIAR(ES) DE ADQUISICIONES - INCISO A) PRIMER PÁRRAFO ARTÍCULO 5° RESOLUCIÓN DE SUPERINTENDENCIA N° 142-2001/SUNAT',    
    'type' => '8',
    'state'=>true
]);
DB::connection('tenant')->table('tables')->insert([
    'code_rent' => '22',
    'description1' => 'REGISTRO(S) AUXILIAR(ES) DE ADQUISICIONES - INCISO C) PRIMER PÁRRAFO ARTÍCULO 5° RESOLUCIÓN DE SUPERINTENDENCIA N° 256-2004/SUNAT',    
    'type' => '8',
    'state'=>true
]);
DB::connection('tenant')->table('tables')->insert([
    'code_rent' => '23',
    'description1' => 'REGISTRO(S) AUXILIAR(ES) DE ADQUISICIONES - INCISO A) PRIMER PÁRRAFO ARTÍCULO 5° RESOLUCIÓN DE SUPERINTENDENCIA N° 257-2004/SUNAT',    
    'type' => '8',
    'state'=>true
]);
DB::connection('tenant')->table('tables')->insert([
    'code_rent' => '24',
    'description1' => 'REGISTRO(S) AUXILIAR(ES) DE ADQUISICIONES - INCISO C) PRIMER PÁRRAFO ARTÍCULO 5° RESOLUCIÓN DE SUPERINTENDENCIA N° 258-2004/SUNAT',    
    'type' => '8',
    'state'=>true
]);
DB::connection('tenant')->table('tables')->insert([
    'code_rent' => '25',
    'description1' => 'REGISTRO(S) AUXILIAR(ES) DE ADQUISICIONES - INCISO A) PRIMER PÁRRAFO ARTÍCULO 5° RESOLUCIÓN DE SUPERINTENDENCIA N° 259-2004/SUNAT',    
    'type' => '8',
    'state'=>true
]);
DB::connection('tenant')->table('tables')->insert([
    'code_rent' => '26',
    'description1' => 'REGISTRO DE RETENCIONES ARTÍCULO 77-A DE LA LEY DEL IMPUESTO A LA RENTA',    
    'type' => '8',
    'state'=>true
]);
DB::connection('tenant')->table('tables')->insert([
    'code_rent' => '27',
    'description1' => 'LIBRO DE ACTAS DE LA EMPRESA INDIVIDUAL DE RESPONSABILIDAD LIMITADA',    
    'type' => '8',
    'state'=>true
]);
DB::connection('tenant')->table('tables')->insert([
    'code_rent' => '28',
    'description1' => 'LIBRO DE ACTAS DE LA JUNTA GENERAL DE ACCIONISTAS',    
    'type' => '8',
    'state'=>true
]);
DB::connection('tenant')->table('tables')->insert([
    'code_rent' => '29',
    'description1' => 'LIBRO DE ACTAS DEL DIRECTORIO',    
    'type' => '8',
    'state'=>true
]);
DB::connection('tenant')->table('tables')->insert([
    'code_rent' => '30',
    'description1' => 'LIBRO DE MATRÍCULA DE ACCIONES',    
    'type' => '8',
    'state'=>true
]);
DB::connection('tenant')->table('tables')->insert([
    'code_rent' => '31',
    'description1' => 'LIBRO DE PLANILLAS',    
    'type' => '8',
    'state'=>true
]);
//tabla 9

//tabla 10
DB::connection('tenant')->table('tables')->insert([
'description1' => '00',
'description2' => 'Otros',
'code_rent' => '',
'type' => '10',
'state'=>true
]);
DB::connection('tenant')->table('tables')->insert([
'description1' => '01',
'description2' => 'Factura',
'code_rent' => '',
'type' => '10',
'state'=>true
]);
DB::connection('tenant')->table('tables')->insert([
'description1' => '02',
'description2' => 'Recibo por Honorarios',
'code_rent' => '',
'type' => '10',
'state'=>true
]);
DB::connection('tenant')->table('tables')->insert([
'description1' => '03',
'description2' => 'Boleta de Venta',
'code_rent' => '',
'type' => '10',
'state'=>true
]);
DB::connection('tenant')->table('tables')->insert([
'description1' => '04',
'description2' => 'Liquidación de compra',
'code_rent' => '',
'type' => '10',
'state'=>true
]);
DB::connection('tenant')->table('tables')->insert([
'description1' => '05',
'description2' => 'Boletos de Transporte Aéreo',
'code_rent' => '',
'type' => '10',
'state'=>true
]);
DB::connection('tenant')->table('tables')->insert([
'description1' => '06',
'description2' => 'Carta de porte aéreo por el servicio de transporte de carga aérea',
'code_rent' => '',
'type' => '10',
'state'=>true
]);
DB::connection('tenant')->table('tables')->insert([
'description1' => '07',
'description2' => 'Nota de crédito',
'code_rent' => '',
'type' => '10',
'state'=>true
]);
DB::connection('tenant')->table('tables')->insert([
'description1' => '08',
'description2' => 'Nota de débito',
'code_rent' => '',
'type' => '10',
'state'=>true
]);
DB::connection('tenant')->table('tables')->insert([
'description1' => '09',
'description2' => 'Guía de remisión-Remitente',
'code_rent' => '',
'type' => '10',
'state'=>true
]);
DB::connection('tenant')->table('tables')->insert([
'description1' => '10',
'description2' => 'Recibo por Arrendamiento',
'code_rent' => '',
'type' => '10',
'state'=>true
]);
DB::connection('tenant')->table('tables')->insert([
'description1' => '11',
'description2' => 'Póliza emitida por las Bolsas de Valores',
'code_rent' => '',
'type' => '10',
'state'=>true
]);
DB::connection('tenant')->table('tables')->insert([
'description1' => '12',
'description2' => 'Ticket o cinta emitido por máquina registradora',
'code_rent' => '',
'type' => '10',
'state'=>true
]);
DB::connection('tenant')->table('tables')->insert([
'description1' => '13',
'description2' => 'Documentos emitidos por las empresas',
'code_rent' => '',
'type' => '10',
'state'=>true
]);
DB::connection('tenant')->table('tables')->insert([
'description1' => '14',
'description2' => 'Recibo por servicios públicos',
'code_rent' => '',
'type' => '10',
'state'=>true
]);
DB::connection('tenant')->table('tables')->insert([
'description1' => '15',
'description2' => 'Boletos emitidos por el servicio de transporte ',
'code_rent' => '',
'type' => '10',
'state'=>true
]);
DB::connection('tenant')->table('tables')->insert([
'description1' => '16',
'description2' => 'Boletos de viaje  de transporte nacional',
'code_rent' => '',
'type' => '10',
'state'=>true
]);
DB::connection('tenant')->table('tables')->insert([
'description1' => '17',
'description2' => 'Documento emitido por la Iglesia Católica',
'code_rent' => '',
'type' => '10',
'state'=>true
]);
DB::connection('tenant')->table('tables')->insert([
'description1' => '18',
'description2' => 'Documento emitido por Fondo de Pensiones',
'code_rent' => '',
'type' => '10',
'state'=>true
]);
DB::connection('tenant')->table('tables')->insert([
'description1' => '19',
'description2' => 'Boleto o entrada espectáculos públicos',
'code_rent' => '',
'type' => '10',
'state'=>true
]);
DB::connection('tenant')->table('tables')->insert([
'description1' => '20',
'description2' => 'Comprobante de Retención',
'code_rent' => '',
'type' => '10',
'state'=>true
]);
DB::connection('tenant')->table('tables')->insert([
'description1' => '21',
'description2' => 'Conocimiento  de transporte de carga marítima',
'code_rent' => '',
'type' => '10',
'state'=>true
]);
DB::connection('tenant')->table('tables')->insert([
'description1' => '22',
'description2' => 'Comprobante por Operaciones No Habituales',
'code_rent' => '',
'type' => '10',
'state'=>true
]);
DB::connection('tenant')->table('tables')->insert([
'description1' => '23',
'description2' => 'Pólizas de Adjudicación emitidas',
'code_rent' => '',
'type' => '10',
'state'=>true
]);
DB::connection('tenant')->table('tables')->insert([
'description1' => '24',
'description2' => 'Certificado de pago de regalías PERUPETRO S.A',
'code_rent' => '',
'type' => '10',
'state'=>true
]);
DB::connection('tenant')->table('tables')->insert([
'description1' => '25',
'description2' => 'Documento de Atribución ISC',
'code_rent' => '',
'type' => '10',
'state'=>true
]);
DB::connection('tenant')->table('tables')->insert([
'description1' => '26',
'description2' => 'Recibo por el Pago de Agua',
'code_rent' => '',
'type' => '10',
'state'=>true
]);
DB::connection('tenant')->table('tables')->insert([
'description1' => '27',
'description2' => 'Seguro Complementario de Trabajo de Riesgo',
'code_rent' => '',
'type' => '10',
'state'=>true
]);

DB::connection('tenant')->table('tables')->insert([
'description1' => '31',
'description2' => 'Guía de Remisión-Transportista',
'code_rent' => '',
'type' => '10',
'state'=>true
]);

DB::connection('tenant')->table('tables')->insert([
'description1' => '33',
'description2' => 'Manifiesto de Pasajeros',
'code_rent' => '',
'type' => '10',
'state'=>true
]);
DB::connection('tenant')->table('tables')->insert([
'description1' => '34',
'description2' => 'Documento del Operador',
'code_rent' => '',
'type' => '10',
'state'=>true
]);
DB::connection('tenant')->table('tables')->insert([
'description1' => '35',
'description2' => 'Documento del Partícipe',
'code_rent' => '',
'type' => '10',
'state'=>true
]);
DB::connection('tenant')->table('tables')->insert([
'description1' => '36',
'description2' => 'Recibo de Distribución de Gas Natural',
'code_rent' => '',
'type' => '10',
'state'=>true
]);

DB::connection('tenant')->table('tables')->insert([
'description1' => '40',
'description2' => 'Comprobante de Percepción',
'code_rent' => '',
'type' => '10',
'state'=>true
]);
DB::connection('tenant')->table('tables')->insert([
'description1' => '41',
'description2' => 'Comprobante de Percepción - Venta interna',
'code_rent' => '',
'type' => '10',
'state'=>true
]);

DB::connection('tenant')->table('tables')->insert([
'description1' => '43',
'description2' => 'Boletos emitidos por las Compañías de Aviación Comercial',
'code_rent' => '',
'type' => '10',
'state'=>true
]);
DB::connection('tenant')->table('tables')->insert([
'description1' => '44',
'description2' => 'Billetes de lotería, rifas y apuestas.',
'code_rent' => '',
'type' => '10',
'state'=>true
]);

DB::connection('tenant')->table('tables')->insert([
'description1' => '46',
'description2' => 'Formulario de Declaración - pago o Boleta de pago de tributos Internos',
'code_rent' => '',
'type' => '10',
'state'=>true
]);
DB::connection('tenant')->table('tables')->insert([
'description1' => '48',
'description2' => 'Comprobante de Operaciones-Ley N° 29972',
'code_rent' => '',
'type' => '10',
'state'=>true
]);
DB::connection('tenant')->table('tables')->insert([
'description1' => '49',
'description2' => 'Constancia de Depósito-IVAP (Ley 28211)',
'code_rent' => '',
'type' => '10',
'state'=>true
]);
DB::connection('tenant')->table('tables')->insert([
'description1' => '50',
'description2' => 'Declaración Única de Aduanas-Importación definitiva',
'code_rent' => '',
'type' => '10',
'state'=>true
]);
DB::connection('tenant')->table('tables')->insert([
'description1' => '51',
'description2' => 'Póliza o DUI Fraccionada',
'code_rent' => '',
'type' => '10',
'state'=>true
]);
DB::connection('tenant')->table('tables')->insert([
'description1' => '52',
'description2' => 'Despacho Simplificado-Importación Simplificada',
'code_rent' => '',
'type' => '10',
'state'=>true
]);
DB::connection('tenant')->table('tables')->insert([
'description1' => '53',
'description2' => 'Declaración de Mensajería o Courier',
'code_rent' => '',
'type' => '10',
'state'=>true
]);
DB::connection('tenant')->table('tables')->insert([
'description1' => '54',
'description2' => 'Liquidación de Cobranza',
'code_rent' => '',
'type' => '10',
'state'=>true
]);
DB::connection('tenant')->table('tables')->insert([
'description1' => '55',
'description2' => 'BVME para transporte ferroviario de pasajeros',
'code_rent' => '',
'type' => '10',
'state'=>true
]);
DB::connection('tenant')->table('tables')->insert([
'description1' => '56',
'description2' => 'Comprobante de pago SEAE',
'code_rent' => '',
'type' => '10',
'state'=>true
]);
DB::connection('tenant')->table('tables')->insert([
'description1' => '87',
'description2' => 'Nota de Crédito Especial',
'code_rent' => '',
'type' => '10',
'state'=>true
]);
DB::connection('tenant')->table('tables')->insert([
'description1' => '88',
'description2' => 'Nota de Débito Especial',
'code_rent' => '',
'type' => '10',
'state'=>true
]);
DB::connection('tenant')->table('tables')->insert([
'description1' => '89',
'description2' => 'Nota de Ajuste de Operaciones-Ley N° 29972',
'code_rent' => '',
'type' => '10',
'state'=>true
]);
DB::connection('tenant')->table('tables')->insert([
'description1' => '91',
'description2' => 'Comprobante de No Domiciliado',
'code_rent' => '',
'type' => '10',
'state'=>true
]);
DB::connection('tenant')->table('tables')->insert([
'description1' => '96',
'description2' => 'Exceso de crédito fiscal por retiro de bienes',
'code_rent' => '',
'type' => '10',
'state'=>true
]);
DB::connection('tenant')->table('tables')->insert([
'description1' => '97',
'description2' => 'Nota de Crédito-No Domiciliado',
'code_rent' => '',
'type' => '10',
'state'=>true
]);
DB::connection('tenant')->table('tables')->insert([
'description1' => '98',
'description2' => 'Nota de Débito-No Domiciliado',
'code_rent' => '',
'type' => '10',
'state'=>true
]);
// tabla 11
DB::connection('tenant')->table('tables')->insert([
'description1' => '19',
'description2' => 'TUMBES',
'code_rent' => '',
'type' => '11',
'state'=>true
]);
DB::connection('tenant')->table('tables')->insert([
'description1' => '28',
'description2' => 'TALARA',
'code_rent' => '',
'type' => '11',
'state'=>true
]);
DB::connection('tenant')->table('tables')->insert([
'description1' => '46',
'description2' => 'PAITA',
'code_rent' => '',
'type' => '11',
'state'=>true
]);
DB::connection('tenant')->table('tables')->insert([
'description1' => '55',
'description2' => 'CHICLAYO',
'code_rent' => '',
'type' => '11',
'state'=>true
]);
DB::connection('tenant')->table('tables')->insert([
'description1' => '82',
'description2' => 'SALAVERRY',
'code_rent' => '',
'type' => '11',
'state'=>true
]);
DB::connection('tenant')->table('tables')->insert([
'description1' => '91',
'description2' => 'CHIMBOTE',
'code_rent' => '',
'type' => '11',
'state'=>true
]);
DB::connection('tenant')->table('tables')->insert([
'description1' => '118',
'description2' => 'TUMBES',
'code_rent' => '',
'type' => '11',
'state'=>true
]);
DB::connection('tenant')->table('tables')->insert([
'description1' => '127',
'description2' => 'PISCO',
'code_rent' => '',
'type' => '11',
'state'=>true
]);
DB::connection('tenant')->table('tables')->insert([
'description1' => '145',
'description2' => 'MOLLENDO MATARANI',
'code_rent' => '',
'type' => '11',
'state'=>true
]);
DB::connection('tenant')->table('tables')->insert([
'description1' => '154',
'description2' => 'AREQUIPA',
'code_rent' => '',
'type' => '11',
'state'=>true
]);
DB::connection('tenant')->table('tables')->insert([
'description1' => '163',
'description2' => 'ILO',
'code_rent' => '',
'type' => '11',
'state'=>true
]);
DB::connection('tenant')->table('tables')->insert([
'description1' => '172',
'description2' => 'TACNA',
'code_rent' => '',
'type' => '11',
'state'=>true
]);
DB::connection('tenant')->table('tables')->insert([
'description1' => '181',
'description2' => 'PUNO',
'code_rent' => '',
'type' => '11',
'state'=>true
]);
DB::connection('tenant')->table('tables')->insert([
'description1' => '190',
'description2' => 'CUZCO',
'code_rent' => '',
'type' => '11',
'state'=>true
]);
DB::connection('tenant')->table('tables')->insert([
'description1' => '217',
'description2' => 'PUCALLPA',
'code_rent' => '',
'type' => '11',
'state'=>true
]);
DB::connection('tenant')->table('tables')->insert([
'description1' => '226',
'description2' => 'IQUITOS',
'code_rent' => '',
'type' => '11',
'state'=>true
]);
DB::connection('tenant')->table('tables')->insert([
'description1' => '235',
'description2' => 'AÉREA DEL CALLAO',
'code_rent' => '',
'type' => '11',
'state'=>true
]);
DB::connection('tenant')->table('tables')->insert([
'description1' => '244',
'description2' => 'POSTAL DE LIMA',
'code_rent' => '',
'type' => '11',
'state'=>true
]);
DB::connection('tenant')->table('tables')->insert([
'description1' => '262',
'description2' => 'DESAGUADERO',
'code_rent' => '',
'type' => '11',
'state'=>true
]);
DB::connection('tenant')->table('tables')->insert([
'description1' => '271',
'description2' => 'TARAPOTO',
'code_rent' => '',
'type' => '11',
'state'=>true
]);
DB::connection('tenant')->table('tables')->insert([
'description1' => '280',
'description2' => 'PUERTO MALDONADO',
'code_rent' => '',
'type' => '11',
'state'=>true
]);
DB::connection('tenant')->table('tables')->insert([
'description1' => '299',
'description2' => 'LA TINA',
'code_rent' => '',
'type' => '11',
'state'=>true
]);
DB::connection('tenant')->table('tables')->insert([
'description1' => '884',
'description2' => 'DEPENDENCIA FERROVIARIA TACNA',
'code_rent' => '',
'type' => '11',
'state'=>true
]);
DB::connection('tenant')->table('tables')->insert([
'description1' => '893',
'description2' => 'DEPENDENCIA POSTAL TACNA',
'code_rent' => '',
'type' => '11',
'state'=>true
]);
DB::connection('tenant')->table('tables')->insert([
'description1' => '910',
'description2' => 'DEPENDENCIA POSTAL AREQUIPA',
'code_rent' => '',
'type' => '11',
'state'=>true
]);
DB::connection('tenant')->table('tables')->insert([
'description1' => '929',
'description2' => 'COMPLEJO FRONTERIZO STA ROSA TACNA',
'code_rent' => '',
'type' => '11',
'state'=>true
]);
DB::connection('tenant')->table('tables')->insert([
'description1' => '938',
'description2' => 'TERMINAL TERRESTRE TACNA',
'code_rent' => '',
'type' => '11',
'state'=>true
]);
DB::connection('tenant')->table('tables')->insert([
'description1' => '947',
'description2' => 'AEROPUERTO TACNA',
'code_rent' => '',
'type' => '11',
'state'=>true
]);
DB::connection('tenant')->table('tables')->insert([
'description1' => '956',
'description2' => 'CETICOS TACNA',
'code_rent' => '',
'type' => '11',
'state'=>true
]);
DB::connection('tenant')->table('tables')->insert([
'description1' => '965',
'description2' => 'DEPENDENCIA POSTAL DE SALAVERRY',
'code_rent' => '',
'type' => '11',
'state'=>true
]);
//tabla 12
DB::connection('tenant')->table('tables')->insert([
'description1' => '01',
'description2' => 'VENTA NACIONAL',
'code_rent'    => '',
'type'        => '12',
'state'        => true
]);
DB::connection('tenant')->table('tables')->insert([
'description1' => '02',
'description2' => 'COMPRA NACIONAL',
'code_rent'    => '',
'type'        => '12',
'state'        => true
]);
DB::connection('tenant')->table('tables')->insert([
'description1' => '03',
'description2' => 'CONSIGNACIÓN RECIBIDA',
'code_rent'    => '',
'type'        => '12',
'state'        => true
]);
DB::connection('tenant')->table('tables')->insert([
'description1' => '04',
'description2' => 'CONSIGNACIÓN ENTREGADA',
'code_rent'    => '',
'type'        => '12',
'state'        => true
]);
DB::connection('tenant')->table('tables')->insert([
'description1' => '05',
'description2' => 'DEVOLUCIÓN RECIBIDA',
'code_rent'    => '',
'type'        => '12',
'state'        => true
]);
DB::connection('tenant')->table('tables')->insert([
'description1' => '06',
'description2' => 'DEVOLUCIÓN ENTREGADA',
'code_rent'    => '',
'type'        => '12',
'state'        => true
]);
DB::connection('tenant')->table('tables')->insert([
'description1' => '07',
'description2' => 'BONIFICACIÓN',
'code_rent'    => '',
'type'        => '12',
'state'        => true
]);
DB::connection('tenant')->table('tables')->insert([
'description1' => '08',
'description2' => 'PREMIO',
'code_rent'    => '',
'type'        => '12',
'state'        => true
]);
DB::connection('tenant')->table('tables')->insert([
'description1' => '09',
'description2' => 'DONACIÓN',
'code_rent'    => '',
'type'        => '12',
'state'        => true
]);
DB::connection('tenant')->table('tables')->insert([
'description1' => '10',
'description2' => 'SALIDA A PRODUCCIÓN',
'code_rent'    => '',
'type'        => '12',
'state'        => true
]);
DB::connection('tenant')->table('tables')->insert([
'description1' => '11',
'description2' => 'SALIDA POR TRANSFERENCIA ENTRE ALMACENES',
'code_rent'    => '',
'type'        => '12',
'state'        => true
]);
DB::connection('tenant')->table('tables')->insert([
'description1' => '12',
'description2' => 'RETIRO',
'code_rent'    => '',
'type'        => '12',
'state'        => true
]);
DB::connection('tenant')->table('tables')->insert([
'description1' => '13',
'description2' => 'MERMAS',
'code_rent'    => '',
'type'        => '12',
'state'        => true
]);
DB::connection('tenant')->table('tables')->insert([
'description1' => '14',
'description2' => 'DESMEDROS',
'code_rent'    => '',
'type'        => '12',
'state'        => true
]);
DB::connection('tenant')->table('tables')->insert([
'description1' => '15',
'description2' => 'DESTRUCCIÓN',
'code_rent'    => '',
'type'        => '12',
'state'        => true
]);
DB::connection('tenant')->table('tables')->insert([
'description1' => '16',
'description2' => 'SALDO INICIAL',
'code_rent'    => '',
'type'        => '12',
'state'        => true
]);
DB::connection('tenant')->table('tables')->insert([
'description1' => '17',
'description2' => 'EXPORTACIÓN',
'code_rent'    => '',
'type'        => '12',
'state'        => true
]);
DB::connection('tenant')->table('tables')->insert([
'description1' => '18',
'description2' => 'IMPORTACIÓN',
'code_rent'    => '',
'type'        => '12',
'state'        => true
]);
DB::connection('tenant')->table('tables')->insert([
'description1' => '19',
'description2' => 'ENTRADA DE PRODUCCIÓN',
'code_rent'    => '',
'type'        => '12',
'state'        => true
]);
DB::connection('tenant')->table('tables')->insert([
'description1' => '20',
'description2' => 'ENTRADA POR DEVOLUCIÓN DE PRODUCCIÓN',
'code_rent'    => '',
'type'        => '12',
'state'        => true
]);
DB::connection('tenant')->table('tables')->insert([
'description1' => '21',
'description2' => 'ENTRADA POR TRANSFERENCIA ENTRE ALMACENES',
'code_rent'    => '',
'type'        => '12',
'state'        => true
]);
DB::connection('tenant')->table('tables')->insert([
'description1' => '22',
'description2' => 'ENTRADA POR IDENTIFICACIÓN ERRONEA',
'code_rent'    => '',
'type'        => '12',
'state'        => true
]);
DB::connection('tenant')->table('tables')->insert([
'description1' => '23',
'description2' => 'SALIDA POR IDENTIFICACIÓN ERRONEA',
'code_rent'    => '',
'type'        => '12',
'state'        => true
]);
DB::connection('tenant')->table('tables')->insert([
'description1' => '24',
'description2' => 'ENTRADA POR DEVOLUCIÓN DEL CLIENTE',
'code_rent'    => '',
'type'        => '12',
'state'        => true
]);
DB::connection('tenant')->table('tables')->insert([
'description1' => '25',
'description2' => 'SALIDA POR DEVOLUCIÓN AL PROVEEDOR',
'code_rent'    => '',
'type'        => '12',
'state'        => true
]);
DB::connection('tenant')->table('tables')->insert([
'description1' => '26',
'description2' => 'ENTRADA PARA SERVICIO DE PRODUCCIÓN',
'code_rent'    => '',
'type'        => '12',
'state'        => true
]);
DB::connection('tenant')->table('tables')->insert([
'description1' => '27',
'description2' => 'SALIDA POR SERVICIO DE PRODUCCIÓN',
'code_rent'    => '',
'type'        => '12',
'state'        => true
]);
DB::connection('tenant')->table('tables')->insert([
'description1' => '28',
'description2' => 'AJUSTE POR DIFERENCIA DE INVENTARIO',
'code_rent'    => '',
'type'        => '12',
'state'        => true
]);
DB::connection('tenant')->table('tables')->insert([
'description1' => '29',
'description2' => 'ENTRADA DE BIENES EN PRÉSTAMO',
'code_rent'    => '',
'type'        => '12',
'state'        => true
]);
DB::connection('tenant')->table('tables')->insert([
'description1' => '30',
'description2' => 'SALIDA DE BIENES EN PRÉSTAMO',
'code_rent'    => '',
'type'        => '12',
'state'        => true
]);
DB::connection('tenant')->table('tables')->insert([
'description1' => '31',
'description2' => 'ENTRADA DE BIENES EN CUSTODIA',
'code_rent'    => '',
'type'        => '12',
'state'        => true
]);
DB::connection('tenant')->table('tables')->insert([
'description1' => '32',
'description2' => 'SALIDA DE BIENES EN CUSTODIA',
'code_rent'    => '',
'type'        => '12',
'state'        => true
]);
DB::connection('tenant')->table('tables')->insert([
'description1' => '33',
'description2' => 'MUESTRAS MÉDICAS',
'code_rent'    => '',
'type'        => '12',
'state'        => true
]);
DB::connection('tenant')->table('tables')->insert([
'description1' => '34',
'description2' => 'PUBLICIDAD',
'code_rent'    => '',
'type'        => '12',
'state'        => true
]);
DB::connection('tenant')->table('tables')->insert([
'description1' => '35',
'description2' => 'GASTOS DE REPRESENTACIÓN',
'code_rent'    => '',
'type'        => '12',
'state'        => true
]);
DB::connection('tenant')->table('tables')->insert([
'description1' => '36',
'description2' => 'RETIRO PARA ENTREGA A TRABAJADORES',
'code_rent'    => '',
'type'        => '12',
'state'        => true
]);
DB::connection('tenant')->table('tables')->insert([
'description1' => '37',
'description2' => 'RETIRO POR CONVENIO COLECTIVO',
'code_rent'    => '',
'type'        => '12',
'state'        => true
]);
DB::connection('tenant')->table('tables')->insert([
'description1' => '38',
'description2' => 'RETIRO POR SUSTITUCIÓN DE BIEN SINIESTRADO',
'code_rent'    => '',
'type'        => '12',
'state'        => true
]);
DB::connection('tenant')->table('tables')->insert([
'description1' => '91',
'description2' => 'OTROS 1',
'code_rent'    => '',
'type'        => '12',
'state'        => true
]);
DB::connection('tenant')->table('tables')->insert([
'description1' => '92',
'description2' => 'OTROS 2',
'code_rent'    => '',
'type'        => '12',
'state'        => true
]);
DB::connection('tenant')->table('tables')->insert([
'description1' => '93',
'description2' => 'OTROS 3',
'code_rent'    => '',
'type'        => '12',
'state'        => true
]);
DB::connection('tenant')->table('tables')->insert([
'description1' => '94',
'description2' => 'OTROS 4',
'code_rent'    => '',
'type'        => '12',
'state'        => true
]);
DB::connection('tenant')->table('tables')->insert([
'description1' => '95',
'description2' => 'OTROS 5',
'code_rent'    => '',
'type'        => '12',
'state'        => true
]);
DB::connection('tenant')->table('tables')->insert([
'description1' => '96',
'description2' => 'OTROS 6',
'code_rent'    => '',
'type'        => '12',
'state'        => true
]);
DB::connection('tenant')->table('tables')->insert([
'description1' => '97',
'description2' => 'OTROS 7',
'code_rent'    => '',
'type'        => '12',
'state'        => true
]);
DB::connection('tenant')->table('tables')->insert([
'description1' => '98',
'description2' => 'OTROS 8',
'code_rent'    => '',
'type'        => '12',
'state'        => true
]);
DB::connection('tenant')->table('tables')->insert([
'description1' => '99',
'description2' => 'OTROS',
'code_rent'    => '',
'type'        => '12',
'state'        => true
]);
//tabla 13
DB::connection('tenant')->table('tables')->insert([
'description1'  => '1',
'description2'  => 'NACIONES UNIDAS',
'code_rent'     => '',
'type'          => '13',
'state'        => true
]);
DB::connection('tenant')->table('tables')->insert([
'description1'  => '1',
'description2'  => 'GS1 (EAN-UCC)',
'code_rent'     => '',
'type'          => '13',
'state'        => true
]);
DB::connection('tenant')->table('tables')->insert([
'description1'  => '1',
'description2'  => 'OTROS',
'code_rent'     => '',
'type'          => '13',
'state'        => true
]);
//tabla 14
DB::connection('tenant')->table('tables')->insert([
'description1'  => '1',
'description2'  => 'PROMEDIO PONDERADO',
'code_rent'     => '',
'type'          => '14',
'state'        => true    
]);
DB::connection('tenant')->table('tables')->insert([
'description1'  => '2',
'description2'  => 'PRIMERAS ENTRADAS, PRIMERAS SALIDAS',
'code_rent'     => '',
'type'          => '14',
'state'        => true    
]);
DB::connection('tenant')->table('tables')->insert([
'description1'  => '3',
'description2'  => 'EXISTENCIAS BÁSICAS',
'code_rent'     => '',
'type'          => '14',
'state'        => true   
]);
DB::connection('tenant')->table('tables')->insert([
'description1'  => '4',
'description2'  => 'DETALLISTA',
'code_rent'     => '',
'type'          => '14',
'state'        => true   
]);
DB::connection('tenant')->table('tables')->insert([
'description1'  => '5',
'description2'  => 'IDENTIFICACIÓN ESPECÍFICA',
'code_rent'     => '',
'type'          => '14',
'state'        => true    
]);
DB::connection('tenant')->table('tables')->insert([
'description1'  => '9',
'description2'  => 'OTROS',
'code_rent'     => '',
'type'          => '14',
'state'        => true    
]);
//tabla15
DB::connection('tenant')->table('tables')->insert([
'description1' => '01',
'description2' => 'VALORES EMITIDOS O GARANTIZADOS POR EL ESTADO',
'code_rent' => '',
'type' => '15',
'state'=>true
]);
DB::connection('tenant')->table('tables')->insert([
'description1' => '02',
'description2' => 'VALORES EMITIDOS O GARANTIZADOS POR EL SISTEMA FINANCIERO',
'code_rent' => '',
'type' => '15',
'state'=>true
]);
DB::connection('tenant')->table('tables')->insert([
'description1' => '03',
'description2' => 'VALORES EMITIDOS POR LA EMPRESA',
'code_rent' => '',
'type' => '15',
'state'=>true
]);
DB::connection('tenant')->table('tables')->insert([
'description1' => '04',
'description2' => 'OTROS INSTRUMENTOS FINANCIEROS REPRESENTATIVOS DE DEUDA',
'code_rent' => '',
'type' => '15',
'state'=>true
]);
DB::connection('tenant')->table('tables')->insert([
'description1' => '05',
'description2' => 'CERTIFICADOS DE SUSCRIPCIÓN PREFERENTE',
'code_rent' => '',
'type' => '15',
'state'=>true
]);
DB::connection('tenant')->table('tables')->insert([
'description1' => '06',
'description2' => 'ACCIONES REPRESENTATIVAS DE CAPITAL SOCIAL',
'code_rent' => '',
'type' => '15',
'state'=>true
]);
DB::connection('tenant')->table('tables')->insert([
'description1' => '07',
'description2' => 'ACCIONES DE INVERSIÓN',
'code_rent' => '',
'type' => '15',
'state'=>true
]);
DB::connection('tenant')->table('tables')->insert([
'description1' => '08',
'description2' => 'CERTIFICADO DE PARTICIPACIÓN DE FONDOS',
'code_rent' => '',
'type' => '15',
'state'=>true
]);
DB::connection('tenant')->table('tables')->insert([
'description1' => '09',
'description2' => 'ASOCIACIONES EN PARTICIPACIÓN Y CONSORCIOS',
'code_rent' => '',
'type' => '15',
'state'=>true
]);
DB::connection('tenant')->table('tables')->insert([
'description1' => '10',
'description2' => 'OTROS INSTRUMENTOS FINANCIEROS REPRESENTATIVOS DE DERECHO PATRIMONIAL',
'code_rent' => '',
'type' => '15',
'state'=>true
]);
DB::connection('tenant')->table('tables')->insert([
'description1' => '99',
'description2' => 'OTROS TÍTULOS',
'code_rent' => '',
'type' => '15',
'state'=>true
]);
//tabla 16
DB::connection('tenant')->table('tables')->insert([
'description1' => '01',
'description2' => 'ACCIONES CON DERECHO A VOTO',
'code_rent' => '',
'type' => '16',
'state'=>true
]);
DB::connection('tenant')->table('tables')->insert([
'description1' => '02',
'description2' => 'ACCIONES SIN DERECHO A VOTO',
'code_rent' => '',
'type' => '16',
'state'=>true
]);
DB::connection('tenant')->table('tables')->insert([
'description1' => '03',
'description2' => 'PARTICIPACIONES',
'code_rent' => '',
'type' => '16',
'state'=>true
]);
DB::connection('tenant')->table('tables')->insert([
'description1' => '04',
'description2' => 'OTROS',
'code_rent' => '',
'type' => '16',
'state'=>true
]);
//tabla 17
DB::connection('tenant')->table('tables')->insert([
'description1' => '01',
'description2' => 'PLAN CONTABLE GENERAL EMPRESARIAL',
'code_rent' => '',
'type' => '17',
'state'=>true
]);
DB::connection('tenant')->table('tables')->insert([
'description1' => '02',
'description2' => 'PLAN CONTABLE GENERAL REVISADO',
'code_rent' => '',
'type' => '17',
'state'=>true
]);
DB::connection('tenant')->table('tables')->insert([
'description1' => '03',
'description2' => 'PLAN DE CUENTAS PARA EMPRESAS DEL SISTEMA FINANCIERO,SUPERVISADAS POR SBS',
'code_rent' => '',
'type' => '17',
'state'=>true
]);
DB::connection('tenant')->table('tables')->insert([
'description1' => '04',
'description2' => 'PLAN DE CUENTAS PARA ENTIDADES PRESTADORAS DE SALUD, SUPERVISADAS POR SBS',
'code_rent' => '',
'type' => '17',
'state'=>true
]);
DB::connection('tenant')->table('tables')->insert([
'description1' => '05',
'description2' => 'PLAN DE CUENTAS PARA EMPRESAS DEL SISTEMA ASEGURADOR, SUPERVISADAS POR SBS',
'code_rent' => '',
'type' => '17',
'state'=>true
]);
DB::connection('tenant')->table('tables')->insert([
'description1' => '06',
'description2' => 'PLAN DE CUENTAS DE LAS ADMINISTRADORAS PRIVADAS DE FONDOS DE PENSIONES, SUPERVISADAS POR SBS',
'code_rent' => '',
'type' => '17',
'state'=>true
]);
DB::connection('tenant')->table('tables')->insert([
'description1' => '07',
'description2' => 'PLAN CONTABLE GUBERNAMENTAL',
'code_rent' => '',
'type' => '17',
'state'=>true
]);
DB::connection('tenant')->table('tables')->insert([
'description1' => '99',
'description2' => 'OTROS',
'code_rent' => '',
'type' => '17',
'state'=>true
]);
//tabla 18
DB::connection('tenant')->table('tables')->insert([
'description1' => '1',
'description2' => 'NO REVALUADO O REVALUADO SIN EFECTO TRIBUTARIO',
'code_rent' => '',
'type' => '18',
'state'=>true
]);
DB::connection('tenant')->table('tables')->insert([
'description1' => '2',
'description2' => 'REVALUADO CON EFECTO TRIBUTARIO',
'code_rent' => '',
'type' => '18',
'state'=>true
]);
//tabla 19
DB::connection('tenant')->table('tables')->insert([
'description1' => '1',
'description2' => 'ACTIVOS EN DESUSO',
'code_rent' => '',
'type' => '19',
'state'=>true
]);
DB::connection('tenant')->table('tables')->insert([
'description1' => '2',
'description2' => 'ACTIVOS OBSOLETOS',
'code_rent' => '',
'type' => '19',
'state'=>true
]);
DB::connection('tenant')->table('tables')->insert([
'description1' => '9',
'description2' => 'RESTO DE ACTIVOS',
'code_rent' => '',
'type' => '19',
'state'=>true
]);
//tabla 20
DB::connection('tenant')->table('tables')->insert([
'description1' => '1',
'description2' => 'LINEA RECTA',
'code_rent' => '',
'type' => '20',
'state'=>true
]);
DB::connection('tenant')->table('tables')->insert([
'description1' => '2',
'description2' => 'UNIDADES PRODUCIDAS',
'code_rent' => '',
'type' => '20',
'state'=>true
]);
DB::connection('tenant')->table('tables')->insert([
'description1' => '9',
'description2' => 'OTROS',
'code_rent' => '',
'type' => '20',
'state'=>true
]);
//tabla 21
DB::connection('tenant')->table('tables')->insert([
'description1' => '1',
'description2' => 'PROCESO PRODUCTIVO',
'code_rent' => '',
'type' => '21',
'state'=>true
]);
DB::connection('tenant')->table('tables')->insert([
'description1' => '2',
'description2' => 'LÍNEA DE PRODUCCIÓN',
'code_rent' => '',
'type' => '21',
'state'=>true
]);
DB::connection('tenant')->table('tables')->insert([
'description1' => '3',
'description2' => 'PRODUCTO',
'code_rent' => '',
'type' => '21',
'state'=>true
]);
DB::connection('tenant')->table('tables')->insert([
'description1' => '4',
'description2' => 'PROYECTO',
'code_rent' => '',
'type' => '21',
'state'=>true
]);
DB::connection('tenant')->table('tables')->insert([
'description1' => '9',
'description2' => 'OTROS',
'code_rent' => '',
'type' => '21',
'state'=>true
]);
//tabla 22
DB::connection('tenant')->table('tables')->insert([
'description1' => '01',
'description2' => 'SUPERINTENDENCIA DEL MERCADO DE VALORES-SECTOR DIVERSAS-INDIVIDUAL',
'code_rent' => '',
'type' => '22',
'state'=>true
]);
DB::connection('tenant')->table('tables')->insert([
'description1' => '02',
'description2' => 'SUPERINTENDENCIA DEL MERCADO DE VALORES-SECTOR SEGUROS-INDIVIDUAL',
'code_rent' => '',
'type' => '22',
'state'=>true
]);
DB::connection('tenant')->table('tables')->insert([
'description1' => '03',
'description2' => 'SUPERINTENDENCIA DEL MERCADO DE VALORES-SECTOR BANCOS Y FINANCIERAS-INDIVIDUAL',
'code_rent' => '',
'type' => '22',
'state'=>true
]);
DB::connection('tenant')->table('tables')->insert([
'description1' => '04',
'description2' => 'SUPERINTENDENCIA DEL MERCADO DE VALORES-ADMINISTRADORAS DE FONDOS DE PENSIONES(AFP)',
'code_rent' => '',
'type' => '22',
'state'=>true
]);
DB::connection('tenant')->table('tables')->insert([
'description1' => '05',
'description2' => 'SUPERINTENDENCIA DEL MERCADO DE VALORES - AGENTES DE INTERMEDIACIÓN',
'code_rent' => '',
'type' => '22',
'state'=>true
]);
DB::connection('tenant')->table('tables')->insert([
'description1' => '06',
'description2' => 'SUPERINTENDENCIA DEL MERCADO DE VALORES-FONDOS DE INVERSIÓN',
'code_rent' => '',
'type' => '22',
'state'=>true
]);
DB::connection('tenant')->table('tables')->insert([
'description1' => '07',
'description2' => 'SUPERINTENDENCIA DEL MERCADO DE VALORES-PATRIMONIO EN FIDEICOMISOS',
'code_rent' => '',
'type' => '22',
'state'=>true
]);
DB::connection('tenant')->table('tables')->insert([
'description1' => '08',
'description2' => 'SUPERINTENDENCIA DEL MERCADO DE VALORES-ICLV',
'code_rent' => '',
'type' => '22',
'state'=>true
]);
DB::connection('tenant')->table('tables')->insert([
'description1' => '09',
'description2' => 'OTROS NO CONSIDERADOS EN LOS ANTERIORES',
'code_rent' => '',
'type' => '22',
'state'=>true
]);
//tabla 25
DB::connection('tenant')->table('tables')->insert([
'description1' => '00',
'description2' => 'NINGUNO',
'code_rent' => '',
'type' => '25',
'state'=>true
]);
DB::connection('tenant')->table('tables')->insert([
'description1' => '01',
'description2' => 'CANADA',
'code_rent' => '',
'type' => '25',
'state'=>true
]);
DB::connection('tenant')->table('tables')->insert([
'description1' => '02',
'description2' => 'CHILE',
'code_rent' => '',
'type' => '25',
'state'=>true
]);
DB::connection('tenant')->table('tables')->insert([
'description1' => '03',
'description2' => 'COMUNIDAD ANDINA DE NACIONES (CAN)',
'code_rent' => '',
'type' => '25',
'state'=>true
]);
DB::connection('tenant')->table('tables')->insert([
'description1' => '04',
'description2' => 'BRASIL',
'code_rent' => '',
'type' => '25',
'state'=>true
]);
DB::connection('tenant')->table('tables')->insert([
'description1' => '05',
'description2' => 'ESTADOS UNIDOS MEXICANOS',
'code_rent' => '',
'type' => '25',
'state'=>true
]);
DB::connection('tenant')->table('tables')->insert([
'description1' => '06',
'description2' => 'REPUBLICA DE COREA',
'code_rent' => '',
'type' => '25',
'state'=>true
]);
DB::connection('tenant')->table('tables')->insert([
'description1' => '07',
'description2' => 'CONFEDERACIÓN SUIZA',
'code_rent' => '',
'type' => '25',
'state'=>true
]);
DB::connection('tenant')->table('tables')->insert([
'description1' => '08',
'description2' => 'PORTUGAL',
'code_rent' => '',
'type' => '25',
'state'=>true
]);
DB::connection('tenant')->table('tables')->insert([
'description1' => '09',
'description2' => 'OTROS',
'code_rent' => '',
'type' => '25',
'state'=>true
]);
// tabla 27
DB::connection('tenant')->table('tables')->insert([
'description1' =>'01',
'description2' =>'Una persona natural o jurídica posea más de treinta por ciento (30%) del capital de otra persona jurídica, directamente o por intermedio de un tercero',
'code_rent' => 'Articulo 24° numeral 1',
'type' => '27',
'state'=>true
]);
DB::connection('tenant')->table('tables')->insert([
'description1' =>'02',
'description2' =>'Más del treinta por ciento (30%) del capital de dos (2) o más personas jurídicas pertenezca a una misma persona natural o jurídica, directamente o por intermedio de un tercero',
'code_rent' => 'Articulo 24° numeral 2',
'type' => '27',
'state'=>true
]);
DB::connection('tenant')->table('tables')->insert([
'description1' => '03',
'description2' => 'En cualesquiera de los casos anteriores, cuando la indicada proporción del capital pertenezca a cónyuges entre sí o a personas naturales vinculadas hasta el segundo grado de consanguinidad o afinidad',
'code_rent' => 'Articulo 24° numeral 3',
'type' => '27',
'state'=>true
]);
DB::connection('tenant')->table('tables')->insert([
'description1' => '04',
'description2' => 'El capital de dos (2) o más personas jurídicas pertenezca en más del treinta por ciento (30%) a socios comunes a éstas',
'code_rent' => 'Articulo 24° numeral 4',
'type' => '27',
'state'=>true
]);
DB::connection('tenant')->table('tables')->insert([
'description1' => '05',
'description2' => 'Las personas jurídicas o entidades cuenten con una o más directores, gerentes, administradores u otros directivos comunes, que tengan poder de decisión en los acuerdos financieros, operativos y/o comerciales que se adopten',
'code_rent' => 'Articulo 24° numeral 5',
'type' => '27',
'state'=>true
]);
DB::connection('tenant')->table('tables')->insert([
'description1' => '06',
'description2' => 'Dos o más personas naturales o jurídicas consoliden Estados Financieros',
'code_rent' => 'Articulo 24° numeral 6',
'type' => '27',
'state'=>true
]);
DB::connection('tenant')->table('tables')->insert([
'description1' => '07',
'description2' => 'Exista un contrato de colaboración empresarial con contabilidad independiente, en cuyo caso el contrato se considerará vinculado con aquellas partes contratantes que participen, directamente o por intermedio de un tercero, en mas del treinta por ciento (30%) en el patrimonio del contrato o cuando alguna de las partes contratantes tengan poder de decisión en los acuerdos financieros, comerciales u operativos que se adopten para el desarrollo del contrato, caso en el cual la parte contratante que ejerza el poder de decisión se encontrará vinculado con el contrato',
'code_rent' => 'Articulo 24° numeral 7',
'type' => '27',
'state'=>true
]);
DB::connection('tenant')->table('tables')->insert([
'description1' => '08',
'description2' => 'En el caso de un contrato de colaboración empresarial sin contabilidad independiente, la vinculación entre cada una de las partes integrantes del contrato y la contraparte deberá verificarse individualmente, aplicando algunos de los criterios de vinculación establecidos en este artículo (articulo 24° del reglamento del Impuesto a la Renta). Se entiende por contraparte a la persona natural o jurídica con las que las partes integrantes celebren alguna operación con el fin de alcanzar el objeto del contrato',
'code_rent' => 'Articulo 24° numeral 8',
'type' => '27',
'state'=>true
]);
DB::connection('tenant')->table('tables')->insert([
'description1' => '09',
'description2' => 'Exista un contrato de asociación en participación, en el que alguno de los asociados, directa o indirectamente, participe en mas del treinta por ciento (30%) en los resultados o en las utilidades de uno o varios negocios del asociante, en cuyo caso se considerará que existe vinculación entre el asociante y cada uno de sus asociados. También se considerará que existe vinculación cuando alguno de los asociados tenga poder de decisión en los aspectos financieros, comerciales u operativos en uno o varios negocios del asociante',
'code_rent' => 'Articulo 24° numeral 9',
'type' => '27',
'state'=>true
]);
DB::connection('tenant')->table('tables')->insert([
'description1' => '10',
'description2' => 'Una empresa no domiciliada tenga uno o mas establecimientos permanentes en el país, en cuyo caso existirá vinculación entre la empresa no domiciliada y cada uno de sus establecimientos permanentes y entre todos ellos entre si',
'code_rent' => 'Articulo 24° numeral 10',
'type' => '27',
'state'=>true
]);
DB::connection('tenant')->table('tables')->insert([
'description1' => '11',
'description2' => 'Una empresa domiciliada en territorio peruano tenga uno o mas establecimientos permanentes en el extranjero, en cuyo caso existirá vinculación entre la empresa domiciliada y cada uno de sus establecimientos permanentes',
'code_rent' => 'Articulo 24° numeral 11',
'type' => '27',
'state'=>true
]);
DB::connection('tenant')->table('tables')->insert([
'description1' => '12',
'description2' => 'Una persona natural o jurídica ejerza influencia dominante en las decisiones de los órganos de administración de una o mas personas jurídicas o entidades. En tal situación, se considerará que las personas jurídicas o entidades influidas están vinculadas entre si y con la persona natural o jurídica que ejerce dicha influencia. Se entiende que una persona natural o jurídica ejerce influencia  dominante cuando, en la adopción del acuerdo, ejerce o controla la mayoría absoluta de votos para la toma de decisiones en los órganos de administración de la persona jurídica o entidad. En el caso de decisiones relacionadas con los asuntos mencionados en el artículo 126° de la Ley General de Sociedades, existirá influencia dominante de la persona natural o jurídica que, participando en la adopción del acuerdo, por si misma o con la intervención de votos de terceros, tiene en el acto de votación el mayor número de acciones suscritas con derecho a voto, siempre y cuando cuente con, al menos, el diez por ciento (10%) de las acciones suscritas con derecho a voto. También se otorgará el tratamiento de partes vinculadas cuando una persona, empresa o entidad domiciliada en el país realice, en el ejercicio gravable anterior, el ochenta por ciento (80%) o mas de sus ventas de bienes, prestación de servicios u otro tipo de operaciones, con una persona, empresa o entidad domiciliada en el país o con personas, empresas o entidades vinculadas entre si, domiciliadas en el país, siempre que tales operaciones, a su vez, representen por lo menos el treinta por ciento (30%) de las compras o adquisiciones de la otra parte en el mismo período. Tratándose de empresas que tengan actividades por períodos mayores a tres ejercicios gravables, tales porcentajes se calcularán teniendo en cuenta el porcentaje promedio de ventas o compras, según sea el caso, realizadas en los tres ejercicios gravables inmediatos anteriores. Lo dispuesto en este párrafo no será de aplicación a las operaciones que realicen las empresas que conforman la Actividad Empresarial del Estado, en las cuales la participación del Estado sea mayor al cincuenta por ciento (50%) del capital. La vinculación, de acuerdo a alguno de los criterios establecidos en este artículo, también operará cuando la transacción sea realizada utilizando personas o entidades interpuestas, domiciliadas o no en el país con el propósito de encubrir una transacción entre partes vinculadas',
'code_rent' => 'Articulo 24° numeral 12',
'type' => '27',
'state'=>true
]);
//tabla 28 
DB::connection('tenant')->table('tables')->insert([
'description1' => '',
'description2' => '50 CAPITAL',
'code_rent' => '',
'type' => '28',
'state'=>false
]);
DB::connection('tenant')->table('tables')->insert([
'description1' => '501',
'description2' => 'Capital social',
'code_rent' => '',
'type' => '28',
'state'=>true
]);
DB::connection('tenant')->table('tables')->insert([
'description1' => '5011',
'description2' => 'Acciones',
'code_rent' => '',
'type' => '28',
'state'=>true
]);
DB::connection('tenant')->table('tables')->insert([
'description1' => '5012',
'description2' => 'Participaciones',
'code_rent' => '',
'type' => '28',
'state'=>true
]);
DB::connection('tenant')->table('tables')->insert([
'description1' => '502',
'description2' => 'Acciones en tesorería',
'code_rent' => '',
'type' => '28',
'state'=>true
]);
DB::connection('tenant')->table('tables')->insert([
'description1' => '',
'description2' => '51 ACCIONES DE INVERSIÓN',
'code_rent' => '',
'type' => '28',
'state'=>false
]);
DB::connection('tenant')->table('tables')->insert([
'description1' => '511',
'description2' => 'Acciones de inversión',
'code_rent' => '',
'type' => '28',
'state'=>true
]);
DB::connection('tenant')->table('tables')->insert([
'description1' => '512',
'description2' => 'Acciones de inversión en tesorería',
'code_rent' => '',
'type' => '28',
'state'=>true
]);
DB::connection('tenant')->table('tables')->insert([
'description1' => '',
'description2' => '52 CAPITAL ADICIONAL',
'code_rent' => '',
'type' => '28',
'state'=>false
]);
DB::connection('tenant')->table('tables')->insert([
'description1' => '521',
'description2' => 'Primas (descuento) de acciones',
'code_rent' => '',
'type' => '28',
'state'=>true
]);
DB::connection('tenant')->table('tables')->insert([
'description1' => '522',
'description2' => 'Capitalizaciones en trámite',
'code_rent' => '',
'type' => '28',
'state'=>true
]);
DB::connection('tenant')->table('tables')->insert([
'description1' => '5221',
'description2' => 'Aportes',
'code_rent' => '',
'type' => '28',
'state'=>true
]);
DB::connection('tenant')->table('tables')->insert([
'description1' => '5222',
'description2' => 'Reservas',
'code_rent' => '',
'type' => '28',
'state'=>true
]);
DB::connection('tenant')->table('tables')->insert([
'description1' => '5223',
'description2' => 'Acreencias',
'code_rent' => '',
'type' => '28',
'state'=>true
]);
DB::connection('tenant')->table('tables')->insert([
'description1' => '5224',
'description2' => 'Utilidades',
'code_rent' => '',
'type' => '28',
'state'=>true
]);
DB::connection('tenant')->table('tables')->insert([
'description1' => '523',
'description2' => 'Reducciones de capital pendientes de formalización',
'code_rent' => '',
'type' => '28',
'state'=>true
]);
DB::connection('tenant')->table('tables')->insert([
'description1' => '',
'description2' => '56 RESULTADOS NO REALIZADOS',
'code_rent' => '',
'type' => '28',
'state'=>false
]);
DB::connection('tenant')->table('tables')->insert([
'description1' => '561',
'description2' => 'Diferencia en cambio de inversiones permanentes en entidades extranjeras',
'code_rent' => '',
'type' => '28',
'state'=>true
]);
DB::connection('tenant')->table('tables')->insert([
'description1' => '562',
'description2' => 'Instrumentos financieros–Cobertura de flujo de efectivo',
'code_rent' => '',
'type' => '28',
'state'=>true
]);
DB::connection('tenant')->table('tables')->insert([
'description1' => '563',
'description2' => 'Ganancia o pérdida en activos o pasivos financieros disponibles para la venta',
'code_rent' => '',
'type' => '28',
'state'=>true
]);
DB::connection('tenant')->table('tables')->insert([
'description1' => '5631',
'description2' => 'Ganancia',
'code_rent' => '',
'type' => '28',
'state'=>true
]);
DB::connection('tenant')->table('tables')->insert([
'description1' => '5632',
'description2' => 'Pérdida',
'code_rent' => '',
'type' => '28',
'state'=>true
]);
DB::connection('tenant')->table('tables')->insert([
'description1' => '564',
'description2' => 'Ganancia o pérdida en activos o pasivos financieros disponibles para la venta–Compra o venta convencional fecha de liquidación',
'code_rent' => '',
'type' => '28',
'state'=>true
]);
DB::connection('tenant')->table('tables')->insert([
'description1' => '5641',
'description2' => 'Ganancia',
'code_rent' => '',
'type' => '28',
'state'=>true
]);
DB::connection('tenant')->table('tables')->insert([
'description1' => '5642',
'description2' => 'Pérdida',
'code_rent' => '',
'type' => '28',
'state'=>true
]);
DB::connection('tenant')->table('tables')->insert([
'description1' => '',
'description2' => '57 EXCEDENTE DE REVALUACIÓN',
'code_rent' => '',
'type' => '28',
'state'=>false
]);
DB::connection('tenant')->table('tables')->insert([
'description1' => '571',
'description2' => 'Excedente de revaluación',
'code_rent' => '',
'type' => '28',
'state'=>true
]);
DB::connection('tenant')->table('tables')->insert([
'description1' => '5711',
'description2' => 'Inversiones inmobiliarias',
'code_rent' => '',
'type' => '28',
'state'=>true
]);
DB::connection('tenant')->table('tables')->insert([
'description1' => '5712',
'description2' => 'Inmuebles,maquinaria y equipos',
'code_rent' => '',
'type' => '28',
'state'=>true
]);
DB::connection('tenant')->table('tables')->insert([
'description1' => '5713',
'description2' => 'Intangibles',
'code_rent' => '',
'type' => '28',
'state'=>true
]);
DB::connection('tenant')->table('tables')->insert([
'description1' => '572',
'description2' => 'Excedente de revaluación–Acciones liberadas recibidas',
'code_rent' => '',
'type' => '28',
'state'=>true
]);
DB::connection('tenant')->table('tables')->insert([
'description1' => '573',
'description2' => 'Participación en excedente de revaluación–Inversiones en entidades relacionadas',
'code_rent' => '',
'type' => '28',
'state'=>true
]);
DB::connection('tenant')->table('tables')->insert([
'description1' => '',
'description2' => '52 CAPITAL ADICIONAL',
'code_rent' => '',
'type' => '28',
'state'=>true
]);
DB::connection('tenant')->table('tables')->insert([
'description1' => '',
'description2' => '58 RESERVAS',
'code_rent' => '',
'type' => '28',
'state'=>false
]);
DB::connection('tenant')->table('tables')->insert([
'description1' => '581',
'description2' => 'Reinversión',
'code_rent' => '',
'type' => '28',
'state'=>true
]);
DB::connection('tenant')->table('tables')->insert([
'description1' => '582',
'description2' => 'Legal',
'code_rent' => '',
'type' => '28',
'state'=>true
]);
DB::connection('tenant')->table('tables')->insert([
'description1' => '583',
'description2' => 'Contractuales',
'code_rent' => '',
'type' => '28',
'state'=>true
]);
DB::connection('tenant')->table('tables')->insert([
'description1' => '584',
'description2' => 'Estatutarias',
'code_rent' => '',
'type' => '28',
'state'=>true
]);
DB::connection('tenant')->table('tables')->insert([
'description1' => '585',
'description2' => 'Facultativas',
'code_rent' => '',
'type' => '28',
'state'=>true
]);
DB::connection('tenant')->table('tables')->insert([
'description1' => '589',
'description2' => 'Otras reservas',
'code_rent' => '',
'type' => '28',
'state'=>true
]);
DB::connection('tenant')->table('tables')->insert([
'description1' => '',
'description2' => '59 RESULTADOS ACUMULADOS',
'code_rent' => '',
'type' => '28',
'state'=>false
]);
DB::connection('tenant')->table('tables')->insert([
'description1' => '591',
'description2' => 'Utilidades no distribuidas',
'code_rent' => '',
'type' => '28',
'state'=>true
]);
DB::connection('tenant')->table('tables')->insert([
'description1' => '5911',
'description2' => 'Utilidades acumuladas',
'code_rent' => '',
'type' => '28',
'state'=>true
]);
DB::connection('tenant')->table('tables')->insert([
'description1' => '5912',
'description2' => 'Ingresos de años anteriores',
'code_rent' => '',
'type' => '28',
'state'=>true
]);
DB::connection('tenant')->table('tables')->insert([
'description1' => '592',
'description2' => 'Pérdidas acumuladas',
'code_rent' => '',
'type' => '28',
'state'=>true
]);
DB::connection('tenant')->table('tables')->insert([
'description1' => '5921',
'description2' => 'Pérdidas acumuladas',
'code_rent' => '',
'type' => '28',
'state'=>true
]);
DB::connection('tenant')->table('tables')->insert([
'description1' => '5922',
'description2' => 'Gastos de años anteriores',
'code_rent' => '',
'type' => '28',
'state'=>true
]);
//tabla 30
DB::connection('tenant')->table('tables')->insert([
'description1' => '1',
'description2' => 'MERCADERIA,MATERIA PRIMA,SUMINISTRO,ENVASES Y EMBALAJES',
'code_rent' => '',
'type' => '30',
'state'=>true
]);
DB::connection('tenant')->table('tables')->insert([
'description1' => '2',
'description2' => 'ACTIVO FIJO',
'code_rent' => '',
'type' => '30',
'state'=>true
]);
DB::connection('tenant')->table('tables')->insert([
'description1' => '3',
'description2' => 'OTROS ACTIVOS NO CONSIDERADOS EN LOS NUMERALES 1 Y 2',
'code_rent' => '',
'type' => '30',
'state'=>true
]);
DB::connection('tenant')->table('tables')->insert([
'description1' => '4',
'description2' => 'GASTOS DE EDUCACIÓN,RECREACIÓN,SALUD,CULTURALES.REPRESENTACIÓN,CAPACITACIÓN,DE VIAJE,MANTENIMIENTO DE VEHICULO Y DE PREMIOS',
'code_rent' => '',
'type' => '30',
'state'=>true
]);
DB::connection('tenant')->table('tables')->insert([
'description1' => '5',
'description2' => 'OTROS GASTOS NO INCLUIDOS EN EL NUMERAL 4',
'code_rent' => '',
'type' => '30',
'state'=>true
]);
//tabla 31
DB::connection('tenant')->table('tables')->insert([
'description1' => '00',
'description2' => 'Bienes',
'code_rent' => '',
'description3' => '',
'type' => '31',
'state'=>true
]);
DB::connection('tenant')->table('tables')->insert([
    'description1' => '00',
    'description2' => 'Bienes',
    'code_rent' => '',
    'description3' => '',
    'type' => '31',
    'state'=>true
    ]);
DB::connection('tenant')->table('tables')->insert([
    'description1' => '01',
    'description2' => 'Arrendamiento de predios',
    'code_rent' => '06',
    'description3' => 'Art. 9° a)',
    'type' => '31',
    'state'=>true
]);
DB::connection('tenant')->table('tables')->insert([
    'description1' => '02',
    'description2' => 'Enajenación Inmuebles y derechos sobre inmuebles',
    'code_rent' => '06',
    'description3' => 'Art. 9° a)',
    'type' => '31',
    'state'=>true
 ]);
 DB::connection('tenant')->table('tables')->insert([
    'description1' => '03',
    'description2' => 'Rentas de bienes situados en el país o derechos utilizados en el país, incluye enajenación',
    'code_rent' => '12',
    'description3' => 'Art. 9° b)',
    'type' => '31',
    'state'=>true
    ]);
DB::connection('tenant')->table('tables')->insert([
        'description1' => '04',
        'description2' => 'Regalias: Bienes o derechos por los que pagan son utilizados país.',
        'code_rent' => '12',
        'description3' => 'Art. 9° b)',
        'type' => '31',
        'state'=>true
]);
DB::connection('tenant')->table('tables')->insert([
    'description1' => '05',
    'description2' => 'Regalias:Pagador de las regalías es domiciliado.',
    'code_rent' => '12',
    'description3' => 'Art. 9° b)',
    'type' => '31',
    'state'=>true
]);
DB::connection('tenant')->table('tables')->insert([
    'description1' => '06',
    'description2' => 'Por capitales,intereses,comisiones,primas -operaciones financieras-capital utilizado en el país o el pagador sea un sujeto domiciliado.Originadas por fondos de cualquier tipo de entidad, por cesión a terceros de un capital, por operaciones de capitalización o por contratos de seguro de vida o invalidez que no tengan su origen en el trabajo personal.',
    'code_rent' => '11',
    'description3' => 'Art. 9° c)',
    'type' => '31',
    'state'=>true
]);
DB::connection('tenant')->table('tables')->insert([
    'description1' => '07',
    'description2' => 'Dividendos y cualquier otra forma de distribución de utilidades, cuando la empresa que los distribuye, pague y acredite se encuentre domiciliada en el país, o cuando el fondo de inversión, patrimonio fideicometido o fiduciario banacario, fondo de pensiones, que lo distribuya,pague o acredite,se encuentre constituido o establecido en el país',
    'code_rent' => '10',
    'description3' => 'Art. 9° d)',
    'type' => '31',
    'state'=>true
]);
DB::connection('tenant')->table('tables')->insert([
    'description1' => '08',
    'description2' => 'Los rendimientos de los ADR y GDRs que tengan como subyacentes acciones emitidas por empresas no domiciliadas',
    'code_rent' => '10',
    'description3' => 'Art. 9° d)',
    'type' => '31',
    'state'=>true
]);
DB::connection('tenant')->table('tables')->insert([
    'description1' => '09',
    'description2' => 'Originadas en actividades civiles, comerciales, empresariales o de cualquier índole, que se lleven a cabo en territorio nacional.',
    'code_rent' => '21',
    'description3' => 'Art. 9° e)',
    'type' => '31',
    'state'=>true
]);
DB::connection('tenant')->table('tables')->insert([
    'description1' => '10',
    'description2' => 'Originadas en el trabajo personal que lleven a cabo en el territorio nacional No comprende cuando se ingresa al país  temporalmente a efectuar: actos previos a la realización de inversiones extranjeras; actos destinados a supervisar o controlar la inversión o el negocio, tales como los de recolección de datos o información o la realización de entrevistas; actos relacionados con la contratación de personal local; actos relacionados con la firma de convenios o actos similares.',
    'code_rent' => '15',
    'description3' => 'Art. 9° f)',
    'type' => '31',
    'state'=>true
]);
DB::connection('tenant')->table('tables')->insert([
    'description1' => '11',
    'description2' => 'Rentas vitalicias y las pensiones que tengan su origen en el trabajo personal, pagadas por un sujeto domiciliada',
    'code_rent' => '18',
    'description3' => 'Art. 9° g)',
    'type' => '31',
    'state'=>true
]);
DB::connection('tenant')->table('tables')->insert([
    'description1' => '12',
    'description2' => 'Las obtenidas por la enajenación de acciones y participaciones representativas del capital,  cuando las empresas, que los hayan emitido estén constituidos o establecidos en el Perú. Fuera de Bolsa',
    'code_rent' => '13',
    'description3' => 'Art. 9° h)',
    'type' => '31',
    'state'=>true
]);
DB::connection('tenant')->table('tables')->insert([
    'description1' => '13',
    'description2' => 'Las obtenidas por la enajenación, de certificados, títulos, bonos y papeles comerciales, valores representativos de cédulas hipotecarias, obligaciones al portador u otros valores al portador y otros valores mobiliarios cuando los  Fondos de Inversión, Fondos Mutuos de Inversión en Valores o Patrimonios Fideicometidos que los hayan emitido estén constituidos o establecidos en el Perú.Fuera de Bolsa',
    'code_rent' => '13',
    'description3' => 'Art. 9° h)',
    'type' => '31',
    'state'=>true
]);
DB::connection('tenant')->table('tables')->insert([
    'description1' => '14',
    'description2' => 'Las obtenidas por la redención o rescate de certificados,títulos, bonos y papeles comerciales, valores representativos de cédulas hipotecarias, obligaciones al portador u otros valores al portador y otros valores mobiliarios cuando los  Fondos de Inversión, Fondos Mutuos de Inversión en Valores o Patrimonios Fideicometidos que los hayan emitido estén constituidos o establecidos en el Perú.Fuera de Bolsa',
    'code_rent' => '13',
    'description3' => 'Art. 9° h)',
    'type' => '31',
    'state'=>true
]);
DB::connection('tenant')->table('tables')->insert([
    'description1' => '15',
    'description2' => 'Las obtenidas por la enajenación de acciones y participaciones representativas del capital, acciones de inversión, certificados, títulos, bonos y papeles comerciales, valores representativos de cédulas hipotecarias, obligaciones al portador u otros valores al portador y otros valores mobiliarios cuando las empresas, sociedades, Fondos de Inversión, Fondos Mutuos de Inversión en Valores o Patrimonios Fideicometidos que los hayan emitido estén constituidos o establecidos en el Perú.Dentro de Bolsa',
    'code_rent' => '13',
    'description3' => 'Art. 9° h)',
    'type' => '31',
    'state'=>true
]);
DB::connection('tenant')->table('tables')->insert([
    'description1' => '16',
    'description2' => 'Las obtenidas por la redención o rescate de  acciones de inversión, certificados, títulos, bonos y papeles comerciales, valores representativos de cédulas hipotecarias, obligaciones al portador u otros valores al portador y otros valores mobiliarios cuando las empresas, sociedades, Fondos de Inversión, Fondos Mutuos de Inversión en Valores o Patrimonios Fideicometidos que los hayan emitido estén constituidos o establecidos en el Perú.Dentro de Bolsa',
    'code_rent' => '13',
    'description3' => 'Art. 9° h)',
    'type' => '31',
    'state'=>true
]);
DB::connection('tenant')->table('tables')->insert([
    'description1' => '17',
    'description2' => 'Enajenación de los ADR’s y GDR’s que tengan como subyacente acciones emitidas por empresas domiciliadas en el país.',
    'code_rent' => '13',
    'description3' => 'Art. 9° h)',
    'type' => '31',
    'state'=>true
]);
DB::connection('tenant')->table('tables')->insert([
    'description1' => '18',
    'description2' => 'Las obtenidas por servicios digitales prestados a través del Internet o de cualquier adaptación o aplicación de los protocolos, plataformas o de la tecnología utilizada por Internet o cualquier otra red a través de la que se presten servicios equivalentes,cuando el servicio se utilice económicamente, use o consuma en el país',
    'code_rent' => '21',
    'description3' => 'Art. 9° i)',
    'type' => '31',
    'state'=>true
]);
DB::connection('tenant')->table('tables')->insert([
    'description1' => '19',
    'description2' => 'La obtenida por asistencia técnica,cuando ésta se utilice económicamente en el país',
    'code_rent' => '7',
    'description3' => 'Art. 9° j)',
    'type' => '31',
    'state'=>true
]);
DB::connection('tenant')->table('tables')->insert([
    'description1' => '20',
    'description2' => 'Los intereses de obligaciones,cuando la entidad emisora ha sido constituida en el país',
    'code_rent' => '11',
    'description3' => '10 a)',
    'type' => '31',
    'state'=>true
]);
DB::connection('tenant')->table('tables')->insert([
    'description1' => '21',
    'description2' => 'Remuneracion pagada por Domiciliados; Sujeto miembro de un consejo directivo o adm.; Actuación en el Extranjero',
    'code_rent' => '16',
    'description3' => '10 b)',
    'type' => '31',
    'state'=>true
]);
DB::connection('tenant')->table('tables')->insert([
    'description1' => '22',
    'description2' => 'Honorarios o remuneraciones del Sector Público Nacional por trabajos realizados en el exterior.',
    'code_rent' => '19',
    'description3' => '10 c)',
    'type' => '31',
    'state'=>true
]);
DB::connection('tenant')->table('tables')->insert([
    'description1' => '23',
    'description2' => 'Resultados Proveniente de la contratación de IFD',
    'code_rent' => '7',
    'description3' => '10 d)',
    'type' => '31',
    'state'=>true
]);
DB::connection('tenant')->table('tables')->insert([
    'description1' => '24',
    'description2' => 'Resultados de IFD con cobertura destinados a la generación de RFP',
    'code_rent' => '7',
    'description3' => '10 d)',
    'type' => '31',
    'state'=>true
]);
DB::connection('tenant')->table('tables')->insert([
    'description1' => '25',
    'description2' => 'Proveniente de la contratación de IFD sin cobertura destinados a la generación de RFP',
    'code_rent' => '7',
    'description3' => '10 d)',
    'type' => '31',
    'state'=>true
]);
DB::connection('tenant')->table('tables')->insert([
    'description1' => '26',
    'description2' => 'IFD donde Activo subyacente sea referido al Tipo de Cambio Moneda extranjera.180 dias.(La Dom es la prestadora del IFD, la No Dom es la que obtiene el resultado)',
    'code_rent' => '7',
    'description3' => '10 d)',
    'type' => '31',
    'state'=>true
]);
DB::connection('tenant')->table('tables')->insert([
    'description1' => '27',
    'description2' => 'Las obtenidas por la enajenación indirecta de acciones y participaciones representativas del capital de personas jurídicas domiciliadas en el país,cuando la operación cumple las condiciones para ser gravadas en el Perú',
    'code_rent' => '13',
    'description3' => 'Art.10° e)',
    'type' => '31',
    'state'=>true
]);
DB::connection('tenant')->table('tables')->insert([
    'description1' => '28',
    'description2' => 'Se incluye dentro de la enajenación de de empresas no domiciliadas en el país la enajenación de ADR´s (American Depositary Receipts) que tengan como subyacente a tales acciones.',
    'code_rent' => '13',
    'description3' => 'Art.10° e)',
    'type' => '31',
    'state'=>true
]);
DB::connection('tenant')->table('tables')->insert([
    'description1' => '29',
    'description2' => 'Sujeto es el No Dom que distribuye por la reducción de capital dentro de los 12 meses anteriores cuando hubiera aumentado su capital como consecuencia de nuevos aportes,de capitalización de créditos o de una reorganización',
    'code_rent' => '10',
    'description3' => 'Art.10 f)',
    'type' => '31',
    'state'=>true
]);
DB::connection('tenant')->table('tables')->insert([
    'description1' => '30',
    'description2' => 'Actividades de Seguros 7% sobre las primas.',
    'code_rent' => '07 - 21',
    'description3' => 'Art.12 y 48°a)',
    'type' => '31',
    'state'=>true
]);
DB::connection('tenant')->table('tables')->insert([
    'description1' => '31',
    'description2' => 'Alquiler de Naves, 8% sobre Ingresos Brutos que perciban por dicha actividad',
    'code_rent' => '8',
    'description3' => 'Art.12 y 48°b) ',
    'type' => '31',
    'state'=>true
]);
DB::connection('tenant')->table('tables')->insert([
    'description1' => '31',
    'description2' => 'Alquiler de Naves, 8% sobre Ingresos Brutos que perciban por dicha actividad',
    'code_rent' => '8',
    'description3' => 'Art.12 y 48°b) ',
    'type' => '31',
    'state'=>true
]);
DB::connection('tenant')->table('tables')->insert([
    'description1' => '32',
    'description2' => 'Alquiler de Aeronaves,60% sobre Ingresos Brutos que perciban por dicha actividad',
    'code_rent' => '8',
    'description3' => 'Art.12 y 48° c)',
    'type' => '31',
    'state'=>true
]);
DB::connection('tenant')->table('tables')->insert([
    'description1' => '33',
    'description2' => '1% por el ingresos brutos por el transporte aéreo.',
    'code_rent' => '8',
    'description3' => 'Art.12 y 48° d)',
    'type' => '31',
    'state'=>true
]);
DB::connection('tenant')->table('tables')->insert([
    'description1' => '34',
    'description2' => 'Excepto Otros transportes no aereos o maritimos que van por los ingresos de la parte prestada en el país',
    'code_rent' => '8',
    'description3' => 'Art.12 y 48° d)',
    'type' => '31',
    'state'=>true
]);
DB::connection('tenant')->table('tables')->insert([
    'description1' => '35',
    'description2' => '2% por el ingresos brutos por fletamiento o transporte marítimo.',
    'code_rent' => '8',
    'description3' => 'Art.12 y 48° d)',
    'type' => '31',
    'state'=>true
]);
DB::connection('tenant')->table('tables')->insert([
    'description1' => '36',
    'description2' => 'Empresas por reciprocidad en lineas extranjeras con sedes en esos países',
    'code_rent' => '24',
    'description3' => 'Art.12 y 48° d)',
    'type' => '31',
    'state'=>true
]);
DB::connection('tenant')->table('tables')->insert([
    'description1' => '37',
    'description2' => 'Servicio portadores, teleservcios o finales, difusión y de valor añadido. Con excepcion de los Servicios Digitales según 4-A',
    'code_rent' => '07 - 21',
    'description3' => 'Art 12 y 48°e)',
    'type' => '31',
    'state'=>true
]);
DB::connection('tenant')->table('tables')->insert([
    'description1' => '38',
    'description2' => '10%. sobre las remuneraciones brutas que obtengan por el suministro de noticias y, en general material informativo o gráfico,a personas o entidades domiciliadas o que utilicen dicho material en el país.',
    'code_rent' => '07 - 21',
    'description3' => 'Art 12 y 48° f)',
    'type' => '31',
    'state'=>true 
]);
DB::connection('tenant')->table('tables')->insert([
    'description1' => '39',
    'description2' => 'para su utilización por personas naturales o jurídicas domiciliadas: 20% sobre los ingresos brutos que perciban por el uso de películas cinematográficas o para televisión, "video tape", radionovelas, discos fonográficos, historietas gráficas y cualquier otro medio similar de proyección, reproducción, transmisión o difusión de imágenes o sonidos.',
    'code_rent' => '07 - 21',
    'description3' => 'Art 12 y 48° g)',
    'type' => '31',
    'state'=>true
]);
DB::connection('tenant')->table('tables')->insert([
    'description1' => '40',
    'description2' => 'en el país o desde el país al exterior y no presten el servicio de transporte 15% de los ingresos brutos que obtengan por dicho suministro.',
    'code_rent' => '8',
    'description3' => 'Art 12 y 48° h)',
    'type' => '31',
    'state'=>true
]);
DB::connection('tenant')->table('tables')->insert([
    'description1' => '41',
    'description2' => '80% de los ingresos brutos que obtengan por el exceso de estadía de contenedores.',
    'code_rent' => '07 - 21',
    'description3' => 'Art 12 y 48° i)',
    'type' => '31',
    'state'=>true
]);
DB::connection('tenant')->table('tables')->insert([
    'description1' => '42',
    'description2' => '20% de los ingresos brutos que obtengan los contribuyentes no domiciliados por la cesión de derechos para la retransmisión por televisión en el país de eventos en vivo',
    'code_rent' => '07 - 21',
    'description3' => 'Art 12 y 48° j)',
    'type' => '31',
    'state'=>true
]);
DB::connection('tenant')->table('tables')->insert([
    'description1' => '43',
    'description2' => 'Extranjeros que ingresan al país y cuentas con las siguientes calidades especificas a prestar servicios',
    'code_rent' => '15',
    'description3' => 'Art. 13°',
    'type' => '31',
    'state'=>true
]);
//tabla 32
DB::connection('tenant')->table('tables')->insert([
    'description1' => '1',
    'description2' => 'SERVICIO PRESTADO INTEGRAMENTE EN EL PERÚ',
    'code_rent' => '',
    'type' => '32',
    'state'=>true
]);
DB::connection('tenant')->table('tables')->insert([
    'description1' => '2',
    'description2' => 'SERVICIO PRESTADO PARTE EN EL PERÚ Y PARTE EN EL EXTRANJERO',
    'code_rent' => '',
    'type' => '32',
    'state'=>true
]);
DB::connection('tenant')->table('tables')->insert([
    'description1' => '3',
    'description2' => 'SERVICIO PRESTADO EXCLUSIVAMENTE EN EL EXTRANJERO',
    'code_rent' => '',
    'type' => '32',
    'state'=>true
]);
//Tabla de Asientos////////////////////////////////////////////////////////////////////////////
DB::connection('tenant')->table('seating')->insert([
    'cod' => '1401',
    'description' => 'VENTA DE MERCADERIA (PRODUCTOS)',
    'seating1' => '70111',
    'seating2' => '70121',
    'seating3' => '70121',
    'seating4' => '70121',
    'seating5' => '40121',
    'seating6' => '40111',
    'seating7' => '40189',
    'seating8' => '12121',
    'type' => 'ventas',
    'state'=>true
]); 
DB::connection('tenant')->table('seating')->insert([
    'cod' => '1402',
    'description' => 'PRESTACIÓN DE SERVICIOS',
    'seating1' => '70311',
    'seating2' => '70321',
    'seating3' => '70321',
    'seating4' => '70321',
    'seating5' => '40121',
    'seating6' => '40111',
    'seating7' => '40189',
    'seating8' => '12121',
    'type' => 'ventas',
    'state'=>true
]); 
DB::connection('tenant')->table('seating')->insert([
    'cod' => '1403',
    'description' => 'VENTA DE PRODUCTOS TERMINADO (MANUFACTURADOS)',
    'seating1' => '70211',
    'seating2' => '70221',
    'seating3' => '70221',
    'seating4' => '70221',
    'seating5' => '40121',
    'seating6' => '40111',
    'seating7' => '40189',
    'seating8' => '12121',
    'type' => 'ventas',
    'state'=>true
]); 
DB::connection('tenant')->table('seating')->insert([
    'cod' => '1404',
    'description' => 'VENTA DE SUBPRODUCTOS',
    'seating1' => '70411',
    'seating2' => '70411',
    'seating3' => '70411',
    'seating4' => '70411',
    'seating5' => '40121',
    'seating6' => '40111',
    'seating7' => '40189',
    'seating8' => '12121',
     'type' => 'ventas',
    'state'=>true
]); 
///////////////////////////////////////////////////////////////////////////////////////////////
//Detalle de operacion 
DB::connection('tenant')->table('seating')->insert([
    'cod' => '100',
    'description' => 'VENTAS NETAS GRAVADAS',
    'seating1' => '',
    'seating2' => '',
    'seating3' => '',
    'seating4' => '',
    'seating5' => '',
    'seating6' => '',
    'seating7' => '',
    'seating8' => '',
    'type' => 'detalle',
    'state'=>true
]); 
DB::connection('tenant')->table('seating')->insert([
    'cod' => '102',
    'description' => 'DESCUENTOS CONCEDIDOS Y/O DEV DE VENTAS',
    'seating1' => '',
    'seating2' => '',
    'seating3' => '',
    'seating4' => '',
    'seating5' => '',
    'seating6' => '',
    'seating7' => '',
    'seating8' => '',
    'type' => 'detalle',
    'state'=>true
]); 
DB::connection('tenant')->table('seating')->insert([
    'cod' => '106',
    'description' => 'EXPORTACIONES FACTURADAS EN EL PERIODO (BIENES Y SERVICIOS)',
    'seating1' => '',
    'seating2' => '',
    'seating3' => '',
    'seating4' => '',
    'seating5' => '',
    'seating6' => '',
    'seating7' => '',
    'seating8' => '',
    'type' => 'detalle',
    'state'=>true
]); 
DB::connection('tenant')->table('seating')->insert([
    'cod' => '127',
    'description' => 'EXPORTACIONES EMBARCADAS EN EL PERIODO (INCLUYE EXPORTACION DE SERVICIOS)',
    'seating1' => '',
    'seating2' => '',
    'seating3' => '',
    'seating4' => '',
    'seating5' => '',
    'seating6' => '',
    'seating7' => '',
    'seating8' => '',
     'type' => 'detalle',
    'state'=>true
]); 
DB::connection('tenant')->table('seating')->insert([
    'cod' => '105',
    'description' => 'VENTAS NO GRAVADAS (SIN CONSIDERAR EXPORT)',
    'seating1' => '',
    'seating2' => '',
    'seating3' => '',
    'seating4' => '',
    'seating5' => '',
    'seating6' => '',
    'seating7' => '',
    'seating8' => '',
    'type' => 'detalle',
    'state'=>true
]); 
DB::connection('tenant')->table('seating')->insert([
    'cod' => '109',
    'description' => 'VENTAS NO GRAVADAS SIN EFECTO EN RATIO',
    'seating1' => '',
    'seating2' => '',
    'seating3' => '',
    'seating4' => '',
    'seating5' => '',
    'seating6' => '',
    'seating7' => '',
    'seating8' => '',
     'type' => 'detalle',
    'state'=>true
]); 
DB::connection('tenant')->table('seating')->insert([
    'cod' => '112',
    'description' => 'OTRAS VENTAS',
    'seating1' => '',
    'seating2' => '',
    'seating3' => '',
    'seating4' => '',
    'seating5' => '',
    'seating6' => '',
    'seating7' => '',
    'seating8' => '',
     'type' => 'detalle',
    'state'=>true
]); 
/////////////////////////////////////////////////////////////////////////////////////////////////
// tabla 33
DB::connection('tenant')->table('tables')->insert([
    'description1' => '1',
    'description2' => 'Los intereses provenientes de créditos de fomento otorgados directamente o mediante proveedores o intermediarios financieros por organismos internacionales o instituciones gubernamentales extranjeras.',
    'code_rent' => '',
    'type' => '33',
    'state'=>true
]);
DB::connection('tenant')->table('tables')->insert([
    'description1' => '2',
    'description2' => 'Las rentas de los inmuebles de propiedad de organismos internacionales que les sirvan de sede.',
    'code_rent' => '',
    'type' => '33',
    'state'=>true
]);  
DB::connection('tenant')->table('tables')->insert([
    'description1' => '3',
    'description2' => 'Las remuneraciones que perciban, por el ejercicio de su cargo en el país, los funcionarios y empleados considerados como tales dentro de la estructura organizacional de los gobiernos extranjeros, instituciones oficiales extranjeras y organismos internacionales, siempre que los convenios constitutivos así lo establezcan.',
    'code_rent' => '',
    'type' => '33',
    'state'=>true
]);  
DB::connection('tenant')->table('tables')->insert([
    'description1' => '4',
    'description2' => 'Los ingresos brutos que perciben las representaciones deportivas nacionales de países extranjeros por sus actuaciones en el país.',
    'code_rent' => '',
    'type' => '33',
    'state'=>true
]);  
DB::connection('tenant')->table('tables')->insert([
    'description1' => '5',
    'description2' => 'Las regalías por asesoramiento técnico, económico, financiero, o de otra índole, prestados desde el exterior por entidades estatales u organismos internacionales.',
    'code_rent' => '',
    'type' => '33',
    'state'=>true
]);  
DB::connection('tenant')->table('tables')->insert([
    'description1' => '6',
    'description2' => 'Los ingresos extranjeros por los espectáculos en vivo de teatro, zarzuela, conciertos de música clásica, ópera, opereta, ballet y folclor, calificados como espectáculos públicos culturales por el Instituto Nacional de Cultura, realizados en el país. brutos que perciben las representaciones de países.',
    'code_rent' => '',
    'type' => '33',
    'state'=>true
]); 
//tabla 34
/////////////////////////////////////////////////////////////////////////////////////////////////////////////

        $user_id = DB::connection('tenant')->table('users')->insert([
            'name' => 'Administrador',
            'email' => $request->input('email'),
            'password' => bcrypt($request->input('password')),
            'api_token' => $token,
            'type' => $request->input('type'),
            'locked' => true
        ]);


        if($request->input('type') == 'admin'){

            $array_modules = [];

            foreach ($request->modules as $module) {
                if($module['checked']){
                    $array_modules[] = ['module_id' => $module['id'], 'user_id' => $user_id];

                    if($module['id'] == 1){
                        DB::connection('tenant')->table('module_level_user')->insert([
                            ['module_level_id' => 1, 'user_id' => $user_id],
                            ['module_level_id' => 2, 'user_id' => $user_id],
                            ['module_level_id' => 3, 'user_id' => $user_id],
                            ['module_level_id' => 4, 'user_id' => $user_id],
                            ['module_level_id' => 5, 'user_id' => $user_id],
                            ['module_level_id' => 6, 'user_id' => $user_id],
                            ['module_level_id' => 7, 'user_id' => $user_id],
                            ['module_level_id' => 8, 'user_id' => $user_id],
                            ['module_level_id' => 9, 'user_id' => $user_id],
                        ]);
                    }
                }
            }
            DB::connection('tenant')->table('module_user')->insert($array_modules);

            // DB::connection('tenant')->table('module_user')->insert([
            //     ['module_id' => 1, 'user_id' => $user_id],
            //     ['module_id' => 2, 'user_id' => $user_id],
            //     ['module_id' => 3, 'user_id' => $user_id],
            //     ['module_id' => 4, 'user_id' => $user_id],
            //     ['module_id' => 5, 'user_id' => $user_id],
            //     ['module_id' => 6, 'user_id' => $user_id],
            //     ['module_id' => 7, 'user_id' => $user_id],
            //     ['module_id' => 8, 'user_id' => $user_id],
            //     ['module_id' => 9, 'user_id' => $user_id],
            //     ['module_id' => 10, 'user_id' => $user_id],
            //     ['module_id' => 11, 'user_id' => $user_id],
            // ]);

        }else{

            DB::connection('tenant')->table('module_user')->insert([
                ['module_id' => 1, 'user_id' => $user_id],
                ['module_id' => 3, 'user_id' => $user_id],
                ['module_id' => 5, 'user_id' => $user_id],
            ]);

        }

        return [
            'success' => true,
            'message' => 'Cliente Registrado satisfactoriamente'
        ];
    }

    public function validateWebsite($uuid, $website){

        $exists = $website::where('uuid', $uuid)->first();

        if($exists){
            throw new Exception("El subdominio ya se encuentra registrado");
        }

    }

    public function renewPlan(Request $request){

        // dd($request->all());
        $client = Client::findOrFail($request->id);
        $tenancy = app(Environment::class);
        $tenancy->tenant($client->hostname->website);

        DB::connection('tenant')->table('billing_cycles')->insert([
            'date_time_start' => date('Y-m-d H:i:s'),
            'renew' => true,
            'quantity_documents' => DB::connection('tenant')->table('configurations')->where('id', 1)->first()->quantity_documents,
            'created_at' => now(),
            'updated_at' => now(),
        ]);

        DB::connection('tenant')->table('configurations')->where('id', 1)->update(['quantity_documents' =>0]);


        return [
            'success' => true,
            'message' => 'Plan renovado con exito'
        ];

    }


    public function lockedUser(Request $request){

        $client = Client::findOrFail($request->id);
        $client->locked_users = $request->locked_users;
        $client->save();

        $tenancy = app(Environment::class);
        $tenancy->tenant($client->hostname->website);
        DB::connection('tenant')->table('configurations')->where('id', 1)->update(['locked_users' => $client->locked_users]);

        return [
            'success' => true,
            'message' => ($client->locked_users) ? 'Limitar creación de usuarios activado' : 'Limitar creación de usuarios desactivado'
        ];

    }


    public function lockedEmission(Request $request){

        $client = Client::findOrFail($request->id);
        $client->locked_emission = $request->locked_emission;
        $client->save();

        $tenancy = app(Environment::class);
        $tenancy->tenant($client->hostname->website);
        DB::connection('tenant')->table('configurations')->where('id', 1)->update(['locked_emission' => $client->locked_emission]);

        return [
            'success' => true,
            'message' => ($client->locked_emission) ? 'Limitar emisión de documentos activado' : 'Limitar emisión de documentos desactivado'
        ];

    }


    public function lockedTenant(Request $request){

        $client = Client::findOrFail($request->id);
        $client->locked_tenant = $request->locked_tenant;
        $client->save();

        $tenancy = app(Environment::class);
        $tenancy->tenant($client->hostname->website);
        DB::connection('tenant')->table('configurations')->where('id', 1)->update(['locked_tenant' => $client->locked_tenant]);

        return [
            'success' => true,
            'message' => ($client->locked_tenant) ? 'Cuenta bloqueada' : 'Cuenta desbloqueada'
        ];

    }



    public function destroy($id)
    {
        $client = Client::find($id);

        if($client->locked){
            return [
                'success' => false,
                'message' => 'Cliente bloqueado, no puede eliminarlo'
            ];
        }

        $hostname = Hostname::find($client->hostname_id);
        $website = Website::find($hostname->website_id);

        app(HostnameRepository::class)->delete($hostname, true);
        app(WebsiteRepository::class)->delete($website, true);

        return [
            'success' => true,
            'message' => 'Cliente eliminado con éxito'
        ];
    }

    public function password($id)
    {
        $client = Client::find($id);
        $website = Website::find($client->hostname->website_id);
        $tenancy = app(Environment::class);
        $tenancy->tenant($website);
        DB::connection('tenant')->table('users')
            ->where('id', 1)
            ->update(['password' => bcrypt($client->number)]);

        return [
            'success' => true,
            'message' => 'Clave cambiada con éxito'
        ];
    }

    public function startBillingCycle(Request $request)
    {
        $client = Client::findOrFail($request->id);
        $client->start_billing_cycle = $request->start_billing_cycle;
        $client->save();

        return [
            'success' => true,
            'message' => ($client->start_billing_cycle) ? 'Ciclo de Facturacion definido.' : 'No se pudieron guardar los cambios.'
        ];
    }

    public function upload(Request $request)
    {
        if ($request->hasFile('file')) {
            $new_request = [
                'file' => $request->file('file'),
                'type' => $request->input('type'),
            ];

            return $this->upload_certificate($new_request);
        }
        return [
            'success' => false,
            'message' => 'Error al subir file.',
        ];
    }

    function upload_certificate($request)
    {
        $file = $request['file'];
        $type = $request['type'];

        $temp = tempnam(sys_get_temp_dir(), $type);
        file_put_contents($temp, file_get_contents($file));

        $mime = mime_content_type($temp);
        $data = file_get_contents($temp);

        return [
            'success' => true,
            'data' => [
                'filename' => $file->getClientOriginalName(),
                'temp_path' => $temp,
                //'temp_image' => 'data:' . $mime . ';base64,' . base64_encode($data)
            ]
        ];
    }





}
