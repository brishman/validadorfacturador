<?php

$hostname = app(Hyn\Tenancy\Contracts\CurrentHostname::class);

if ($hostname) {
    Route::domain($hostname->fqdn)->group(function() {

        Auth::routes([
            'register' => false,
            'reset' => false,
            'verify' => false
        ]);

        Route::get('search', 'Tenant\SearchController@index')->name('search.index');
        Route::get('buscar', 'Tenant\SearchController@index')->name('search.index');
        Route::get('search/tables', 'Tenant\SearchController@tables');
        Route::post('search', 'Tenant\SearchController@store');
        Route::get('downloads/{model}/{type}/{external_id}/{format?}', 'Tenant\DownloadController@downloadExternal')->name('tenant.download.external_id');
        Route::get('print/{model}/{external_id}/{format?}', 'Tenant\DownloadController@toPrint');
        Route::get('/exchange_rate/ecommence/{date}', 'Tenant\Api\ServiceController@exchangeRateTest');

        Route::middleware(['auth', 'locked.tenant'])->group(function() {
            Route::get('/', function () {
                return redirect()->route('login');
            });
            //Login Sunat
            Route::get('sunat/consultar/{tipo}/{serie}/{numero}', 'Tenant\SunatController@listar_facturas');
            Route::get('catalogs', 'Tenant\CatalogController@index')->name('tenant.catalogs.index');
            Route::get('advanced', 'Tenant\AdvancedController@index')->name('tenant.advanced.index');
            Route::get('tasks', 'Tenant\TaskController@index')->name('tenant.tasks.index');
            Route::post('tasks/commands', 'Tenant\TaskController@listsCommand');
            Route::post('tasks/tables', 'Tenant\TaskController@tables');
            Route::post('tasks', 'Tenant\TaskController@store');
            Route::delete('tasks/{task}', 'Tenant\TaskController@destroy');

           /*//Orders
            Route::get('orders', 'Tenant\OrderController@index')->name('tenant_orders_index');
            Route::get('orders/columns', 'Tenant\OrderController@columns');
            Route::get('orders/records', 'Tenant\OrderController@records');
            Route::get('orders/record/{order}', 'Tenant\OrderController@record');
            //Route::get('orders/print/{external_id}/{format?}', 'Tenant\OrderController@toPrint');
            Route::post('statusOrder/update/', 'Tenant\OrderController@updateStatusOrders');
            Route::get('orders/pdf/{id}', 'Tenant\OrderController@pdf');

            //warehouse
            Route::post('orders/warehouse', 'Tenant\OrderController@searchWarehouse');
            Route::get('orders/tables', 'Tenant\OrderController@tables');
            Route::get('orders/tables/item/{internal_id}', 'Tenant\OrderController@item');
            //Status Orders
            Route::get('statusOrder/records', 'Tenant\StatusOrdersController@records');
            */
            //Company
            Route::get('companies', 'Tenant\CompanyController@create')->name('tenant.companies.create');
            Route::get('companies/tables', 'Tenant\CompanyController@tables');
            Route::get('companies/record', 'Tenant\CompanyController@record');
            Route::post('companies', 'Tenant\CompanyController@store');
            Route::post('companies/uploads', 'Tenant\CompanyController@uploadFile');



            //Configurations
            Route::get('configurations/addSeeder', 'Tenant\ConfigurationController@addSeeder');
            Route::get('configurations/getFormats', 'Tenant\ConfigurationController@getFormats');
            Route::get('configurations/create', 'Tenant\ConfigurationController@create')->name('tenant.configurations.create');
            Route::get('configurations/record', 'Tenant\ConfigurationController@record');
            Route::post('configurations', 'Tenant\ConfigurationController@store');
            Route::post('configurations/icbper', 'Tenant\ConfigurationController@icbper');
            Route::post('configurations/changeFormat', 'Tenant\ConfigurationController@changeFormat');
            Route::get('configurations/tables', 'Tenant\ConfigurationController@tables');
            Route::get('configurations/visual_defaults', 'Tenant\ConfigurationController@visualDefaults')->name('visual_defaults');
            Route::post('configurations/visual_settings', 'Tenant\ConfigurationController@visualSettings')->name('visual-settings');
            Route::get('configurations/pdf_templates', 'Tenant\ConfigurationController@pdfTemplates')->name('tenant.advanced.pdf_templates');
            Route::post('configurations/uploads', 'Tenant\ConfigurationController@uploadFile');

           //Periodo
           Route::get('periodo', 'Tenant\PeriodController@index')->name('tenant.periodo.index');
           Route::get('periodo/listar', 'Tenant\PeriodController@listar');
           Route::post('periodo', 'Tenant\PeriodController@store');

            //Establishments
            Route::get('taxpayer', 'Tenant\EstablishmentController@taxpayer')->name('tenant.taxpayer.index');
            Route::get('taxpayer/periodo', 'Tenant\EstablishmentController@periodo');
            Route::post('taxpayer/select', 'Tenant\EstablishmentController@select');
            Route::get('establishments', 'Tenant\EstablishmentController@index')->name('tenant.establishments.index');
            Route::get('establishments/create', 'Tenant\EstablishmentController@create');
            Route::get('establishments/columns', 'Tenant\EstablishmentController@columns');
            Route::get('establishments/tables', 'Tenant\EstablishmentController@tables');
            Route::get('establishments/record/{establishment}', 'Tenant\EstablishmentController@record');
            Route::post('establishments', 'Tenant\EstablishmentController@store');
            Route::get('establishments/records', 'Tenant\EstablishmentController@records');
            Route::get('establishments/count', 'Tenant\EstablishmentController@count');

            Route::delete('establishments/{establishment}', 'Tenant\EstablishmentController@destroy');

            //Opennig
            Route::get('openning', 'Tenant\OpeningController@index')->name('tenant.openning.index');
            Route::get('openning/create', 'Tenant\OpeningController@create');
            Route::get('openning/tables', 'Tenant\OpeningController@tables');
            Route::get('openning/record/{establishment}', 'Tenant\OpeningController@record');
            Route::post('openning', 'Tenant\OpeningController@store');
            Route::get('openning/{establishment_id}/records', 'Tenant\OpeningController@records');
            Route::get('openning/records', 'Tenant\OpeningController@records');
            Route::delete('openning/{establishment}', 'Tenant\OpeningController@destroy');
            Route::get('openning/columns', 'Tenant\OpeningController@columns');
            Route::get('openning/search/customers', 'Tenant\OpeningController@searchCustomers');
            Route::post('openning/import', 'Tenant\OpeningController@import');
            Route::get('openning/select', 'Tenant\OpeningController@select');
            Route::get('openning/tipodoc', 'Tenant\OpeningController@tipodoc');
            //Sales
            Route::get('sales', 'Tenant\SaleController@index')->name('tenant.sales.index');
            Route::get('sales/create', 'Tenant\SaleController@create');
            Route::get('sales/tables', 'Tenant\SaleController@tables');
            Route::get('sales/record/{establishment}', 'Tenant\SaleController@record');
            Route::post('sales', 'Tenant\SaleController@store');
            Route::get('sales/{establishment_id}/records', 'Tenant\SaleController@records');
            Route::get('sales/records', 'Tenant\SaleController@records');
            Route::delete('sales/{establishment}', 'Tenant\SaleController@destroy');
            Route::get('sales/columns', 'Tenant\SaleController@columns');
            Route::get('sales/search/customers', 'Tenant\SaleController@searchCustomers');
            Route::post('sales/import', 'Tenant\SaleController@import');
            Route::post('sales/download_xml', 'Tenant\SaleController@download_xml');
            Route::post('sales/download_cdr', 'Tenant\SaleController@download_cdr');
            Route::post('sales/download_pdf', 'Tenant\SaleController@download_pdf');
            Route::post('sales/api_service_url', 'Tenant\SaleController@api_service_url');
            Route::get('sales/download_file', 'Tenant\SaleController@download_file');
            Route::get('sales/credenciales/{id}', 'Tenant\SaleController@credenciales');
            Route::get('sales/periodo', 'Tenant\SaleController@periodo');
            Route::post('sales/generartxt', 'Tenant\SaleController@generartxt');
            Route::post('sales/validar_cpe', 'Tenant\SaleController@validar_cpe');
            Route::get('sales/select', 'Tenant\SaleController@select');
            Route::get('sales/tipodoc', 'Tenant\SaleController@tipodoc');
            //Libros Electronico
            Route::get('books', 'Tenant\BookController@index')->name('tenant.books.index');
            Route::get('books/records', 'Tenant\BookController@records');

            Route::get('plan', 'Tenant\PlanController@index')->name('tenant.plan.index');
            Route::post('plan/import', 'Tenant\PlanController@import');
            Route::get('plan/columns', 'Tenant\PlanController@columns');
            Route::get('plan/records', 'Tenant\PlanController@records');
            //Bank Accounts
            Route::get('bank_accounts', 'Tenant\BankAccountController@index')->name('tenant.bank_accounts.index');
            Route::get('bank_accounts/records', 'Tenant\BankAccountController@records');
            Route::get('bank_accounts/create', 'Tenant\BankAccountController@create');
            Route::get('bank_accounts/tables', 'Tenant\BankAccountController@tables');
            Route::get('bank_accounts/record/{bank_account}', 'Tenant\BankAccountController@record');
            Route::post('bank_accounts', 'Tenant\BankAccountController@store');
            Route::delete('bank_accounts/{bank_account}', 'Tenant\BankAccountController@destroy');

            //Series
            Route::get('series/records/{establishment}', 'Tenant\SeriesController@records');
            Route::get('series/create', 'Tenant\SeriesController@create');
            Route::get('series/tables', 'Tenant\SeriesController@tables');
            Route::post('series', 'Tenant\SeriesController@store');
            Route::delete('series/{series}', 'Tenant\SeriesController@destroy');

            //Users
            Route::get('users', 'Tenant\UserController@index')->name('tenant.users.index');
            Route::get('users/create', 'Tenant\UserController@create')->name('tenant.users.create');
            Route::get('users/tables', 'Tenant\UserController@tables');
            Route::get('users/record/{user}', 'Tenant\UserController@record');
            Route::post('users', 'Tenant\UserController@store');
            Route::get('users/records', 'Tenant\UserController@records');
            Route::delete('users/{user}', 'Tenant\UserController@destroy');

            //ChargeDiscounts
            Route::get('charge_discounts', 'Tenant\ChargeDiscountController@index')->name('tenant.charge_discounts.index');
            Route::get('charge_discounts/records/{type}', 'Tenant\ChargeDiscountController@records');
            Route::get('charge_discounts/create', 'Tenant\ChargeDiscountController@create');
            Route::get('charge_discounts/tables/{type}', 'Tenant\ChargeDiscountController@tables');
            Route::get('charge_discounts/record/{charge}', 'Tenant\ChargeDiscountController@record');
            Route::post('charge_discounts', 'Tenant\ChargeDiscountController@store');
            Route::delete('charge_discounts/{charge}', 'Tenant\ChargeDiscountController@destroy');


            //Items Ecommerce
            Route::get('items_ecommerce', 'Tenant\ItemController@index_ecommerce')->name('tenant.items_ecommerce.index');
            /*
            //Items
            Route::get('items', 'Tenant\ItemController@index')->name('tenant.items.index')->middleware('redirect.level');
            Route::get('items/columns', 'Tenant\ItemController@columns');
            Route::get('items/records', 'Tenant\ItemController@records');
            Route::get('items/tables', 'Tenant\ItemController@tables');
            Route::get('items/record/{item}', 'Tenant\ItemController@record');
            Route::post('items', 'Tenant\ItemController@store');
            Route::delete('items/{item}', 'Tenant\ItemController@destroy');
            Route::delete('items/item-unit-type/{item}', 'Tenant\ItemController@destroyItemUnitType');
            Route::post('items/import', 'Tenant\ItemController@import');
            Route::post('items/upload', 'Tenant\ItemController@upload');
            Route::post('items/visible_store', 'Tenant\ItemController@visibleStore');
            Route::post('items/duplicate', 'Tenant\ItemController@duplicate');
            Route::get('items/disable/{item}', 'Tenant\ItemController@disable');
            Route::get('items/enable/{item}', 'Tenant\ItemController@enable');
            Route::get('items/images/{item}', 'Tenant\ItemController@images');
            Route::get('items/images/delete/{id}', 'Tenant\ItemController@delete_images');
            Route::get('items/export', 'Tenant\ItemController@export')->name('tenant.items.export');
            Route::get('items/export/wp', 'Tenant\ItemController@exportWp')->name('tenant.items.export.wp');
            */
            //Persons
            Route::get('persons/columns', 'Tenant\PersonController@columns');
            Route::get('persons/tables', 'Tenant\PersonController@tables');
            Route::get('persons/{type}', 'Tenant\PersonController@index')->name('tenant.persons.index');
            Route::get('persons/{type}/records', 'Tenant\PersonController@records');
            Route::get('persons/record/{person}', 'Tenant\PersonController@record');
            Route::post('persons', 'Tenant\PersonController@store');
            Route::delete('persons/{person}', 'Tenant\PersonController@destroy');
            Route::post('persons/import', 'Tenant\PersonController@import');
            Route::get('persons/enabled/{type}/{person}', 'Tenant\PersonController@enabled');
            Route::get('persons/customers/exportation', 'Tenant\PersonController@export')->name('tenant.persons.export');

            Route::get('exchange_rates/columns', 'Tenant\ExchangeRateController@columns');
            Route::get('exchange_rates', 'Tenant\ExchangeRateController@index')->name('tenant.exchangerates.index');
            Route::get('exchange_rates/records', 'Tenant\ExchangeRateController@records');
            Route::post('exchange_rates', 'Tenant\ExchangeRateController@store');
            //Documents
            //Route::get('documents/pruebas', 'Tenant\DocumentController@pruebas');

           /* Route::post('documents/categories', 'Tenant\DocumentController@storeCategories');
            Route::post('documents/brands', 'Tenant\DocumentController@storeBrands');
            Route::get('documents/search/customers', 'Tenant\DocumentController@searchCustomers');
            Route::get('documents/search/customer/{id}', 'Tenant\DocumentController@searchCustomerById');
            Route::get('documents/search/externalId/{external_id}', 'Tenant\DocumentController@searchExternalId');

            Route::get('documents', 'Tenant\DocumentController@index')->name('tenant.documents.index')->middleware(['redirect.level','tenant.internal.mode']);
            Route::get('documents/columns', 'Tenant\DocumentController@columns');
            Route::get('documents/records', 'Tenant\DocumentController@records');
            Route::get('documents/create', 'Tenant\DocumentController@create')->name('tenant.documents.create')->middleware(['redirect.level','tenant.internal.mode']);
            Route::get('documents/create_tensu', 'Tenant\DocumentController@create_tensu')->name('tenant.documents.create_tensu');

            Route::get('documents/tables', 'Tenant\DocumentController@tables');
            Route::get('documents/record/{document}', 'Tenant\DocumentController@record');
            Route::post('documents', 'Tenant\DocumentController@store');
            Route::get('documents/send/{document}', 'Tenant\DocumentController@send');
            Route::get('documents/consult_cdr/{document}', 'Tenant\DocumentController@consultCdr');
            Route::post('documents/email', 'Tenant\DocumentController@email');
            Route::get('documents/note/{document}', 'Tenant\NoteController@create');
            Route::get('documents/note/record/{document}', 'Tenant\NoteController@record');
            Route::get('documents/item/tables', 'Tenant\DocumentController@item_tables');
            Route::get('documents/table/{table}', 'Tenant\DocumentController@table');
            Route::get('documents/re_store/{document}', 'Tenant\DocumentController@reStore');
            Route::get('documents/locked_emission', 'Tenant\DocumentController@messageLockedEmission');

           Route::get('document_payments/records/{document_id}', 'Tenant\DocumentPaymentController@records');
           Route::get('document_payments/document/{document_id}', 'Tenant\DocumentPaymentController@document');
           Route::get('document_payments/tables', 'Tenant\DocumentPaymentController@tables');
           Route::post('document_payments', 'Tenant\DocumentPaymentController@store');
           Route::delete('document_payments/{document_payment}', 'Tenant\DocumentPaymentController@destroy');
           Route::get('document_payments/initialize_balance', 'Tenant\DocumentPaymentController@initialize_balance');
           Route::get('document_payments/report/{start}/{end}/{report}', 'Tenant\DocumentPaymentController@report');


            Route::get('documents/send_server/{document}/{query?}', 'Tenant\DocumentController@sendServer');
            Route::get('documents/check_server/{document}', 'Tenant\DocumentController@checkServer');
            Route::get('documents/change_to_registered_status/{document}', 'Tenant\DocumentController@changeToRegisteredStatus');

            Route::post('documents/import', 'Tenant\DocumentController@import');
            Route::post('documents/import_second_format', 'Tenant\DocumentController@importTwoFormat');
            Route::get('documents/data_table', 'Tenant\DocumentController@data_table');
            Route::get('documents/payments/excel', 'Tenant\DocumentController@report_payments')->name('tenant.document.payments.excel');

            Route::delete('documents/delete_document/{document_id}', 'Tenant\DocumentController@destroyDocument');

            Route::get('documents/data-table/items', 'Tenant\DocumentController@getDataTableItem');

            //Contingencies
            Route::get('contingencies', 'Tenant\ContingencyController@index')->name('tenant.contingencies.index')->middleware('redirect.level','tenant.internal.mode');
            Route::get('contingencies/columns', 'Tenant\ContingencyController@columns');
            Route::get('contingencies/records', 'Tenant\ContingencyController@records');
            Route::get('contingencies/create', 'Tenant\ContingencyController@create')->name('tenant.contingencies.create');


            //Summaries
            Route::get('summaries', 'Tenant\SummaryController@index')->name('tenant.summaries.index')->middleware('redirect.level','tenant.internal.mode');
            Route::get('summaries/records', 'Tenant\SummaryController@records');
            Route::post('summaries/documents', 'Tenant\SummaryController@documents');
            Route::post('summaries', 'Tenant\SummaryController@store');
            Route::get('summaries/status/{summary}', 'Tenant\SummaryController@status');
            Route::get('summaries/columns', 'Tenant\SummaryController@columns');
            Route::delete('summaries/{summary}', 'Tenant\SummaryController@destroy');

            //Voided
            Route::get('voided', 'Tenant\VoidedController@index')->name('tenant.voided.index')->middleware('redirect.level','tenant.internal.mode');
            Route::get('voided/columns', 'Tenant\VoidedController@columns');
            Route::get('voided/records', 'Tenant\VoidedController@records');
            Route::post('voided', 'Tenant\VoidedController@store');
//            Route::get('voided/download/{type}/{voided}', 'Tenant\VoidedController@download')->name('tenant.voided.download');
            Route::get('voided/status/{voided}', 'Tenant\VoidedController@status');
            Route::get('voided/status_masive', 'Tenant\VoidedController@status_masive');

            Route::delete('voided/{voided}', 'Tenant\VoidedController@destroy');
//            Route::get('voided/ticket/{voided_id}/{group_id}', 'Tenant\VoidedController@ticket');

            //Retentions
            Route::get('retentions', 'Tenant\RetentionController@index')->name('tenant.retentions.index');
            Route::get('retentions/columns', 'Tenant\RetentionController@columns');
            Route::get('retentions/records', 'Tenant\RetentionController@records');
            Route::get('retentions/create', 'Tenant\RetentionController@create')->name('tenant.retentions.create');
            Route::get('retentions/tables', 'Tenant\RetentionController@tables');
            Route::get('retentions/record/{retention}', 'Tenant\RetentionController@record');
            Route::post('retentions', 'Tenant\RetentionController@store');
            Route::delete('retentions/{retention}', 'Tenant\RetentionController@destroy');
            Route::get('retentions/document/tables', 'Tenant\RetentionController@document_tables');
            Route::get('retentions/table/{table}', 'Tenant\RetentionController@table');

            //Dispatches
            Route::get('dispatches', 'Tenant\DispatchController@index')->name('tenant.dispatches.index');
            Route::get('dispatches/columns', 'Tenant\DispatchController@columns');
            Route::get('dispatches/records', 'Tenant\DispatchController@records');
            Route::get('dispatches/create/{document?}/{type?}/{dispatch?}', 'Tenant\DispatchController@create');
            Route::post('dispatches/tables', 'Tenant\DispatchController@tables');
            Route::post('dispatches', 'Tenant\DispatchController@store');
            Route::get('dispatches/record/{id}', 'Tenant\DispatchController@record');
            Route::post('dispatches/email', 'Tenant\DispatchController@email');
            */
            Route::get('reports/consistency-documents', 'Tenant\ReportConsistencyDocumentController@index')->name('tenant.consistency-documents.index')->middleware('tenant.internal.mode');
            Route::post('reports/consistency-documents/lists', 'Tenant\ReportConsistencyDocumentController@lists');

            Route::post('options/delete_documents', 'Tenant\OptionController@deleteDocuments');

            Route::get('service/{type}/{number}', 'Tenant\ServiceData@service');
           // Route::get('services/dni/{number}', 'Tenant\Api\ServiceController@dni');
           // Route::get('validarcpe/{document_type_code}/{series}/{number}', 'Tenant\Api\ServiceController@consultCdrStatus');
            //Route::post('services/consult_cdr_status', 'Tenant\Api\ServiceController@consultCdrStatus');
            //Route::post('services/exchange_rate', 'Tenant\Api\ServiceController@exchangeRateTest');
            //Route::post('services/search_exchange_rate', 'Tenant\Api\ServiceController@searchExchangeRateByDate');
            //Route::get('services/exchange_rate/{date}', 'Tenant\Api\ServiceController@exchangeRateTest');
            //Route::get('services/tipocambio', 'Tenant\Api\ServiceController@tipoCambio');
            //BUSQUEDA DE DOCUMENTOS
            // Route::get('busqueda', 'Tenant\SearchController@index')->name('search');
            // Route::post('busqueda', 'Tenant\SearchController@index')->name('search');
            /*
            //Codes
            Route::get('codes/records', 'Tenant\Catalogs\CodeController@records');
            Route::get('codes/tables', 'Tenant\Catalogs\CodeController@tables');
            Route::get('codes/record/{code}', 'Tenant\Catalogs\CodeController@record');
            Route::post('codes', 'Tenant\Catalogs\CodeController@store');
            Route::delete('codes/{code}', 'Tenant\Catalogs\CodeController@destroy');

            //Units
            Route::get('unit_types/records', 'Tenant\UnitTypeController@records');
            Route::get('unit_types/record/{code}', 'Tenant\UnitTypeController@record');
            Route::post('unit_types', 'Tenant\UnitTypeController@store');
            Route::delete('unit_types/{code}', 'Tenant\UnitTypeController@destroy');

            //Detractions
            Route::get('detraction_types/records', 'Tenant\DetractionTypeController@records');
            Route::get('detraction_types/tables', 'Tenant\DetractionTypeController@tables');
            Route::get('detraction_types/record/{code}', 'Tenant\DetractionTypeController@record');
            Route::post('detraction_types', 'Tenant\DetractionTypeController@store');
            Route::delete('detraction_types/{code}', 'Tenant\DetractionTypeController@destroy');

            //Banks
            Route::get('banks/records', 'Tenant\BankController@records');
            Route::get('banks/record/{bank}', 'Tenant\BankController@record');
            Route::post('banks', 'Tenant\BankController@store');
            Route::delete('banks/{bank}', 'Tenant\BankController@destroy');

            //Exchange Rates
         

            //Currency Types
            Route::get('currency_types/records', 'Tenant\CurrencyTypeController@records');
            Route::get('currency_types/record/{currency_type}', 'Tenant\CurrencyTypeController@record');
            Route::post('currency_types', 'Tenant\CurrencyTypeController@store');
            Route::delete('currency_types/{currency_type}', 'Tenant\CurrencyTypeController@destroy');

            //Perceptions
            Route::get('perceptions', 'Tenant\PerceptionController@index')->name('tenant.perceptions.index');
            Route::get('perceptions/columns', 'Tenant\PerceptionController@columns');
            Route::get('perceptions/records', 'Tenant\PerceptionController@records');
            Route::get('perceptions/create', 'Tenant\PerceptionController@create')->name('tenant.perceptions.create');
            Route::get('perceptions/tables', 'Tenant\PerceptionController@tables');
            Route::get('perceptions/record/{perception}', 'Tenant\PerceptionController@record');
            Route::post('perceptions', 'Tenant\PerceptionController@store');
            Route::delete('perceptions/{perception}', 'Tenant\PerceptionController@destroy');
            Route::get('perceptions/document/tables', 'Tenant\PerceptionController@document_tables');
            Route::get('perceptions/table/{table}', 'Tenant\PerceptionController@table');


            //Tribute Concept Type
            Route::get('tribute_concept_types/records', 'Tenant\TributeConceptTypeController@records');
            Route::get('tribute_concept_types/record/{id}', 'Tenant\TributeConceptTypeController@record');
            Route::post('tribute_concept_types', 'Tenant\TributeConceptTypeController@store');
            Route::delete('tribute_concept_types/{id}', 'Tenant\TributeConceptTypeController@destroy');
            */
            //purchases
            Route::get('purchases', 'Tenant\PurchasesController@index')->name('tenant.purchases.index');
            Route::get('purchases/columns', 'Tenant\PurchasesController@columns');
            Route::get('sales/credenciales/{id}', 'Tenant\PurchasesController@credenciales');
            Route::get('purchases/records', 'Tenant\PurchasesController@records');
            Route::get('purchases/create/{purchase_order_id?}', 'Tenant\PurchasesController@create')->name('tenant.purchases.create');
            Route::get('purchases/tables', 'Tenant\PurchasesController@tables');
            Route::get('purchases/table/{table}', 'Tenant\PurchasesController@table');
            Route::post('purchases', 'Tenant\PurchasesController@store');
            Route::post('purchases/update', 'Tenant\PurchasesController@update');
            Route::get('purchases/record/{document}', 'Tenant\PurchasesController@record');
            Route::get('purchases/edit/{id}', 'Tenant\PurchasesController@edit');
            Route::get('purchases/anular/{id}', 'Tenant\PurchasesController@anular');
            Route::get('purchases/delete/{id}', 'Tenant\PurchasesController@delete');
            Route::post('purchases/import', 'Tenant\PurchasesController@import');
            Route::post('purchases/download_xml', 'Tenant\PurchasesController@download_xml');
            Route::post('purchases/download_cdr', 'Tenant\PurchasesController@download_cdr');
            Route::post('purchases/download_pdf', 'Tenant\PurchasesController@download_pdf');
            Route::get('purchases/api_service_url', 'Tenant\PurchasesController@api_service_url');
            Route::get('purchases/download_file', 'Tenant\PurchasesController@download_file');
            Route::get('purchases/periodo', 'Tenant\PurchasesController@periodo');
            Route::post('purchases/generartxt', 'Tenant\PurchasesController@generartxt');
            Route::post('purchases/validar_cpe', 'Tenant\PurchasesController@validar_cpe');
            Route::get('purchases/select', 'Tenant\PurchasesController@select');
            Route::get('purchases/tipodoc', 'Tenant\PurchasesController@tipodoc');
            Route::get('purchases/exchangerate/{fecha}/{fecha2}', 'Tenant\PurchasesController@exchangeRate');

            Route::get('purchases/search/customers', 'Tenant\PurchasesController@searchCustomers');
            // Route::get('documents/send/{document}', 'Tenant\DocumentController@send');
            // Route::get('documents/consult_cdr/{document}', 'Tenant\DocumentController@consultCdr');
            // Route::post('documents/email', 'Tenant\DocumentController@email');
            // Route::get('documents/note/{document}', 'Tenant\NoteController@create');
            //Route::get('purchases/item/tables', 'Tenant\PurchaseController@item_tables');
            // Route::get('documents/table/{table}', 'Tenant\DocumentController@table');


           Route::get('item-sets', 'Tenant\ItemSetController@index')->name('tenant.item_sets.index');
           Route::get('item-sets/columns', 'Tenant\ItemSetController@columns');
           Route::get('item-sets/records', 'Tenant\ItemSetController@records');
           Route::get('item-sets/tables', 'Tenant\ItemSetController@tables');
           Route::get('item-sets/record/{item}', 'Tenant\ItemSetController@record');
           Route::post('item-sets', 'Tenant\ItemSetController@store');
           Route::delete('item-sets/{item}', 'Tenant\ItemSetController@destroy');
           Route::delete('item-sets/item-unit-type/{item}', 'Tenant\ItemSetController@destroyItemUnitType');
           Route::post('item-sets/import', 'Tenant\ItemSetController@import');
           Route::post('item-sets/upload', 'Tenant\ItemSetController@upload');
           Route::post('item-sets/visible_store', 'Tenant\ItemSetController@visibleStore');

           Route::get('person-types/columns', 'Tenant\PersonTypeController@columns');
           Route::get('person-types', 'Tenant\PersonTypeController@index')->name('tenant.person_types.index');
           Route::get('person-types/records', 'Tenant\PersonTypeController@records');
           Route::get('person-types/record/{person}', 'Tenant\PersonTypeController@record');
           Route::post('person-types', 'Tenant\PersonTypeController@store');
           Route::delete('person-types/{person}', 'Tenant\PersonTypeController@destroy');

           //Cuenta
           Route::get('cuenta/payment_index', 'Tenant\AccountController@paymentIndex')->name('tenant.payment.index');
           Route::get('cuenta/configuration', 'Tenant\AccountController@index')->name('tenant.configuration.index');
           Route::get('cuenta/payment_records', 'Tenant\AccountController@paymentRecords');
           Route::get('cuenta/tables', 'Tenant\AccountController@tables');
           Route::post('cuenta/update_plan', 'Tenant\AccountController@updatePlan');
           Route::post('cuenta/payment_culqui', 'Tenant\AccountController@paymentCulqui')->name('tenant.account.payment_culqui');

           //Payment Methods
           Route::get('payment_method/records', 'Tenant\PaymentMethodTypeController@records');
           Route::get('payment_method/record/{code}', 'Tenant\PaymentMethodTypeController@record');
           Route::post('payment_method', 'Tenant\PaymentMethodTypeController@store');
           Route::delete('payment_method/{code}', 'Tenant\PaymentMethodTypeController@destroy');

           //formats PDF
           Route::get('templates', 'Tenant\FormatTemplateController@records');

        });
    });
} else {
    Route::domain(env('APP_URL_BASE'))->group(function() {

        Route::get('login', 'System\LoginController@showLoginForm')->name('login');
        Route::post('login', 'System\LoginController@login');
        Route::post('logout', 'System\LoginController@logout')->name('logout');
        Route::get('phone', 'System\UserController@getPhone');

        Route::middleware('auth:admin')->group(function() {
            Route::get('logs', '\Rap2hpoutre\LaravelLogViewer\LogViewerController@index');
            Route::get('/', function () {
                return redirect()->route('system.dashboard');
            });
            Route::get('dashboard', 'System\HomeController@index')->name('system.dashboard');

            //Clients
            Route::get('clients', 'System\ClientController@index')->name('system.clients.index');
            Route::get('clients/records', 'System\ClientController@records');
            Route::get('clients/record/{client}', 'System\ClientController@record');
            Route::get('clients/columns', 'System\ClientController@columns');
            Route::get('clients/create', 'System\ClientController@create');
            Route::get('clients/tables', 'System\ClientController@tables');
            Route::get('clients/charts', 'System\ClientController@charts');
            Route::post('clients', 'System\ClientController@store');
            Route::get('clients/search/{id}/{state_type_id}', 'System\ClientController@search');
            Route::get('clients/validateDocuments', 'System\ClientController@validateDocuments');
            Route::post('clients/update', 'System\ClientController@update');

            Route::delete('clients/{client}', 'System\ClientController@destroy');
            Route::post('clients/password/{client}', 'System\ClientController@password');
            Route::post('clients/locked_emission', 'System\ClientController@lockedEmission');
            Route::post('clients/locked_tenant', 'System\ClientController@lockedTenant');
            Route::post('clients/locked_user', 'System\ClientController@lockedUser');
            Route::post('clients/renew_plan', 'System\ClientController@renewPlan');


            Route::post('clients/set_billing_cycle', 'System\ClientController@startBillingCycle');

            Route::post('clients/locked_tenant', 'System\ClientController@lockedTenant');

            Route::post('clients/upload', 'System\ClientController@upload');

            Route::get('client_payments/records/{client_id}', 'System\ClientPaymentController@records');
            Route::get('client_payments/client/{client_id}', 'System\ClientPaymentController@client');
            Route::get('client_payments/tables', 'System\ClientPaymentController@tables');
            Route::post('client_payments', 'System\ClientPaymentController@store');
            Route::delete('client_payments/{client_payment}', 'System\ClientPaymentController@destroy');
            Route::get('client_payments/cancel_payment/{client_payment_id}', 'System\ClientPaymentController@cancel_payment');

            Route::get('client_account_status/records/{client_id}', 'System\AccountStatusController@records');
            Route::get('client_account_status/client/{client_id}', 'System\AccountStatusController@client');
            Route::get('client_account_status/tables', 'System\AccountStatusController@tables');

            //Planes
            Route::get('plans', 'System\PlanController@index')->name('system.plans.index');
            Route::get('plans/records', 'System\PlanController@records');
            Route::get('plans/tables', 'System\PlanController@tables');
            Route::get('plans/record/{plan}', 'System\PlanController@record');
            Route::post('plans', 'System\PlanController@store');
            Route::delete('plans/{plan}', 'System\PlanController@destroy');

            //Users
            Route::get('users/create', 'System\UserController@create')->name('system.users.create');
            Route::get('users/record', 'System\UserController@record');
            Route::post('users', 'System\UserController@store');

            Route::get('services/ruc/{number}', 'System\ServiceController@ruc');

            Route::get('certificates/record', 'System\CertificateController@record');
            Route::post('certificates/uploads', 'System\CertificateController@uploadFile');
            Route::post('certificates/saveSoapUser', 'System\CertificateController@saveSoapUser');
            Route::delete('certificates', 'System\CertificateController@destroy');
            Route::get('configurations', 'System\ConfigurationController@index')->name('system.configuration.index');

            Route::get('companies/record', 'System\CompanyController@record');
            Route::post('companies', 'System\CompanyController@store');

            // auto-update
            Route::get('auto-update', 'System\UpdateController@index')->name('system.update');
            Route::get('auto-update/branch', 'System\UpdateController@branch')->name('system.update.branch');
            Route::get('auto-update/pull/{branch}', 'System\UpdateController@pull')->name('system.update.pull');

            Route::get('auto-update/artisan/migrate', 'System\UpdateController@artisanMigrate')->name('system.update.artisan.migrate');
            Route::get('auto-update/artisan/migrate/tenant', 'System\UpdateController@artisanTenancyMigrate')->name('system.update.artisan.tenancy.migrate');
            Route::get('auto-update/artisan/clear', 'System\UpdateController@artisanClear')->name('system.update.artisan.clear');

            Route::get('auto-update/composer/install', 'System\UpdateController@composerInstall')->name('system.update.composer.install');

            Route::get('auto-update/keygen', 'System\UpdateController@keygen')->name('system.update.keygen');

            Route::get('auto-update/version', 'System\UpdateController@version')->name('system.update.version');

            //Configuration

            Route::post('configurations', 'System\ConfigurationController@store');
            Route::get('configurations/record', 'System\ConfigurationController@record');

            // backup
            Route::get('backup', 'System\BackupController@index')->name('system.backup');
            Route::get('backup/db', 'System\BackupController@db')->name('system.backup.db');
            Route::get('backup/files', 'System\BackupController@files')->name('system.backup.files');

        });
    });
}
