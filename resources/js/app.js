require('./bootstrap');

// window.Vue = require('vue');
import Vue from 'vue'
import ElementUI from 'element-ui'
import Axios from 'axios'

import lang from 'element-ui/lib/locale/lang/es'
import locale from 'element-ui/lib/locale'
locale.use(lang)

//Vue.use(ElementUI)
Vue.use(ElementUI, {size: 'small'})
Vue.prototype.$eventHub = new Vue()
Vue.prototype.$http = Axios

// import VueCharts from 'vue-charts'
// Vue.use(VueCharts);
// import { TableComponent, TableColumn } from 'vue-table-component';
//
// Vue.component('table-component', TableComponent);
// Vue.component('table-column', TableColumn);
//Vue.component('tenant-dashboard-index', require('../../modules/Dashboard/Resources/assets/js/views/index.vue'));
/*
Vue.component('x-graph', require('./components/graph/src/Graph.vue'));
Vue.component('x-graph-line', require('./components/graph/src/GraphLine.vue'));
Vue.component('x-form-group', require('./components/FormGroup.vue'));
Vue.component('tenant-sale-list', require('./views/tenant/sales/index.vue'));
Vue.component('tenant-sales-form', require('./views/tenant/sales/form.vue'));
Vue.component('tenant-establishments-index', require('./views/tenant/establishments/index.vue'));
Vue.component('tenant-purchases-index', require('./views/tenant/purchase/index.vue'));

Vue.component('tenant-companies-form', require('./views/tenant/companies/form.vue'));

Vue.component('books-purchase-index', require('./views/tenant/books/purchases.vue'));
Vue.component('books-sale-index', require('./views/tenant/books/sales.vue'));
Vue.component('tenant-persons-index', require('./views/tenant/persons/index.vue'));
Vue.component('tenant-taxpayer-index', require('./views/tenant/taxpayer/index.vue'));
Vue.component('tenant-periodo-index', require('./views/tenant/periodo/index.vue'));
*/
Vue.component('x-input-service', require('./components/InputService.vue'));
//Vue.component('system-certificate-index', require('./views/system/certificate/index.vue'));
//auto update
//Vue.component('system-update', require('./views/system/update/index.vue'));
//Vue.component('tenant-opening', require('./views/tenant/opening/index.vue'));
//auto update
Vue.component('system-backup', require('./views/system/backup/index.vue'));
//Vue.component('system-configuration-culqi', require('./views/system/configuration/culqi.vue'))
//Vue.component('system-plans-index', require('./views/system/plans/index.vue'));
//Vue.component('system-plans-form', require('./views/system/plans/form.vue'));
Vue.component('system-clients-index', require('./views/system/clients/index.vue'));
//Vue.component('system-clients-form', require('./views/system/clients/form.vue'));
//Vue.component('system-accounting-index', require('@viewsModuleAccount/system/accounting/index.vue'));
const app = new Vue({
    el: '#dashboard-ecommerce',

});
