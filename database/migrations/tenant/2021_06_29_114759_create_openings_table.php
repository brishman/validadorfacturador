<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateOpeningsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('openings', function (Blueprint $table) {
            $table->id();
            $table->string('libro')->nullable(); 
            $table->string('oper')->nullable(); 
            $table->date('fechae')->nullable(); 
            $table->date('fechav')->nullable(); 
            $table->string('tipo')->nullable(); 
            $table->string('serie')->nullable(); 
            $table->string('numero')->nullable(); 
            $table->string('tipodoc')->nullable(); 
            $table->unsignedInteger('customer_id')->nullable(); 
            $table->unsignedInteger('establishment_id')->nullable(); 
            $table->double('tota',10,4)->nullable(); 
            $table->string('totac')->nullable(); 
            $table->string('totalz')->nullable(); 
            $table->string('periodo')->nullable(); 
            $table->timestamps();
            $table->foreign('customer_id')->references('id')->on('persons');
            $table->foreign('establishment_id')->references('id')->on('establishments');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('openings');
    }
}
