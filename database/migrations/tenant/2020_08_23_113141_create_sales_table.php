<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateSalesTable extends Migration
{
    public function up()
    {
        Schema::create('sales', function (Blueprint $table) {
            $table->increments('id');
            $table->string('period')->nullable(); 
            $table->string('oper')->nullable();
            $table->string('tipoc')->nullable();
            $table->unsignedInteger('customer_id');
            $table->string('serie')->nullable();
            $table->string('numero')->nullable();
            $table->string('serienota')->nullable()->default('');
            $table->string('numeronota')->nullable()->default('');
            $table->string('tipodocm',2)->nullable();
            $table->date('fecham')->nullable();
            $table->date('fechae')->nullable();
            $table->date('fechav')->nullable();
            $table->string('moneda')->nullable();
            $table->date('fechacam')->nullable();
            $table->string('tipocam')->nullable();
            $table->string('asiento')->nullable();
            $table->string('detalle')->nullable();
            $table->double('tota',7,2)->nullable();
            $table->double('igv',7,2)->nullable();
            $table->integer('n_igv')->nullable();
            $table->double('base',7,2)->nullable();
            $table->double('based',7,2)->nullable();
            $table->double('baseivap',7,2)->nullable();
            $table->double('ivap',7,2)->nullable();
            $table->double('inafecta',7,2)->nullable();
            $table->double('icbper',7,2)->nullable();
            $table->double('exo',7,2)->nullable();
            $table->double('otros',7,2)->nullable();
            $table->double('isc',7,2)->nullable();
            $table->double('reten',7,2)->nullable();
            $table->double('export',7,2)->nullable();
            $table->string('detrac',4   )->nullable();
            $table->date('fechadet')->nullable();
            $table->string('numerodet')->nullable();
            $table->double('totadetrac',7,2)->nullable();
            $table->string('cobrar')->nullable();
            $table->string('exportc')->nullable();
            $table->string('basec')->nullable();
            $table->string('exoc')->nullable();
            $table->string('inafc')->nullable();
            $table->string('iscoc')->nullable();
            $table->string('igvc')->nullable();
            $table->string('otrosc')->nullable();
            $table->string( 'totac')->nullable();
            $table->string('code_status')->nullable();
            $table->text('xml_sales')->nullable();
            $table->text('cdr_sales')->nullable();
            $table->text('pdf_sales')->nullable();
            $table->unsignedInteger('establishment_id');
            $table->timestamps();
            $table->foreign('establishment_id')->references('id')->on('establishments');
            $table->foreign('customer_id')->references('id')->on('persons');
            
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('sales');
    }
}
