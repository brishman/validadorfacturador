<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSeatingTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('seating', function (Blueprint $table) {
            $table->increments('id');
            $table->string('cod',4);
            $table->string('description');
            $table->string('seating1',10);
            $table->string('seating2',10);
            $table->string('seating3',10);
            $table->string('seating4',10);
            $table->string('seating5',10);
            $table->string('seating6',10);
            $table->string('seating7',10);
            $table->string('seating8',10);
            $table->string('type',30);
            $table->string('state',2);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('seating');
    }
}
