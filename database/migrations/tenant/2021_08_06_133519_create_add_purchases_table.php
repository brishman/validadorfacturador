<?php

use App\Models\Tenant\Seat;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAddPurchasesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
       //Tabla de Asientos////////////////////////////////////////////////////////////////////////////
    Seat::create([
    'cod' => '0801',
    'description' => 'COMPRA DE MERCADERIA (PRODUCTOS)',
    'seating1' => '60111',
    'seating2' => '40111',
    'seating3' => '60112',
    'seating4' => '40121',
    'seating5' => '6011',
    'seating6' => '42121',
    'seating7' => '40113',
    'seating8' => '',
    'type' => 'compras',
    'state'=>true
]);
Seat::create([
    'cod' => '0802',
    'description' => 'IMPORTACION DE MERCADERIA (PRODUCTOS)',
    'seating1' => '60111',
    'seating2' => '40111',
    'seating3' => '60112',
    'seating4' => '40121',
    'seating5' => '6011',
    'seating6' => '42121',
    'seating7' => '40113',
    'seating8' => '',
    'type' => 'compras',
    'state'=>true
]);
Seat::create([
    'cod' => '0803',
    'description' => 'COMPRA DE MATERIA PRIMA',
    'seating1' => '60211',
    'seating2' => '40111',
    'seating3' => '60211',
    'seating4' => '40121',
    'seating5' => '602',
    'seating6' => '42121',
    'seating7' => '40113',
    'seating8' => '',
    'type' => 'compras',
    'state'=>true
]);
Seat::create([
    'cod' => '0804',
    'description' => 'IMPORTACION DE MATERIA PRIMA',
    'seating1' => '60211',
    'seating2' => '40111',
    'seating3' => '60211',
    'seating4' => '40121',
    'seating5' => '602',
    'seating6' => '42121',
    'seating7' => '40113',
    'seating8' => '',
     'type' => 'compras',
    'state'=>true
]);
//Tabla de Asientos////////////////////////////////////////////////////////////////////////////
Seat::create([
    'cod' => '0805',
    'description' => 'COMPRA DE MATERIALES AUXILIARES',
    'seating1' => '60311',
    'seating2' => '40111',
    'seating3' => '60311',
    'seating4' => '40121',
    'seating5' => '6031',
    'seating6' => '42121',
    'seating7' => '40113',
    'seating8' => '',
    'type' => 'compras',
    'state'=>true
]);
Seat::create([
    'cod' => '0806',
    'description' => 'COMPRA DE SUMINISTROS',
    'seating1' => '60321',
    'seating2' => '40111',
    'seating3' => '60321',
    'seating4' => '40121',
    'seating5' => '6032',
    'seating6' => '42121',
    'seating7' => '40113',
    'seating8' => '',
    'type' => 'compras',
    'state'=>true
]);
Seat::create([
    'cod' => '0807',
    'description' => 'COMPRA DE ENVASES',
    'seating1' => '60411',
    'seating2' => '40111',
    'seating3' => '60411',
    'seating4' => '40121',
    'seating5' => '6041',
    'seating6' => '42121',
    'seating7' => '40113',
    'seating8' => '',
    'type' => 'compras',
    'state'=>true
]);
Seat::create([
    'cod' => '0808',
    'description' => 'COMPRA DE EMBALAJES',
    'seating1' => '60421',
    'seating2' => '40111',
    'seating3' => '60421',
    'seating4' => '40121',
    'seating5' => '6042',
    'seating6' => '42121',
    'seating7' => '40113',
    'seating8' => '',
     'type' => 'compras',
    'state'=>true
]);
Seat::create([
    'cod' => '0809',
    'description' => 'COMPRA, COSTOS VINCULADOS CON LAS COMPRAS DE MERCADERIA',
    'seating1' => '60911',
    'seating2' => '40111',
    'seating3' => '60911',
    'seating4' => '40121',
    'seating5' => '60911',
    'seating6' => '42121',
    'seating7' => '40113',
    'seating8' => '',
     'type' => 'compras',
    'state'=>true
]);
Seat::create([
    'cod' => '0810',
    'description' => 'COMPRA DE ACTIVO FIJO',
    'seating1' => '33411',
    'seating2' => '40111',
    'seating3' => '33411',
    'seating4' => '40121',
    'seating5' => '33411',
    'seating6' => '42121',
    'seating7' => '40113',
    'seating8' => '',
     'type' => 'compras',
    'state'=>true
]);
Seat::create([
    'cod' => '0811',
    'description' => 'COMPRA DE MUEBLES Y ENSERES',
    'seating1' => '33511',
    'seating2' => '40111',
    'seating3' => '33511',
    'seating4' => '40121',
    'seating5' => '33511',
    'seating6' => '42121',
    'seating7' => '40113',
    'seating8' => '',
     'type' => 'compras',
    'state'=>true
]);
Seat::create([
    'cod' => '0812',
    'description' => 'COMPRA DE EQUIPOS DIVERSOS (COMPUTO, VARIOS)',
    'seating1' => '33611',
    'seating2' => '40111',
    'seating3' => '33611',
    'seating4' => '40121',
    'seating5' => '33611',
    'seating6' => '42121',
    'seating7' => '40113',
    'seating8' => '',
     'type' => 'compras',
    'state'=>true
]);
Seat::create([
    'cod' => '0813',
    'description' => 'COMPRA DE HERRAMIENTA',
    'seating1' => '33711',
    'seating2' => '40111',
    'seating3' => '33711',
    'seating4' => '40121',
    'seating5' => '33711',
    'seating6' => '42121',
    'seating7' => '40113',
    'seating8' => '',
     'type' => 'compras',
    'state'=>true
]);
Seat::create([
    'cod' => '0814',
    'description' => 'COMPRA DE INTANGIBLE (SOFTWARE INFORMATICO)',
    'seating1' => '34311',
    'seating2' => '40111',
    'seating3' => '34311',
    'seating4' => '40121',
    'seating5' => '34311',
    'seating6' => '42121',
    'seating7' => '40113',
    'seating8' => '',
     'type' => 'compras',
    'state'=>true
]);
Seat::create([
    'cod' => '0815',
    'description' => 'SERVICIO DE TRANSPORTE DE CARGA - PRODUCCION',
    'seating1' => '63111',
    'seating2' => '40111',
    'seating3' => '63111',
    'seating4' => '40121',
    'seating5' => '63111',
    'seating6' => '42121',
    'seating7' => '40113',
    'seating8' => '',
     'type' => 'compras',
    'state'=>true
]);
Seat::create([
    'cod' => '0816',
    'description' => 'SERVICIO DE TRANSPORTE DE CARGA - ADMINISTRACION',
    'seating1' => '63111',
    'seating2' => '40111',
    'seating3' => '63111',
    'seating4' => '40121',
    'seating5' => '63111',
    'seating6' => '42121',
    'seating7' => '40113',
    'seating8' => '',
     'type' => 'compras',
    'state'=>true
]);
Seat::create([
    'cod' => '0817',
    'description' => 'SERVICIO DE TRANSPORTE DE CARGA - VENTA',
    'seating1' => '63111',
    'seating2' => '40111',
    'seating3' => '63111',
    'seating4' => '40121',
    'seating5' => '63111',
    'seating6' => '42121',
    'seating7' => '40113',
    'seating8' => '',
     'type' => 'compras',
    'state'=>true
]);
Seat::create([
    'cod' => '0818',
    'description' => 'SERVICIO DE CORREO (PRODUCCION)',
    'seating1' => '63129',
    'seating2' => '40111',
    'seating3' => '63129',
    'seating4' => '40121',
    'seating5' => '63129',
    'seating6' => '42121',
    'seating7' => '40113',
    'seating8' => '',
     'type' => 'compras',
    'state'=>true
]);

Seat::create([
    'cod' => '107',
    'description' => 'COMPRAS NETAS DESTINADAS A VENTA GRAVADA',
    'seating1' => '',
    'seating2' => '',
    'seating3' => '',
    'seating4' => '',
    'seating5' => '',
    'seating6' => '',
    'seating7' => '',
    'seating8' => '',
    'type' => 'detalles',
    'state'=>true
]);
Seat::create([
    'cod' => '110',
    'description' => 'COMPRAS NETAS DESTINADAS A VENTA GRAVADA Y NO GRAVADA',
    'seating1' => '',
    'seating2' => '',
    'seating3' => '',
    'seating4' => '',
    'seating5' => '',
    'seating6' => '',
    'seating7' => '',
    'seating8' => '',
    'type' => 'detalles',
    'state'=>true
]);
Seat::create([
    'cod' => '113',
    'description' => 'COMPRAS NETAS DESTINADAS A VENTA NO GRAVADA',
    'seating1' => '',
    'seating2' => '',
    'seating3' => '',
    'seating4' => '',
    'seating5' => '',
    'seating6' => '',
    'seating7' => '',
    'seating8' => '',
    'type' => 'detalles',
    'state'=>true
]);
Seat::create([
    'cod' => '114',
    'description' => 'COMPRAS NETAS DESTINADAS A VENTA GRAVADA IMPORTADA',
    'seating1' => '',
    'seating2' => '',
    'seating3' => '',
    'seating4' => '',
    'seating5' => '',
    'seating6' => '',
    'seating7' => '',
    'seating8' => '',
    'type' => 'detalles',
    'state'=>true
]);
Seat::create([
    'cod' => '116',
    'description' => 'COMPRAS NETA DEST. VENTA GRAVADA Y NO GRAVADA IMPORTADA',
    'seating1' => '',
    'seating2' => '',
    'seating3' => '',
    'seating4' => '',
    'seating5' => '',
    'seating6' => '',
    'seating7' => '',
    'seating8' => '',
    'type' => 'detalles',
    'state'=>true
]);
Seat::create([
    'cod' => '119',
    'description' => 'COMPRAS IMPORTADAS DESTINADAS A NO GRAVADA',
    'seating1' => '',
    'seating2' => '',
    'seating3' => '',
    'seating4' => '',
    'seating5' => '',
    'seating6' => '',
    'seating7' => '',
    'seating8' => '',
    'type' => 'detalles',
    'state'=>true
]);

Seat::create([
    'cod' => '120',
    'description' => 'COMPRAS INTERNA NO GRAVADA',
    'seating1' => '',
    'seating2' => '',
    'seating3' => '',
    'seating4' => '',
    'seating5' => '',
    'seating6' => '',
    'seating7' => '',
    'seating8' => '',
    'type' => 'detalles',
    'state'=>true
]);
Seat::create([
    'cod' => '122',
    'description' => 'COMPRAS IMPORTADAS NO GRAVADA',
    'seating1' => '',
    'seating2' => '',
    'seating3' => '',
    'seating4' => '',
    'seating5' => '',
    'seating6' => '',
    'seating7' => '',
    'seating8' => '',
    'type' => 'detalles',
    'state'=>true
]);

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('add_purchases');
    }
}
